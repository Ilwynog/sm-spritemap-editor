#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "rom/romhandler.h"
#include <QFileDialog>
#include <iostream>
#include "lorom/lorom.h"
#include "data/tileset.h"
#include <QGraphicsPixmapItem>
#include "gui/spritescene.h"
#include <QListWidgetItem>
#include <QTextEdit>
#include <QStandardItemModel>
#include <rom/json.hpp>
#include <fstream>
#include <QMessageBox>
#include <QShortcut>
#include "gui/animationobject.h"
#include <QInputDialog>

static AnimationObject animationObject; //Holds graphics, spritemaps, hitboxes, instruction lists
static std::vector<uint8_t> romData;
static std::vector<uint16_t> paletteData;
static SpriteScene *spritemapScene = nullptr; //TODO: Make these not global static vars

void MainWindow::connectAnimatorSignals()
{
    connect(animator, &Animator::currentDrawInstruction, ui->instructionListTable, &QTableWidget::selectRow);
    connect(ui->instructionListTable, &QTableWidget::currentItemChanged, this, [=](QTableWidgetItem* cur) { ui->spinCurrentInstruction->setValue(cur->row());});
    connect(animator, &Animator::counterChanged, ui->spinCounter, &QSpinBox::setValue);
    connect(animator, &Animator::timerChanged, ui->spinTimer, &QSpinBox::setValue);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    instListParserDialog = new instrListParserDialog(this);
    //instListParserDialog->show();

    ui->tabWidget_2->setTabEnabled(2, false); //TODO: Get rid of these when adding animation/hitbox/ext spritemap stuff
    ui->tabWidget_2->setTabText(2, "There are no easter eggs up here, go away");

    QGraphicsView* gfxViews[] = {ui->gfxView0, ui->gfxView1, ui->gfxView2, ui->gfxView3, ui->gfxView4, ui->gfxView5, ui->gfxView6, ui->gfxView7};
    SpriteScene* gfxViewScene;
    for (int i=0; i < 8; i++)
    {
        gfxViewScene = new SpriteScene(SpriteScene::GFX);
        gfxViewScene->setBackgroundBrush(Qt::blue);
        gfxViews[i]->setScene(gfxViewScene);
        gfxViews[i]->scale(2,2);

        connect(gfxViewScene, SIGNAL(spriteDoubleClicked(int)), this, SLOT(addSprite(int)));
        connect(ui->buttonAddSprite, SIGNAL(released()), gfxViewScene, SLOT(requestSprite()));
    }

    spritemapScene = new SpriteScene(SpriteScene::SPRITEMAP);
    spritemapScene->setSceneRect(-128,-128,256,256);
    //spritemapScene->setSceneRect(-128,-128,128,128); //Offset scene rectangle
    ui->viewSpritemap->setScene(spritemapScene);
    ui->viewSpritemap->scale(2,2);

    ui->instructionListTable->setRowCount(0);

    //Connect signals/slots
    connect(ui->checkLargeSprite, SIGNAL(toggled(bool)), ui->gfxView0->scene(), SLOT(setUseLarge(bool)));
    connect(ui->checkLargeSprite, SIGNAL(toggled(bool)), ui->gfxView1->scene(), SLOT(setUseLarge(bool)));
    connect(ui->checkLargeSprite, SIGNAL(toggled(bool)), ui->gfxView2->scene(), SLOT(setUseLarge(bool)));
    connect(ui->checkLargeSprite, SIGNAL(toggled(bool)), ui->gfxView3->scene(), SLOT(setUseLarge(bool)));
    connect(ui->checkLargeSprite, SIGNAL(toggled(bool)), ui->gfxView4->scene(), SLOT(setUseLarge(bool)));
    connect(ui->checkLargeSprite, SIGNAL(toggled(bool)), ui->gfxView5->scene(), SLOT(setUseLarge(bool)));
    connect(ui->checkLargeSprite, SIGNAL(toggled(bool)), ui->gfxView6->scene(), SLOT(setUseLarge(bool)));
    connect(ui->checkLargeSprite, SIGNAL(toggled(bool)), ui->gfxView7->scene(), SLOT(setUseLarge(bool)));

    connect(spritemapScene, &SpriteScene::spriteLarge, ui->checkLargeSprite, &QCheckBox::setChecked);

    connect(spritemapScene, &SpriteScene::spriteXPos, this, [=]( const int &val ) { ui->spinPosX->setProperty("oldValue", val); });
    connect(spritemapScene, &SpriteScene::spriteXPos, ui->spinPosX, &QSpinBox::setValue);
    connect(spritemapScene, &SpriteScene::spriteYPos, this, [=]( const int &val ) { ui->spinPosY->setProperty("oldValue", val); });
    connect(spritemapScene, &SpriteScene::spriteYPos, ui->spinPosY, &QSpinBox::setValue);

    connect(spritemapScene, &SpriteScene::spriteTileIndex, ui->spinTileNumber, &QSpinBox::setValue);
    //connect(spritemapScene, &SpriteScene::spritePalette, ui->comboPalette, &QComboBox::setCurrentIndex);
    connect(spritemapScene, &SpriteScene::spritePalette, this, [=](const int &val) { ui->comboPalette->blockSignals(true);
                                                                               ui->comboPalette->setCurrentIndex(val);
                                                                               ui->comboPalette->blockSignals(false);});
    //connect(spritemapScene, &SpriteScene::spritePriority, ui->comboPriority, &QComboBox::setCurrentIndex);
    connect(spritemapScene, &SpriteScene::spritePriority, this, [=](const int &val) { ui->comboPriority->blockSignals(true);
                                                                               ui->comboPriority->setCurrentIndex(val);
                                                                               ui->comboPriority->blockSignals(false);});

    connect(spritemapScene, &SpriteScene::spriteObject, static_cast<SpriteScene*>(ui->gfxView0->scene()), &SpriteScene::selectSpriteTile);

    connect(spritemapScene, &SpriteScene::spriteObject, this, &MainWindow::setSpriteListIndexViaSpriteObject);
    connect(spritemapScene, &SpriteScene::selectionChanged, this, &MainWindow::connectSpriteSignals);
    connect(spritemapScene, &SpriteScene::selectionChanged, this, &MainWindow::selectSpriteListItems);

    connect(ui->buttonAddSpritemap, &QPushButton::released, this, &MainWindow::actAddSpritemap);
    connect(ui->buttonDelSpritemap, &QPushButton::released, this, &MainWindow::actDelSpritemap);
    connect(ui->spritemapList, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), this, SLOT(currentSpritemapChanged(QListWidgetItem*,QListWidgetItem*)));

    connect(ui->checkGfxCompressed, &QCheckBox::toggled, this, &MainWindow::enablePage1);

    connect(ui->checkDrawGrid, &QCheckBox::toggled, spritemapScene, &SpriteScene::toggleGrid);
    connect(ui->buttonBackdropColor, &QPushButton::released, this, &MainWindow::changeBackdropColor);

    //Frame combobox autoloads spritemap //TODO: add toggle
    connect(ui->comboFrame, &QComboBox::currentIndexChanged, this, &MainWindow::actLoadSpritemap);

    //Regular instruction list editor
    connect(ui->pushDelInstruction, &QPushButton::released, this, &MainWindow::actDelInstruction);
    connect(ui->pushAddIndstruction, &QPushButton::released, this, &MainWindow::actAddInstruction);
    connect(ui->instTimer, &QSpinBox::valueChanged, this, &MainWindow::actSetInstTimer);
    connect(ui->instComboSpritemap, &QComboBox::activated, this, &MainWindow::actSetInstSpritemap);
    connect(ui->instructionListTable, &QTableWidget::itemSelectionChanged, this, &MainWindow::actInstListActivated);
    connect(ui->instListCommandType, &QComboBox::activated, this, &MainWindow::actSetInstructionTypeGuiElements);
    connect(ui->instListCommandType, &QComboBox::activated, this, &MainWindow::actSetInstructionType);
    connect(ui->instComboCommand, &QComboBox::activated, this, &MainWindow::actSetInstCommand);
    connect(ui->instLineArgument, &QLineEdit::editingFinished, this, &MainWindow::actSetInstArgument);
    connect(ui->instPushAddCommand, &QPushButton::released, this, &MainWindow::actAddCodeCommand);
    connect(ui->instPushDeleteCommand, &QPushButton::released, this, &MainWindow::actDeleteCodeCommand);
    connect(ui->instPushLoadDefaultCommands, &QPushButton::released, this, &MainWindow::actDisplayDefaultCommandLoader);


    //Instruction list parser
    connect(instListParserDialog, &instrListParserDialog::sigParseInstList, instListParserDialog, &instrListParserDialog::parseInstructionList);
    connect(instListParserDialog, &instrListParserDialog::sigAcceptInstList, this, &MainWindow::actAcceptInstructionList);


    ui->instComboCommand->setModel( ui->instKnownCommands->model() );

    //Animator
    connect(ui->instPlay, &QPushButton::released, this, &MainWindow::actToggleAnimation);
    connectAnimatorSignals();

    //Keyboard shortcuts
      //Delete sprites
    QShortcut *shortcut = nullptr;
    shortcut = new QShortcut(QKeySequence(Qt::Key_Delete), this);
    connect(shortcut, &QShortcut::activated, this, &MainWindow::on_buttonDelSprite_released);
    shortcut = new QShortcut(QKeySequence(Qt::ShiftModifier|Qt::Key_Delete), this);
    connect(shortcut, &QShortcut::activated, this, &MainWindow::on_buttonDelSprite_released);

    //Make all spritemap lists show the same model
    QAbstractItemModel* sprMapModel = ui->spritemapList->model();
    ui->spritemapListAnimation->setModel(
                sprMapModel);
    ui->instComboSpritemap->setModel( sprMapModel );

    //Setup good sizes for table view cells
    ui->instructionListTable->resizeColumnsToContents();
    ui->extendedSpritemapTable->resizeColumnsToContents();

    //Create menus
    createMenuActions();

    //Set up gfx view tabs
    on_tabWidget_currentChanged(0);

    //Palettes and priorities
    ui->comboPalette->clear();
    for (int i=0; i < 8; i++)
        ui->comboPalette->addItem(QString::number(i));

    ui->comboPriority->clear();
    for (int i=0; i < 4; i++)
        ui->comboPriority->addItem(QString::number(i));

    //Default palette TODO: Make a proper one
    paletteData.resize(128);
    for (int i=0; i < 128; i++)
    {
        paletteData.at(i) = i*4;
    }
}

void MainWindow::createMenuActions()
{
    //Separate gfx/palette loaders
    QMenu *loadMenu = new QMenu("Load file");
    ui->menuFile->addMenu(loadMenu);

    QMenu *menuTools = new QMenu("Tools");
    ui->menubar->addMenu(menuTools);

    QAction *loadGfxFileAct = new QAction("Load .gfx page 0");
    connect(loadGfxFileAct, &QAction::triggered, this, &MainWindow::actLoadGfxPage0File);
    loadMenu->addAction(loadGfxFileAct);

    QAction *loadGfxFileActPage1 = new QAction("Load .gfx page 1");
    connect(loadGfxFileActPage1, &QAction::triggered, this, &MainWindow::actLoadGfxPage1File);
    loadMenu->addAction(loadGfxFileActPage1);

    QMenu *paletteLoadMenu = new QMenu("Palette");
    loadMenu->addMenu(paletteLoadMenu);

    QAction *loadFullPalette = new QAction("Full palette");
    connect(loadFullPalette, &QAction::triggered, this, &MainWindow::actLoadFullPalette);
    paletteLoadMenu->addAction(loadFullPalette);

    //TODO: Add separator?
    QString palLoadNames[] = {"Palette 0", "Palette 1", "Palette 2", "Palette 3", "Palette 4", "Palette 5", "Palette 6", "Palette 7"};
    QAction *loadPaletteLine = new QAction(palLoadNames[0]);
    connect(loadPaletteLine, &QAction::triggered, this, &MainWindow::actLoadPal0);
    paletteLoadMenu->addAction(loadPaletteLine);

    loadPaletteLine = new QAction(palLoadNames[1]);
    connect(loadPaletteLine, &QAction::triggered, this, &MainWindow::actLoadPal1);
    paletteLoadMenu->addAction(loadPaletteLine);

    loadPaletteLine = new QAction(palLoadNames[2]);
    connect(loadPaletteLine, &QAction::triggered, this, &MainWindow::actLoadPal2);
    paletteLoadMenu->addAction(loadPaletteLine);

    loadPaletteLine = new QAction(palLoadNames[3]);
    connect(loadPaletteLine, &QAction::triggered, this, &MainWindow::actLoadPal3);
    paletteLoadMenu->addAction(loadPaletteLine);

    loadPaletteLine = new QAction(palLoadNames[4]);
    connect(loadPaletteLine, &QAction::triggered, this, &MainWindow::actLoadPal4);
    paletteLoadMenu->addAction(loadPaletteLine);

    loadPaletteLine = new QAction(palLoadNames[5]);
    connect(loadPaletteLine, &QAction::triggered, this, &MainWindow::actLoadPal5);
    paletteLoadMenu->addAction(loadPaletteLine);

    loadPaletteLine = new QAction(palLoadNames[6]);
    connect(loadPaletteLine, &QAction::triggered, this, &MainWindow::actLoadPal6);
    paletteLoadMenu->addAction(loadPaletteLine);

    loadPaletteLine = new QAction(palLoadNames[7]);
    connect(loadPaletteLine, &QAction::triggered, this, &MainWindow::actLoadPal7);
    paletteLoadMenu->addAction(loadPaletteLine);


    //Other things
    QAction *openAct = new QAction("Load ROM");
    connect(openAct, &QAction::triggered, this, &MainWindow::actLoadRom); //Load rom
    //connect(openAct, &QAction::triggered, this, &MainWindow::actLoadSpritemapOldDeprecatedRemoveThis); //Load spritemap
    ui->menuFile->addAction(openAct);

    /*QAction *saveAct = new QAction("Save ROM");
    connect(saveAct, &QAction::triggered, this, &MainWindow::actSaveRom);
    ui->menuFile->addAction(saveAct);
    ui->toolBar->addAction(saveAct);

    QAction *saveNewAct = new QAction("Save new ROM");
    connect(saveNewAct, &QAction::triggered, this, &MainWindow::actSaveNewRom);
    ui->menuFile->addAction(saveNewAct);*/

    QAction *saveAnimObjectAct = new QAction("Save project");
    connect(saveAnimObjectAct, &QAction::triggered, this, &MainWindow::actSaveAnimObject);
    ui->menuFile->addAction(saveAnimObjectAct);

    QAction *loadAnimObjectAct = new QAction("Load project");
    connect(loadAnimObjectAct, &QAction::triggered, this, &MainWindow::actLoadAnimObject);
    ui->menuFile->addAction(loadAnimObjectAct);

    QAction *saveSpriteMapAct = new QAction("Save spritemap bin");
    connect(saveSpriteMapAct, &QAction::triggered, this, &MainWindow::actSaveSpritemap);
    ui->menuFile->addAction(saveSpriteMapAct);
    ui->toolBar->addAction(saveSpriteMapAct);

    QAction *saveSpritemapAsmAct = new QAction("Save spritemap asm");
    connect(saveSpritemapAsmAct, &QAction::triggered, this, &MainWindow::actSaveSpritemapAsm);
    ui->menuFile->addAction(saveSpritemapAsmAct);
    ui->toolBar->addAction(saveSpritemapAsmAct);

    QAction *exportAnimObjectAsmAct = new QAction("Export");
    connect(exportAnimObjectAsmAct, &QAction::triggered, this, &MainWindow::actExportAnimation);
    ui->menuFile->addAction(exportAnimObjectAsmAct);
    ui->toolBar->addAction(exportAnimObjectAsmAct);

    QAction *pippeli = new QAction("Parse instruction list");
    connect(pippeli, &QAction::triggered, this, &MainWindow::actShowInstListParser);
    menuTools->addAction(pippeli);
    //ui->toolBar->addAction(pippeli);

    //Load new spritemap, palette and gfx
    connect(ui->buttonLoadSpritemap, &QPushButton::released, this, &MainWindow::actLoadPalette); //Shows warning if no rom loaded
    connect(ui->buttonLoadSpritemap, &QPushButton::released, this, &MainWindow::actLoadGfx);
    connect(ui->buttonLoadSpritemap, &QPushButton::released, this, &MainWindow::actLoadSpritemap);

    QAction *showSpritemapAsmAct = new QAction("View spritemap asm");
    connect(showSpritemapAsmAct, &QAction::triggered, this, &MainWindow::actShowSpritemapAsm);
    ui->toolBar->addAction(showSpritemapAsmAct);


    QMenu *menuHelp = new QMenu("Help");
    ui->menubar->addMenu(menuHelp);
    QAction *showAboutAct = new QAction("About");
    connect(showAboutAct, &QAction::triggered, this, &MainWindow::actShowAbout);
    menuHelp->addAction(showAboutAct);
}

void MainWindow::loadObjectList()
{
    //Create object/animation/frame lists
    /*QStandardItemModel* frameModel = new QStandardItemModel;
    QStandardItem* test = new QStandardItem;
    test->setText("do i exist");
    test->setData(5, Qt::UserRole);
    frameModel->appendRow(test);
    test = new QStandardItem;
    test->setText("i am, therefore i am");
    test->setData(8, Qt::UserRole+1);
    frameModel->appendRow(test);

    ui->comboFrame->setModel(frameModel);*/

    //Get known spritemap data
    std::ifstream i( "./data/spritemaps.json" );
    if (!i.is_open()) return;

    nlohmann::json spriteJson;
    i >> spriteJson;

    QStandardItemModel *objModel, *animModel, *frameModel;
    int gfx, gfx1, pal, spr, size;
    size = 0;
    bool gfxDecomp = false;
    bool gfxDecomp1 = false;
    bool palDecomp = false;


    objModel = new QStandardItemModel;
    for (nlohmann::json object : spriteJson["objects"])
    {
        QStandardItem* objItem = new QStandardItem;
        objItem->setText( QString::fromStdString( object["name"] ));

        animModel = new QStandardItemModel;

        for (nlohmann::json anim : object)
        {
            if (!anim.is_object()) continue;
            frameModel = new QStandardItemModel;

            QStandardItem* animItem = new QStandardItem;
            animItem->setText( QString::fromStdString(anim["name"]) );
            animModel->appendRow(animItem);

            if (anim.contains("gfx"))  gfx = QString::fromStdString( anim["gfx"] ).toInt(nullptr,16);
            if (anim.contains("gfx1")) gfx1 = QString::fromStdString( anim["gfx1"] ).toInt(nullptr,16);
            pal = QString::fromStdString( anim["palette"] ).toInt(nullptr,16);
            if (anim.contains("decompressGfx")) gfxDecomp = anim["decompressGfx"];
            if (anim.contains("decompressGfx1")) gfxDecomp1 = anim["decompressGfx1"];
            if (anim.contains("decompressPal")) palDecomp = anim["decompressPal"];

            int i=0;
            for (nlohmann::json frame : anim)
            {
                if (!frame.is_array()) continue;
                for (nlohmann::json data : frame)
                {
                    if (!data.is_string()) continue;

                    QStandardItem* frameItem = new QStandardItem;
                    frameItem->setText(QString::number(i));
                    i++;

                    spr = QString::fromStdString( data.get<std::string>() ).toInt(nullptr,16);

                    frameItem->setData(gfx, Qt::UserRole);
                    frameItem->setData(pal, Qt::UserRole+1);
                    frameItem->setData(spr, Qt::UserRole+2);
                    frameItem->setData(gfxDecomp, Qt::UserRole+3);
                    frameItem->setData(palDecomp, Qt::UserRole+4);
                    frameItem->setData(gfx1, Qt::UserRole+5);
                    frameItem->setData(gfxDecomp1, Qt::UserRole+6);

                    frameModel->appendRow(frameItem);
                }
            }

            animItem->setData(QVariant::fromValue(frameModel), Qt::UserRole);
        }

        objItem->setData(QVariant::fromValue(animModel), Qt::UserRole);
        if (object.contains("size"))
          size = QString::fromStdString( object["size"] ).toInt(nullptr, 16);
        if (size < 0 || size > 5) size = 0; //Clamp size
        objItem->setData(size, Qt::UserRole+1);

        objModel->appendRow(objItem);
    }

    ui->comboObject->setModel(objModel);
    ui->comboAnim->setModel( qvariant_cast<QStandardItemModel*>(ui->comboObject->currentData(Qt::UserRole)) );
    ui->comboFrame->setModel( qvariant_cast<QStandardItemModel*>(ui->comboAnim->currentData(Qt::UserRole)) );
}


MainWindow::~MainWindow()
{
    delete ui;
    delete spritemapScene;
}

Sprite *MainWindow::addSprite()
{
    return addSprite(std::vector<uint8_t>{0,0,0,0,0}, 0);
}

Sprite *MainWindow::parseSprite(const std::vector<uint8_t> &data, const int ptr)
{
    if (static_cast<int>(data.size()) < ptr+5) throw std::runtime_error("Not enough data for sprite");

    Sprite *sprite = new Sprite(nullptr,
                                data[ptr+0],
                                data[ptr+1],
                                data[ptr+2],
                                data[ptr+3],
                                data[ptr+4]
                                );

    return sprite;
}

Sprite *MainWindow::addSprite(const std::vector<uint8_t> &data, const int ptr)
{
    if (static_cast<int>(data.size()) < ptr+5) throw std::runtime_error("Not enough data for sprite");

    Sprite *sprite = new Sprite(spritemapScene,
            data[ptr+0],
            data[ptr+1],
            data[ptr+2],
            data[ptr+3],
            data[ptr+4]
           );
    sprite->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);
    sprite->setPixmap( getSpriteGraphic(sprite) );
    sprite->setFlags(QGraphicsPixmapItem::ItemIsSelectable | QGraphicsPixmapItem::ItemIsMovable | QGraphicsPixmapItem::ItemIsFocusable);
    spritemapScene->addItem(sprite);

    return sprite;
}

/**
 * @brief addSprite Adds given sprite to spritemap
 * @param sprite Sprite to add
 * @return Return same sprite
 */
Sprite* MainWindow::addSprite(Sprite* sprite)
{
    sprite->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);
    sprite->setPixmap( getSpriteGraphic(sprite) );
    sprite->setFlags(QGraphicsPixmapItem::ItemIsSelectable | QGraphicsPixmapItem::ItemIsMovable | QGraphicsPixmapItem::ItemIsFocusable);
    spritemapScene->addItem(sprite);

    return sprite;
}


void MainWindow::actLoadRom()
{
    //Select rom
    QString file = "";
    file = QFileDialog::getOpenFileName(this, tr("Select ROM"), "./", tr("ROM (*.smc *.sfc)"));
    if (file.isEmpty())
    {
        std::cout << "No file chosen";
        return;
    }

    //Load rom
    try {
        RomHandler::loadFile(file.toStdString(), romData);
    } catch (std::string &e) {
        std::cerr << e << std::endl;
        return;
    }

    lastLoadedRom = file;

    ui->comboFrame->blockSignals(true);
    loadObjectList();
    ui->comboFrame->blockSignals(false);
}

QPixmap MainWindow::getSpriteGraphic(Sprite *sprite)
{
    uint8_t palette = sprite->getPalette();
    QGraphicsView* gfxViews[] = {ui->gfxView0, ui->gfxView1, ui->gfxView2, ui->gfxView3, ui->gfxView4, ui->gfxView5, ui->gfxView6, ui->gfxView7};
    return static_cast<SpriteScene*>(gfxViews[palette]->scene())->getSpriteGraphic(sprite);
}


/**
 * @brief MainWindow::on_buttonSaveSpritemap_released Saves spritemap data back to rom
 */
void MainWindow::actSaveRom()
{
    //TODO: Remove this function, saving directly to rom is dumb af
    uint32_t spritemapPointer = LoRom::snes2pc(ui->sprMapPointerLorom->text().toInt(nullptr,16));

    std::vector<uint8_t> result, temp;
    result.emplace_back(ui->spriteIndexList->count());
    result.emplace_back(0);

    for (int i=0; i < ui->spriteIndexList->count(); i++)
    {
        temp = qvariant_cast<Sprite*>(ui->spriteIndexList->item(i)->data(Qt::UserRole)) -> getByteData();
        result.insert(result.end(), temp.begin(), temp.end());
    }

    for (uint i=0; i < result.size(); i++)
        romData[spritemapPointer+i] = result[i];

    QString outFile;// = QFileDialog::getSaveFileName(this, tr("Save ROM"), "./", tr("ROM (*.smc *.sfc)"));
    outFile = lastLoadedRom;
    if (outFile.isEmpty()) return;
    RomHandler::writeFile(outFile.toStdString(), romData);
}


/**
 * @brief MainWindow::on_buttonSaveSpritemap_released Saves spritemap data back to a different rom file
 */
void MainWindow::actSaveNewRom()
{
    //TODO: Remove this function, saving directly to rom is dumb af
    uint32_t spritemapPointer = LoRom::snes2pc(ui->sprMapPointerLorom->text().toInt(nullptr,16));

    std::vector<uint8_t> result, temp;
    result.emplace_back(ui->spriteIndexList->count());
    result.emplace_back(0);

    for (int i=0; i < ui->spriteIndexList->count(); i++)
    {
        temp = qvariant_cast<Sprite*>(ui->spriteIndexList->item(i)->data(Qt::UserRole)) -> getByteData();
        result.insert(result.end(), temp.begin(), temp.end());
    }

    for (uint i=0; i < result.size(); i++)
        romData[spritemapPointer+i] = result[i];

    QString outFile = QFileDialog::getSaveFileName(this, tr("Save ROM"), "./", tr("ROM (*.smc *.sfc)"));
    lastLoadedRom = outFile;
    if (outFile.isEmpty()) return;
    RomHandler::writeFile(outFile.toStdString(), romData);
}

void MainWindow::actSaveSpritemap()
{
    std::vector<uint8_t> result, temp;
    result.emplace_back(ui->spriteIndexList->count());
    result.emplace_back(0);

    for (int i=0; i < ui->spriteIndexList->count(); i++)
    {
        temp = qvariant_cast<Sprite*>(ui->spriteIndexList->item(i)->data(Qt::UserRole)) -> getByteData();
        result.insert(result.end(), temp.begin(), temp.end());
    }

    QString outFile = QFileDialog::getSaveFileName(this, tr("Save spritemap"), "./", tr("File (*.bin)"));
    if (outFile.isEmpty()) return;
    RomHandler::writeFile(outFile.toStdString(), result);
}

void MainWindow::actSaveSpritemapAsm()
{
    QString result = getSpritemapAsm();

    auto string = result.toUtf8();

    std::vector<uint8_t> writeThis;
    writeThis.assign(string.begin(), string.end());

    QString outFile = QFileDialog::getSaveFileName(this, tr("Save spritemap asm"), "./", tr("File (*.asm)"));
    if (outFile.isEmpty()) return;
    RomHandler::writeFile(outFile.toStdString(), writeThis);
}

void MainWindow::actShowAbout()
{
    QString message = QString("Version: %1 Beta 1\n"
                              "Join the MetConst Discord for updates: https://discord.gg/xDwaaqa\n\n"
                              "Developed by Smiley").arg(__DATE__);
    QMessageBox::about(this, "About Spredit", message);
}

QImage MainWindow::loadNewGfx(std::vector<uint8_t> gfxData, std::vector<uint16_t> palData, int paletteIndex, QPoint topLeft)
{
    QImage result;

    //Load gfx file to image
    QImage sheetImage = loadGraphicsImage(gfxData, 0, palData, false, paletteIndex);
    //sheetImage = sheetImage.copy(0,0, 0x80, 0x80);
    //sheetImage = sheetImage.copy(0, 0, 0x80, 0x100);

    //Current gfx to image, create blank if doesn't exist
    QGraphicsView* gfxViews[] = {ui->gfxView0, ui->gfxView1, ui->gfxView2, ui->gfxView3, ui->gfxView4, ui->gfxView5, ui->gfxView6, ui->gfxView7};
    result = static_cast<SpriteScene*>(gfxViews[paletteIndex]->scene())->getGfxSheet();
    if (result.isNull() || result.format() == QImage::Format_Invalid)
    {
        result = QImage(256, 512, QImage::Format_ARGB32);
    }

    //Overwrite current gfx with new gfx
    QPainter painter(&result);
    painter.setCompositionMode(QPainter::CompositionMode_Source);
    painter.fillRect(topLeft.x(), topLeft.y(), 0x80, 0x80, Qt::transparent);
    //painter.drawImage(topLeft.x(), topLeft.y(), sheetImage);
    painter.drawImage(QRectF(topLeft.x(), topLeft.y(), 0x80, 0x80),
                      sheetImage,
                      QRectF(0,0,0x80,0x80));

    //Draw copies of the gfx sheet, for drawing large sprites
    painter.drawImage( 0, result.height(), result );
    painter.drawImage( result.width()/2, 0, result, 0, 8);

    return result;
}

void MainWindow::actLoadGfxPage1File()
{
    std::vector<uint8_t> data;

    QString file = "";
    file = QFileDialog::getOpenFileName(this, tr("Select .gfx file"), "./", tr("GFX (*.gfx)"));
    if (file.isEmpty())
    {
        std::cout << "No file chosen";
        return;
    }

    //Load file
    try {
        RomHandler::loadFile(file.toStdString(), data);
    } catch (std::string &e) {
        std::cerr << e << std::endl;
        return;
    }

    //GFX data to animation object
    animationObject.setGfxData(data, 0x2000);

    for (int i=0; i < 8; i++)
    {
        QGraphicsView* gfxViews[] = {ui->gfxView0, ui->gfxView1, ui->gfxView2, ui->gfxView3, ui->gfxView4, ui->gfxView5, ui->gfxView6, ui->gfxView7};
        QImage newGfx = loadNewGfx(data, paletteData, i, QPoint(0, 0x80));

        //Render gfx view
        gfxViews[i]->setBackgroundBrush(Qt::blue);
        static_cast<SpriteScene*>(gfxViews[i]->scene())->setGfxSheet(newGfx);
        static_cast<SpriteScene*>(gfxViews[i]->scene())->clear(); //Delete old image
        gfxViews[i]->scene()->addPixmap(QPixmap::fromImage(newGfx.copy(0,0, //Draw gfx. x,y
                                                                                            0x100,    //width
                                                                                            0x200    //height
                                                                         )));
        //gfxViews[i]->setSceneRect(ui->gfxView0->scene()->itemsBoundingRect());
        gfxViews[i]->setSceneRect(ui->gfxView0->rect());
    }
    redrawSpritemap();
}

void MainWindow::setPalette(std::vector<uint16_t> data, int paletteIndex)
{
    for (uint i=0; i < data.size(); i++)
    {
        paletteData.at(paletteIndex*16 + i) = data[i];
    }
}

void MainWindow::actLoadFullPalette()
{
    std::vector<uint8_t> data, dataTemp;
    std::vector<uint16_t> palData;

    QString file = "";
    file = QFileDialog::getOpenFileName(this, tr("Select palette file"), "./", tr("Palette (*.pal *.tpl)"));
    if (file.isEmpty())
    {
        std::cout << "No file chosen";
        return;
    }

    //Load file
    try {
        RomHandler::loadFile(file.toStdString(), data);
    } catch (std::string &e) {
        std::cerr << e << std::endl;
        return;
    }

    if (data.size() < 8*16*2 || data.size() % 2 != 0) //Palette definitely too small or invalid
    {
        QMessageBox(QMessageBox::NoIcon, QString("Error"), QString("Invalid palette"));
        return;
    }


    if (data.size() == 772) data.erase(data.begin(), data.begin()+4); //Chop off stupid "TPL." header
    if (data.size() != 768) data.resize(768, 0);                      //Ensure correct size TODO: Check for corrupt palette?
    //TODO: Add support for different palette formats
    //8-bit RGB to 5-bit BGR
    uint16_t bgr;
    dataTemp.resize(0x200);
    for (int i=0, max = dataTemp.size()/2; i < max; i++)
    {
        bgr = 0;
        bgr = ( (data.at(i*3+0) >> 3) <<  0) //R
             |( (data.at(i*3+1) >> 3) <<  5) //G
             |( (data.at(i*3+2) >> 3) << 10);//B

        dataTemp.at(i*2+0) = bgr >> 0;
        dataTemp.at(i*2+1) = bgr >> 8;
    }
    dataTemp.resize(0x100);
    palData = Tileset().parsePalette(dataTemp, 0, false);
    palData.resize(0x80); //8 lines

    //Palette data to animation object
    animationObject.setPaletteData(palData);

    setPalette(palData, 0);
}

void MainWindow::loadSinglePalette(int paletteIndex)
{
    std::vector<uint8_t> data, dataTemp;
    std::vector<uint16_t> palData;

    QString file = "";
    file = QFileDialog::getOpenFileName(this, tr("Select palette file"), "./", tr("Palette (*.pal *.tpl)"));
    if (file.isEmpty())
    {
        std::cout << "No file chosen";
        return;
    }

    //Load file
    try {
        RomHandler::loadFile(file.toStdString(), data);
    } catch (std::string &e) {
        std::cerr << e << std::endl;
        return;
    }

    if (data.size() < 16*2 || data.size() % 2 != 0) //Palette definitely too small or invalid
    {
        QMessageBox(QMessageBox::NoIcon, QString("Error"), QString("Invalid palette"));
        return;
    }

    if (data.size() % 16 == 4) data.erase(data.begin(), data.begin()+4); //Chop off stupid "TPL." header
    if (data.size() != 768) data.resize(768, 0);                      //Ensure correct size TODO: Check for corrupt palette?
    //TODO: Add support for different palette formats
    //8-bit RGB to 5-bit BGR
    uint16_t bgr;
    dataTemp.resize(0x200);
    for (int i=0, max = dataTemp.size()/2; i < max; i++)
    {
        bgr = 0;
        bgr = ( (data.at(i*3+0) >> 3) <<  0) //R
             |( (data.at(i*3+1) >> 3) <<  5) //G
             |( (data.at(i*3+2) >> 3) << 10);//B

        dataTemp.at(i*2+0) = bgr >> 0;
        dataTemp.at(i*2+1) = bgr >> 8;
    }
    dataTemp.resize(0x20);
    palData = Tileset().parsePalette(dataTemp, 0, false);
    palData.resize(0x10); //Single line

    //Palette data to animation object
    animationObject.setPaletteLine(palData, paletteIndex);

    setPalette(palData, paletteIndex);
}

void MainWindow::actLoadPal0()
{
    loadSinglePalette(0);
}

void MainWindow::actLoadPal1()
{
    loadSinglePalette(1);
}

void MainWindow::actLoadPal2()
{
    loadSinglePalette(2);
}

void MainWindow::actLoadPal3()
{
    loadSinglePalette(3);
}

void MainWindow::actLoadPal4()
{
    loadSinglePalette(4);
}

void MainWindow::actLoadPal5()
{
    loadSinglePalette(5);
}

void MainWindow::actLoadPal6()
{
    loadSinglePalette(6);
}

void MainWindow::actLoadPal7()
{
    loadSinglePalette(7);
}

void MainWindow::actShowInstListParser()
{
    instListParserDialog->rom = romData;
    instListParserDialog->pal0Scene = static_cast<SpriteScene*>(ui->gfxView0->scene());
    instListParserDialog->pal1Scene = static_cast<SpriteScene*>(ui->gfxView1->scene());
    instListParserDialog->pal2Scene = static_cast<SpriteScene*>(ui->gfxView2->scene());
    instListParserDialog->pal3Scene = static_cast<SpriteScene*>(ui->gfxView3->scene());
    instListParserDialog->pal4Scene = static_cast<SpriteScene*>(ui->gfxView4->scene());
    instListParserDialog->pal5Scene = static_cast<SpriteScene*>(ui->gfxView5->scene());
    instListParserDialog->pal6Scene = static_cast<SpriteScene*>(ui->gfxView6->scene());
    instListParserDialog->pal7Scene = static_cast<SpriteScene*>(ui->gfxView7->scene());
    AnimationObject ao;
    createAnimObjectFromGui(ao);
    instListParserDialog->setCommands(ao.getCommandList());
    instListParserDialog->clearInstructionList();
    instListParserDialog->show();
    /**
    AnimationObject myAss;
    createAnimObjectFromGui(myAss);
    AnimationObject parsed = parseInstructionList(romData, myAss.getCommandList(), 0xAA, 0xAAE461, 0xAAE57F);
    myAss = AnimationObject();
    myAss.setCommandList(parsed.getCommandList());

    int max = parsed.spritemapCount();
    for (int i=0; i < max; i++)
    {
        auto what = parsed.getSpritemap(i);
        myAss.addSpritemap(what);
    }
    max = parsed.getInstructionList(0).second.count();
    for (int i=0; i < max; i++)
    {
        myAss.addInstructionList(parsed.getInstructionList(i));
    }

    loadSpritemaps(myAss);
    loadCodeCommands(myAss);
    loadInstructionLists(myAss);**/
}

void MainWindow::actLoadGfxPage0File()
{
    std::vector<uint8_t> data;

    QString file = "";
    file = QFileDialog::getOpenFileName(this, tr("Select .gfx file"), "./", tr("GFX (*.gfx)"));
    if (file.isEmpty())
    {
        std::cout << "No file chosen";
        return;
    }

    //Load file
    try {
        RomHandler::loadFile(file.toStdString(), data);
    } catch (std::string &e) {
        std::cerr << e << std::endl;
        return;
    }

    //GFX data to animation object
    animationObject.setGfxData(data, 0x0000);

    for (int i=0; i < 8; i++)
    {
        QGraphicsView* gfxViews[] = {ui->gfxView0, ui->gfxView1, ui->gfxView2, ui->gfxView3, ui->gfxView4, ui->gfxView5, ui->gfxView6, ui->gfxView7};
        QImage newGfx = loadNewGfx(data, paletteData, i, QPoint(0, 0x00));

        //Render gfx view
        gfxViews[i]->setBackgroundBrush(Qt::blue);
        static_cast<SpriteScene*>(gfxViews[i]->scene())->setGfxSheet(newGfx);
        static_cast<SpriteScene*>(gfxViews[i]->scene())->clear(); //Delete old image
        gfxViews[i]->scene()->addPixmap(QPixmap::fromImage(newGfx.copy(0,0, //Draw gfx. x,y
                                                                                            0x100,    //width
                                                                                            0x200    //height
                                                                         )));
        //gfxViews[i]->setSceneRect(ui->gfxView0->scene()->itemsBoundingRect());
        gfxViews[i]->setSceneRect(ui->gfxView0->rect());
    }
    redrawSpritemap();
}


std::vector<TilesetTile> MainWindow::loadGraphicsSheet(std::vector<uint8_t> &data, uint32_t pointer, bool decompress)
{
    Tileset spriteGraphics;
    std::vector<TilesetTile> sheet = spriteGraphics.parseTilesheet(data, pointer, decompress);
    sheet.resize(0x200);    
    return sheet;
}

std::vector<uint16_t> MainWindow::loadPalette(std::vector<uint8_t> &data, uint32_t pointer, bool decompress)
{
    std::vector<uint16_t> result;

    Tileset spriteGraphics;
    result = spriteGraphics.parsePalette(data, pointer, decompress);

    return result;
}

void MainWindow::createGraphics(const AnimationObject &animObject)
{
    std::vector<uint8_t> gfxData = animObject.getGfxData();
    std::array<uint16_t, 128> palData = animObject.getPaletteData();
    std::copy(palData.begin(), palData.end(), paletteData.begin()); //TODO: Get rid of the static var

    for(int i=0; i < 8; i++)
    {
        QImage sheetImage = loadGraphicsImage(gfxData, 0, paletteData, false, i);

        QGraphicsView* gfxViews[] = {ui->gfxView0, ui->gfxView1, ui->gfxView2, ui->gfxView3, ui->gfxView4, ui->gfxView5, ui->gfxView6, ui->gfxView7};

        QPainter sheetPaint(&this->sheetImage);
        sheetPaint.setCompositionMode(QPainter::CompositionMode_Source);
        sheetPaint.drawImage(0,0, sheetImage);

        //Draw copies of the gfx sheet, for drawing large sprites
        sheetPaint.drawImage( 0x80, 0, sheetImage, 0, 8 ); //Right
        sheetPaint.drawImage( 0, 0x200, sheetImage );      //Below
        sheetPaint.drawImage( 0x80, 0x1F8, sheetImage );   //Right+below

        static_cast<SpriteScene*>(gfxViews[i]->scene())->clear();
        static_cast<SpriteScene*>(gfxViews[i]->scene())->setGfxSheet(this->sheetImage);
        gfxViews[i]->scene()->addPixmap(QPixmap::fromImage(this->sheetImage.copy(0,0, //Draw gfx. x,y
                                                                                            0x200,    //width
                                                                                            0x100    //height
                                                                                            )));
        gfxViews[i]->setSceneRect(0, 0, 128, 256);
    }
}

void MainWindow::loadSpritemaps(const AnimationObject &animObject)
{
    ui->spritemapList->clear();
    ui->spriteIndexList->clear();

    QString name;
    QPair<QString, QList<Sprite*>> spritedata;
    QList<Sprite*> spritemap;
    QListWidgetItem* mapItem = nullptr;

    uint max = animObject.spritemapCount();
    for (uint i=0; i < max; i++)
    {
        spritedata = animObject.getSpritemap(i);
        name = spritedata.first;
        spritemap = spritedata.second;

        mapItem = new QListWidgetItem();
        ui->spritemapList->insertItem(ui->spritemapList->count(), mapItem);
        mapItem->setText(name);
        mapItem->setData(Qt::UserRole, QVariant::fromValue(spritemap));
        mapItem->setFlags(mapItem->flags() | Qt::ItemIsEditable);
    }

    //Select first spritemap, if it exists
    ui->spritemapList->setCurrentRow(0);
}

void MainWindow::loadInstructionLists(const AnimationObject &animObject)
{
    ui->instructionListTable->clearContents();
    ui->instructionListTable->setRowCount(0);

    QPair<QString, QList<QPair<QString, QString>>> list;
    bool ok;
    QPair<QString, QString> instruction;
    QString header, first;
    int timer;

    uint lists = animObject.instructionListCount();
    for (uint i=0; i < lists; i++)
    {
        list = animObject.getInstructionList(i);

        uint instrs = list.second.size();
        for (uint j=0; j < instrs; j++)
        {
            instruction = list.second[j];
            first = instruction.first;
            header = "Execute";
            timer = QString(instruction.first).toInt(&ok, 10);
            if (ok) //Hex number = draw instruction
            {
                header = "Draw";
                first = QString::number(timer, 16).rightJustified(4, '0').toUpper();
            }
            ui->instructionListTable->insertRow(j);
            ui->instructionListTable->setVerticalHeaderItem(j, new QTableWidgetItem(header));
            ui->instructionListTable->setItem(j, 0, new QTableWidgetItem(first));
            ui->instructionListTable->setItem(j, 1, new QTableWidgetItem(instruction.second));
        }
    }
}

void MainWindow::loadCodeCommands(const AnimationObject &animObject)
{
    ui->instKnownCommands->clearContents();
    ui->instKnownCommands->setRowCount(0);

    QList<QList<QString>> commandList = animObject.getCommandList();
    QList<QString> command;

    for (uint i=0; i < commandList.count(); i++)
    {
        command = commandList[i];
        ui->instKnownCommands->insertRow(i);
        ui->instKnownCommands->setVerticalHeaderItem(i, new QTableWidgetItem( QString("Command %1").arg(i) ));
        for (uint j=0; j < 3; j++)
        {
            ui->instKnownCommands->setItem(i, j, new QTableWidgetItem(command[j]));
        }
    }
}

QImage MainWindow::loadGraphicsImage(std::vector<uint8_t> &data, uint32_t gfxPointer, uint32_t palPointer, bool gfxCompressed, bool palCompressed, uint8_t paletteIndex)
{
    std::vector<uint16_t> palette = loadPalette(data, palPointer, palCompressed);
    return loadGraphicsImage(data, gfxPointer, palette, gfxCompressed, paletteIndex);
}

QImage MainWindow::loadGraphicsImage (std::vector<uint8_t> &data, uint32_t gfxPointer, std::vector<uint16_t> &palette, bool gfxCompressed, uint8_t paletteIndex)
{
    QImage result;

    std::vector<TilesetTile> sheet = loadGraphicsSheet(data, gfxPointer, gfxCompressed);

    Tileset realGfx = Tileset(std::vector<TilesetBlock>(), sheet, palette);

    std::vector<QImage> *sheetGfx = Tileset::makeTilesheetGraphics(realGfx, Tileset(), paletteIndex);
    //if (gfxCompressed) sheetGfx->resize(0x100);

    result = Tileset::makeTilesheetImage(*sheetGfx);

    return result;
}

void MainWindow::loadTilesheet(int paletteIndex)
{
    uint32_t gfxPointer = 0;
    uint32_t palPointer = 0;
    bool gfxCompressed = false;

    spritemapScene->clear();

    //Page 0
    gfxPointer = LoRom::snes2pc(ui->gfxPointerPage0->text().toInt(nullptr,16));
    palPointer = LoRom::snes2pc(ui->palPointerLorom->text().toInt(nullptr,16));
    gfxCompressed = ui->checkGfxCompressed->isChecked();

    std::vector<uint8_t> gfxData(0x2000);
    std::vector<uint8_t> rawData(0x8000);
    size_t srcSize;
    size_t decompressedSize = 0x2000;
    std::copy(romData.begin()+gfxPointer, romData.begin()+gfxPointer+0x2000, gfxData.begin());
    std::copy(romData.begin()+gfxPointer, romData.begin()+gfxPointer+0x2000, rawData.begin());
    if (gfxCompressed) decompressedSize = Compresch_Metroid::Decompress(gfxData.data(), 0x2000, rawData.data(), srcSize);
    rawData.resize(decompressedSize < 0x4000 ? decompressedSize : 0x4000);
    animationObject.setGfxData(rawData, 0);

    //paletteData = loadPalette(romData, palPointer, false);
    QImage sheetImage = loadGraphicsImage(romData, gfxPointer, paletteData, gfxCompressed, paletteIndex);

    sheetImage = sheetImage.copy(0,0, 0x80, 0x80 * (gfxCompressed ? 2 : 1));
    sheetImage = sheetImage.copy(0, 0, 0x100, 0x200);

    QGraphicsView* gfxViews[] = {ui->gfxView0, ui->gfxView1, ui->gfxView2, ui->gfxView3, ui->gfxView4, ui->gfxView5, ui->gfxView6, ui->gfxView7};

    QPainter sheetPaint(&this->sheetImage);
    //gfxViews[paletteIndex]->setBackgroundBrush(Qt::blue);
    sheetPaint.setCompositionMode(QPainter::CompositionMode_Source);
    sheetPaint.drawImage(0,0, sheetImage);
    int nextY = sheetImage.height() / 4;

    //Page 1
    QImage sheetImage2;
    if (!gfxCompressed) //Only load page1 if page0 gfx wasn't compressed
    {
        gfxCompressed = ui->checkGfxPage1Compressed->isChecked();
        gfxPointer = LoRom::snes2pc(this->ui->gfxPointerPage1->text().toInt(nullptr,16));
        sheetImage2 = loadGraphicsImage(romData, gfxPointer, palPointer, gfxCompressed, false, paletteIndex);
        sheetPaint.drawImage(0, nextY, sheetImage2);

        std::copy(romData.begin()+gfxPointer, romData.begin()+gfxPointer+0x2000, gfxData.begin());
        std::copy(romData.begin()+gfxPointer, romData.begin()+gfxPointer+0x2000, rawData.begin());
        if (gfxCompressed) Compresch_Metroid::Decompress(gfxData.data(), 0x2000, rawData.data(), srcSize);
        animationObject.setGfxData(rawData, 0x2000);
    }

    //Draw copies of the gfx sheet, for drawing large sprites
    sheetPaint.drawImage( 0x80, 0, sheetImage, 0, 8 );
    sheetPaint.drawImage( 0, 0x100, sheetImage );
    sheetPaint.drawImage( 0x80, 0x78, sheetImage2 );
    sheetPaint.drawImage( 0x80, 0xF8, sheetImage );

    static_cast<SpriteScene*>(gfxViews[paletteIndex]->scene())->clear();
    static_cast<SpriteScene*>(gfxViews[paletteIndex]->scene())->setGfxSheet(this->sheetImage);
    //connect(ui->buttonLoadSpritemap, &QPushButton::released, this, &MainWindow::actLoadGfx);
    gfxViews[paletteIndex]->scene()->addPixmap(QPixmap::fromImage(this->sheetImage.copy(0,0, //Draw gfx. x,y
                                                                                        0x200,    //width
                                                                                        0x100    //height
                                                                     )));
    //gfxViews[paletteIndex]->setSceneRect(ui->gfxView0->scene()->itemsBoundingRect());
    //gfxViews[paletteIndex]->setSceneRect(ui->gfxView0->rect());
    gfxViews[paletteIndex]->setSceneRect(0, 0, 128, 256);
}

/**
 * @brief MainWindow::setSpriteListItemText
 * @param sprite Sprite to get data from
 * @param listItem List item to set text of
 * @param i Sprite index
 */
void MainWindow::setSpriteListItemText(const Sprite *sprite, QListWidgetItem* listItem, uint16_t i)
{
    QString flags = "";
    flags.append( sprite->isFlipX() ? 'H' : 'h' );
    flags.append( sprite->isFlipY() ? 'V' : 'v' );
    flags.append( sprite->isLarge() ? 'L' : 's' );

    listItem->setText( QString("Sprite %1 X:%2 Y:%3 Tile:%4 Pri:%5 Pal:%6 Flags:%7")
                       .arg( QString::number( i,16 ).rightJustified(2,'0') )
                       .arg( QString::number( sprite->getX(), 16).rightJustified(3, '0') )
                       .arg( QString::number( sprite->getY(), 16).rightJustified(2, '0') )
                       .arg( QString::number( sprite->getTileIndex(), 16).rightJustified(3, '0') )
                       .arg( QString::number( sprite->getPriority(), 16) )
                       .arg( QString::number( sprite->getPalette(), 16) )
                       .arg(flags)
                       );
}

QList<Sprite*> MainWindow::parseSpritemap(std::vector<uint8_t> const &rom, uint32_t mapPointer)
{
    uint16_t spriteCount = (romData[mapPointer+1] << 8) | romData[mapPointer]; //TODO: Check sanity
    mapPointer += 2;

    QList<Sprite*> map;
    Sprite* sprite = nullptr;
    for (uint16_t i=0; i < spriteCount; i++)
    {
        sprite = parseSprite(rom, mapPointer);
        if (nullptr == sprite) continue;

        //map.append(sprite->raw());
        map.prepend(sprite->raw());
        mapPointer += 5;
    }

    return map;
}

void MainWindow::actLoadSpritemap()
{
    if (romData.empty()) return;

    //First clear everything
    bool block = ui->viewSpritemap->scene()->blockSignals(true);
    ui->viewSpritemap->scene()->clear();
    ui->spriteIndexList->clear();
    ui->viewSpritemap->scene()->blockSignals(block);

    //Select current spritemap to edit. If there are none, create one
    QListWidgetItem *spritemapListItem = ui->spritemapList->currentItem();
    if (nullptr == spritemapListItem && ui->spritemapList->count() == 0)
        spritemapListItem = new QListWidgetItem("Frame 0", ui->spritemapList);

    //Load spritemap
    uint32_t spritePointer = LoRom::snes2pc(ui->sprMapPointerLorom->text().toInt(nullptr,16));
    uint16_t spriteCount = (romData[spritePointer+1] << 8) | romData[spritePointer]; //TODO: Check sanity

    spritePointer += 2;
    ui->spriteIndexList->clear();

    QList<Sprite*> map;
    QListWidgetItem *listItem = nullptr;
    for (uint16_t i = 0; i < spriteCount; i++)
    {
        Sprite* sprite = nullptr;
        sprite = addSprite(romData, spritePointer);
        if (sprite == nullptr) continue;

        map.append(sprite->raw());
        listItem = new QListWidgetItem();
        ui->spriteIndexList->insertItem(ui->spriteIndexList->count(), listItem);
        setSpriteListItemText(sprite, listItem, i);
        listItem->setData(Qt::UserRole, QVariant::fromValue(sprite) );

        spritePointer += 5;
    }
    QPair<QString, QList<Sprite*>> spritemap = QPair<QString, QList<Sprite*>>(listItem->text(), map);
    //spritemapListItem = new QListWidgetItem(spritemap.first, ui->spritemapList);
    spritemapListItem->setFlags(listItem->flags() | Qt::ItemIsEditable);
    spritemapListItem->setData(Qt::UserRole, QVariant::fromValue(spritemap.second));
    ui->spritemapList->setCurrentItem(spritemapListItem);

    //Spritemap loaded, set Z values
    setSpritemapZValues();

    return;
}
/*void MainWindow::actLoadSpritemapOldDeprecatedRemoveThis()
{
    //TODO: Remove this entire function
    if (romData.empty()) return;

    //First clear everything
    bool block = ui->viewSpritemap->scene()->blockSignals(true);
    static_cast<SpriteScene*>(ui->gfxView0->scene())->clear();
    static_cast<SpriteScene*>(ui->gfxView1->scene())->clear();
    static_cast<SpriteScene*>(ui->gfxView2->scene())->clear();
    static_cast<SpriteScene*>(ui->gfxView3->scene())->clear();
    static_cast<SpriteScene*>(ui->gfxView4->scene())->clear();
    static_cast<SpriteScene*>(ui->gfxView5->scene())->clear();
    static_cast<SpriteScene*>(ui->gfxView6->scene())->clear();
    static_cast<SpriteScene*>(ui->gfxView7->scene())->clear();
    ui->viewSpritemap->scene()->clear();
    ui->spriteIndexList->clear();
    ui->spritemapList->clear();
    ui->viewSpritemap->scene()->blockSignals(block);

    //Load tilesheets
    for (int i=0; i < 8; i++)
        loadTilesheet(i);


    //Graphics loaded. Load spritemap
    uint32_t spritePointer = LoRom::snes2pc(ui->sprMapPointerLorom->text().toInt(nullptr,16));
    uint16_t spriteCount = (romData[spritePointer+1] << 8) | romData[spritePointer]; //TODO: Check sanity

    spritePointer += 2;
    QListWidgetItem* listItem = nullptr;
    ui->spriteIndexList->clear();

    QList<Sprite*> map;
    for (uint16_t i = 0; i < spriteCount; i++)
    {
        Sprite* sprite = nullptr;
        sprite = addSprite(romData, spritePointer);
        if (sprite == nullptr) continue;

        map.append(sprite->raw());
        listItem = new QListWidgetItem(ui->spriteIndexList);
        setSpriteListItemText(sprite, listItem, i);
        listItem->setData(Qt::UserRole, QVariant::fromValue(sprite) );

        spritePointer += 5;
    }
    QPair<QString, QList<Sprite*>> spritemap = QPair<QString, QList<Sprite*>>("Frame0", map);
    listItem = new QListWidgetItem(spritemap.first, ui->spritemapList);
    listItem->setFlags(listItem->flags() | Qt::ItemIsEditable);
    listItem->setData(Qt::UserRole, QVariant::fromValue(spritemap.second));
    ui->spritemapList->setCurrentItem(listItem);

    //TEST REMOVE THIS SHIT
    QList<Sprite*> map2;
    for (Sprite* sprite : map)
    {
        map2.append(sprite->raw());
    }
    //map2.removeFirst();
    //map2.removeLast();
    for (Sprite* sprite : map2)
    {
        sprite->setX(sprite->getX()-0x60);
        sprite->setY(sprite->getY()-0x40);
        sprite->setPosition(QPoint(sprite->getX()-0x60, sprite->getY()-0x40));
        sprite->flipX();
        sprite->flipY();
    }
    spritemap = QPair<QString, QList<Sprite*>>("testFr", map2);
    listItem = new QListWidgetItem(spritemap.first, ui->spritemapList);
    listItem->setFlags(listItem->flags() | Qt::ItemIsEditable);
    listItem->setData(Qt::UserRole, QVariant::fromValue(spritemap.second));
    //REMOVE ABOVE IDIOT

    //Spritemap loaded, set Z values
    setSpritemapZValues();

    //Setup ui stuff
    ui->comboPalette->clear();
    for (int i=0; i < 8; i++)
        ui->comboPalette->addItem(QString::number(i));

    ui->comboPriority->clear();
    for (int i=0; i < 4; i++)
        ui->comboPriority->addItem(QString::number(i));
}*/

void MainWindow::actLoadPalette()
{
    if (romData.empty())
    {
        QMessageBox::information(this, "Info", "No ROM loaded");
        return;
    }

    auto data = romData;
    uint32_t palPointer = LoRom::snes2pc(ui->palPointerLorom->text().toInt(nullptr, 16));
    bool decompress = ui->checkPalCompressed->isChecked();

    paletteData = loadPalette(data, palPointer, decompress);
}

void MainWindow::actLoadGfx()
{
    if (romData.empty()) return;
    //Load tilesheets
    for (int i=0; i < 8; i++)
        loadTilesheet(i);
}

void MainWindow::enablePage1(bool enable)
{
    ui->checkGfxPage1Compressed->setEnabled(!enable);
    ui->gfxPointerPage1->setEnabled(!enable);
}

void MainWindow::changeBackdropColor()
{
    QColor result = QColorDialog::getColor(ui->viewSpritemap->scene()->backgroundBrush().color(), this);
    if (!result.isValid()) return;

    ui->viewSpritemap->scene()->setBackgroundBrush(QBrush(result));
    ui->gfxView0->setBackgroundBrush(result);
    ui->gfxView1->setBackgroundBrush(result);
    ui->gfxView2->setBackgroundBrush(result);
    ui->gfxView3->setBackgroundBrush(result);
    ui->gfxView4->setBackgroundBrush(result);
    ui->gfxView5->setBackgroundBrush(result);
    ui->gfxView6->setBackgroundBrush(result);
    ui->gfxView7->setBackgroundBrush(result);
}


/**
 * @brief setSpritemapZValues Set the Z value for each sprite
 */
void MainWindow::setSpritemapZValues()
{
    Sprite* sprite = nullptr;
    int drawOrder = ui->radioDrawOrderDescend->isChecked() ? -1 : 1;
    int items = ui->spriteIndexList->count();
    for (int i=0; i < items; i++)
    {
        sprite = ui->spriteIndexList->item(i)->data(Qt::UserRole).value<Sprite*>();
        if (sprite) sprite->setZValue(i * drawOrder);
    }
}

void MainWindow::redrawSpritemap()
{
    Sprite* sprite = nullptr;
    int items = ui->spriteIndexList->count();
    for (int i=0; i < items; i++)
    {
        sprite = ui->spriteIndexList->item(i)->data(Qt::UserRole).value<Sprite*>();
        if (nullptr == sprite) continue;
        sprite->setPixmap(getSpriteGraphic(sprite));
    }
}

void MainWindow::actShowSpritemapAsm()
{
    QTextEdit *textView = new QTextEdit(this);
    textView->setWindowTitle("Spritemap ASM");
    textView->setWindowFlag(Qt::Window);
    textView->setWindowModality(Qt::NonModal);
    textView->setText( getSpritemapAsm() );
    textView->show();
}

QString MainWindow::getSpritemapAsm()
{
    QString result = "";
    Sprite* sprite;
    QListWidgetItem* item;

    result = QString("DW $%1\n").arg(QString::number(ui->spriteIndexList->count(), 16).rightJustified(4, '0').toUpper());
    for (int i=0; i < ui->spriteIndexList->count(); i++)
    {
        item = ui->spriteIndexList->item(i);
        if (nullptr == item) continue;

        sprite = item->data(Qt::UserRole).value<Sprite*>();
        if (nullptr == sprite) continue;

        result += sprite->toAsmString();
        result += "\n";
    }

    return result;
}

uint16_t readWord(const std::vector<uint8_t> &data, uint32_t offset)
{
    if (offset+2 >= data.size()) return 0;
    return (data[offset+1] << 8) | (data[offset]);
}

/**
     * @brief parseInstructionList Parses instruction list from rom
     * @param rom Rom
     * @param commands Known commands used in instruction list
     * @param mapBank Bank spritemaps are in
     * @param start Start of instruction list, pc
     * @param end End of instruction list, pc
     * @return AnimationObject containing the instruction list
     */
AnimationObject MainWindow::parseInstructionList(const std::vector<uint8_t> &rom, const QList<QList<QString>> &commands, uint8_t mapBank, uint32_t start, uint32_t end)
{
    AnimationObject result;
    result.setCommandList(commands);
    QPair<QString, QList<QPair<QString, QString>>> list;
    list.first = QString("Spritemap_%1").arg(QString::number(LoRom::pc2snes(start), 16).toUpper());
    QPair<QString, QString> instruction;
    QString str;
    uint16_t cmd, arg;
    uint32_t currentPtr = start;
    uint32_t sprMapPtr;

    uint8_t instrBank = start >> 16;

    QPair<QString, QList<Sprite*>> spritemap;
    QList<QPair<QString, QList<Sprite*>>> spritemapList;
    QList<uint16_t> parsedSpritemaps;

    QList<QPair<int, int>> lineOffsets; QPair<int, int> lineOffset; //Line number, pointer to rom
    QList<QList<QString>> argTypesList;
    for (int i=0; i < commands.count(); i++)
    {
        QStringList format = commands[i][1].split(' ');
        QList<QString> sizes;
        for (int j=0; j < format.count(); j++)
        {
            QString s = format[j];
            if (!s.startsWith('%')) continue;
            sizes.push_back(s);
        }
        argTypesList.push_back(sizes);
    }

    int currentLine = -1;
    while (currentPtr < end)
    {
        if (!instruction.first.isEmpty()) list.second.push_back(instruction);
        currentLine++;
        cmd = readWord(rom, LoRom::snes2pc(currentPtr));
        lineOffset.first = currentLine;
        lineOffset.second = currentPtr;
        lineOffsets.push_back(lineOffset);
        currentPtr += 2;
        if (cmd < 0x8000) //Draw instruction
        {
            instruction.first = QString::number(cmd, 10);//.rightJustified(4, '0').toUpper();
            cmd = readWord(rom, LoRom::snes2pc(currentPtr));
            currentPtr += 2;
            str = QString("Frame_%1").arg(QString::number(cmd, 16).rightJustified(4, '0').toUpper());
            instruction.second = str;
            if (parsedSpritemaps.contains(cmd)) continue;
            //New spritemap, add to list
            spritemap = QPair<QString, QList<Sprite*>>();
            spritemap.first = str;
            sprMapPtr = LoRom::snes2pc( ((mapBank<<16)&0xFF0000) | cmd);
            spritemap.second = parseSpritemap(rom, sprMapPtr);
            spritemapList.push_back(spritemap);
            parsedSpritemaps.push_back(cmd);
            continue;
        }
        else //Code instruction, currentPtr points to arguments
        {
            QStringList argStrings;
            bool ok = false;
            str = QString::number(cmd, 16).rightJustified(4, '0').toUpper();
            for (int i=0; i < commands.count(); i++)
            {
                if (commands[i].last().compare(str) == 0)
                {
                    //Known command
                    ok = true;
                    instruction.first = commands[i][0];
                    QList<QString> argTypes = argTypesList[i];
                    for (int j=0; j < argTypes.count(); j++)
                    {
                        QString type = argTypes[j];
                        if (type.compare("%hb") == 0)
                        {
                            //Byte
                            str = QString::number(rom[LoRom::snes2pc(currentPtr)]);
                            argStrings.push_back(str);
                            currentPtr += 1;
                            continue;
                        }
                        if (type.compare("%hw") == 0)
                        {
                            //Word
                            str = QString::number(readWord(rom, LoRom::snes2pc(currentPtr)));
                            argStrings.push_back(str);
                            currentPtr += 2;
                            continue;
                        }
                        if (type.compare("%hl") == 0)
                        {
                            //Long TODO: Implement
                            argStrings.push_back("%hl not supported");
                            currentPtr += 3;
                            continue;
                        }
                        if (type.compare("%hd") == 0)
                        {
                            //Double TODO: Implement
                            argStrings.push_back("%hd not supported");
                            currentPtr += 4;
                            continue;
                        }
                        if (type.compare("%instr") == 0)
                        {
                            //Label to instruction list
                            arg = readWord(rom, LoRom::snes2pc(currentPtr));
                            int targetOffset = ( (instrBank << 16)&0xFF0000) | arg ;
                            str = "Invalid target line";
                            for (QPair<int,int> &offset: lineOffsets)
                            {
                                if (offset.second == targetOffset)
                                {
                                    str = QString::number(offset.first);
                                }
                            }
                            argStrings.push_back(str);
                            currentPtr += 2;
                            continue;
                        }
                    }
                    break;
                }
            }
            instruction.second = argStrings.join(',');
            if (ok) continue;
            //Unknown command
            else
            {
                std::cerr << "Unknown unhandled command: " << str.toStdString() << std::endl;
            }
        }
    }
    //Push last instruction
    list.second.push_back(instruction);

    for (int i=0; i < spritemapList.count(); i++)
        result.addSpritemap(spritemapList[i]);
    result.addInstructionList(list);
    return result;
}

int MainWindow::getSpriteListIndexOfSpriteObject(Sprite *sprite)
{
    for (auto i=0; i < ui->spriteIndexList->count(); i++)
    {
        if (qvariant_cast<Sprite*>
                (ui->spriteIndexList->item(i)->data(Qt::UserRole)) == sprite)
        {
            return i;
        }
    }

    return -1; //Error
}

void MainWindow::setSpriteListIndexViaSpriteObject(Sprite *sprite)
{
    for (auto i=0; i < ui->spriteIndexList->count(); i++)
    {
        if (qvariant_cast<Sprite*>
                (ui->spriteIndexList->item(i)->data(Qt::UserRole)) == sprite)
        {
            //ui->spriteIndexList->clearSelection();
            //ui->spriteIndexList->setCurrentRow(i);
            ui->spriteIndexList->item(i)->setSelected(true);
            return;
        }
    }
}

void MainWindow::on_spinPosX_valueChanged(int pos)
{
    bool ok = false;
    int delta = pos - ui->spinPosX->property("oldValue").toInt(&ok);
    ui->spinPosX->setProperty("oldValue", pos);
    if (!ok) delta = 0;

    auto selectedItems = ui->spriteIndexList->selectedItems();
    if (selectedItems.empty()) return;

    for (QListWidgetItem* tmp : selectedItems)
    {
        Sprite* sprite = qvariant_cast<Sprite*>(tmp->data(Qt::UserRole));
        if (!sprite) continue;

        sprite->setPosition(QPoint(sprite->getX() + delta, sprite->getY()));
        sprite->setPixmap(getSpriteGraphic(sprite));

        setSpriteListItemText(sprite, tmp, ui->spriteIndexList->row(tmp));
    }
}

void MainWindow::on_spinPosY_valueChanged(int pos)
{
    bool ok = false;
    int delta = pos - ui->spinPosY->property("oldValue").toInt(&ok);
    ui->spinPosY->setProperty("oldValue", pos);
    if (!ok) delta = 0;

    auto selectedItems = ui->spriteIndexList->selectedItems();
    if (selectedItems.empty()) return;

    for (QListWidgetItem* tmp : selectedItems)
    {
        Sprite* sprite = qvariant_cast<Sprite*>(tmp->data(Qt::UserRole));
        if (!sprite) continue;

        sprite->setPosition(QPoint(sprite->getX(), sprite->getY() + delta));
        sprite->setPixmap(getSpriteGraphic(sprite));

        setSpriteListItemText(sprite, tmp, ui->spriteIndexList->row(tmp));
    }
}

void MainWindow::on_spinTileNumber_valueChanged(int val)
{
    auto items = ui->spriteIndexList->selectedItems();
    if (items.count() != 1) return; //Only change tile if exactly one item is selected

    auto tmp = items[0];
    if (!tmp) return;

    Sprite* sprite = qvariant_cast<Sprite*>(tmp->data(Qt::UserRole));
    if (!sprite) return;

    sprite->setTileIndex(val);
    sprite->setPixmap( getSpriteGraphic(sprite) );

    setSpriteListItemText(sprite, tmp, ui->spriteIndexList->row(tmp));
}

/**
 * @brief MainWindow::on_checkLargeSprite_toggled Changes sprite size
 * @param checked False = small. True = large.
 */
void MainWindow::on_checkLargeSprite_toggled(bool checked)
{
    auto items = ui->spriteIndexList->selectedItems();
    if (items.count() != 1) return; //Only change size if exactly one item is selected

    auto tmp = items[0];
    if (!tmp) return;

    Sprite* sprite = qvariant_cast<Sprite*>(tmp->data(Qt::UserRole));
    if (!sprite) return;

    sprite->setIsLarge(checked);
    sprite->setPixmap( getSpriteGraphic(sprite) );

    setSpriteListItemText(sprite, tmp, ui->spriteIndexList->row(tmp));
}

void MainWindow::on_comboPalette_currentIndexChanged(int index)
{
    auto selectedItems = ui->spriteIndexList->selectedItems();
    if (selectedItems.empty()) return;

    for (QListWidgetItem* tmp : selectedItems)
    {
        Sprite* sprite = qvariant_cast<Sprite*>(tmp->data(Qt::UserRole));
        if (!sprite) continue;

        sprite->setPalette(index);
        sprite->setPixmap(getSpriteGraphic(sprite));

        setSpriteListItemText(sprite, tmp, ui->spriteIndexList->row(tmp));
    }
}

void MainWindow::on_comboPriority_currentIndexChanged(int index)
{
    auto selectedItems = ui->spriteIndexList->selectedItems();
    if (selectedItems.empty()) return;

    for (QListWidgetItem* tmp : selectedItems)
    {
        Sprite* sprite = qvariant_cast<Sprite*>(tmp->data(Qt::UserRole));
        if (!sprite) continue;

        sprite->setPriority(index);
        sprite->setPixmap(getSpriteGraphic(sprite));

        setSpriteListItemText(sprite, tmp, ui->spriteIndexList->row(tmp));
    }
}

void MainWindow::on_buttonDelSprite_released()
{
    //int index = ui->spriteIndexList->currentRow();
    //if (index < 0 || index > ui->spriteIndexList->count()) return;

    //Selected sprite indices
    QList<int> selectedItems;
    for (QListWidgetItem *item: ui->spriteIndexList->selectedItems())
    {
        selectedItems.append(ui->spriteIndexList->row(item));
    }
    std::sort(selectedItems.begin(), selectedItems.end());

    //Delete one sprite at a time while holding Shift
    bool deleteAll = QGuiApplication::keyboardModifiers() & Qt::ShiftModifier;

    //Move the sprite objects from later sprites into earlier ones
    for (int i = selectedItems.count()-1; i >= 0; i--)
    {
        int index = selectedItems[i];
        Sprite* temp = ui->spriteIndexList->item(index)->data(Qt::UserRole).value<Sprite*>();
        for (int n=index; n < ui->spriteIndexList->count()-1; n++)
        {
            ui->spriteIndexList->item(n)->setData(Qt::UserRole,  //To n
                                                  ui->spriteIndexList->item(n+1) ->data(Qt::UserRole));//From n+1
            setSpriteListItemText(ui->spriteIndexList->item(n)->data(Qt::UserRole).value<Sprite*>(), ui->spriteIndexList->item(n), n);
        }
        //Delete the last sprite item (because it's a duplicate of second-to-last now)
        auto last = ui->spriteIndexList->takeItem(ui->spriteIndexList->count()-1);
        if (!last) return; //Should never happen
        delete last;
        //And delete the temp sprite, which is the one that needed to be deleted
        delete temp;
        if (!deleteAll) i = -1; //Break out of loop if not deleting all
    }

    setSpritemapZValues();
}

void MainWindow::addSprite(int tileIndex)
{
    //TODO: check if spritemap editing is enabled (rom is loaded?)

    Sprite* sprite = addSprite();
    if (sprite == nullptr) return;

    sprite->setTileIndex(tileIndex);
    int palette = ui->tabWidget->currentIndex(); //Use palette from current gfx tab
    if (palette < 0 || palette > 7) palette = 0;
    sprite->setPalette(palette);
    sprite->setIsLarge(ui->checkLargeSprite->isChecked());
    sprite->setPixmap(getSpriteGraphic(sprite));

    int priority = ui->comboPriority->currentIndex(); //Use currently selected priority
    if (priority < 0) priority = 1;
    sprite->setPriority(priority);

    sprite->setPosition(QPoint(-64, -64));

    QListWidgetItem* listItem = new QListWidgetItem(ui->spriteIndexList);
    setSpriteListItemText(sprite, listItem, ui->spriteIndexList->count()-1);

    listItem->setData(Qt::UserRole, QVariant::fromValue(sprite) );
    setSpritemapZValues();

    if (ui->spritemapList->count() == 0) //If no spritemap exists, add one and select it
    {
        actAddSpritemap();
        ui->spritemapList->setCurrentRow(0);
    }
}

void MainWindow::on_comboObject_currentIndexChanged(int index)
{
    ui->comboAnim->setModel( qvariant_cast<QStandardItemModel*>(ui->comboObject->currentData(Qt::UserRole)) );
}

void MainWindow::on_comboAnim_currentIndexChanged(int index)
{
    ui->comboFrame->setModel( qvariant_cast<QStandardItemModel*>(ui->comboAnim->currentData(Qt::UserRole)) );
}

void MainWindow::on_comboFrame_currentIndexChanged(int index)
{
    int gfx = ui->comboFrame->currentData(Qt::UserRole).toInt();
    int pal = ui->comboFrame->currentData(Qt::UserRole+1).toInt();
    int spr = ui->comboFrame->currentData(Qt::UserRole+2).toInt();
    int size = ui->comboObject->currentData(Qt::UserRole+1).toInt();
    bool gfxDecomp = ui->comboFrame->currentData(Qt::UserRole+3).toBool();
    bool palDecomp = ui->comboFrame->currentData(Qt::UserRole+4).toBool();
    bool gfxDecomp1 = ui->comboFrame->currentData(Qt::UserRole+6).toBool();
    int gfx1 = ui->comboFrame->currentData(Qt::UserRole+5).toInt();

    ui->gfxPointerPage0->setText(QString::number(gfx,16).toUpper());
    ui->palPointerLorom->setText(QString::number(pal,16).toUpper());
    ui->sprMapPointerLorom->setText(QString::number(spr,16).toUpper());
    ui->comboSpriteSizes->setCurrentIndex(size);
    ui->checkGfxCompressed->setChecked(gfxDecomp);
    ui->checkPalCompressed->setChecked(palDecomp);
    ui->gfxPointerPage1->setText(QString::number(gfx1,16).toUpper());
    ui->checkGfxPage1Compressed->setChecked(gfxDecomp1);
}

/**
 * @brief MainWindow::connectSpriteSignals Called when selection in sprite view changes
 */
void MainWindow::connectSpriteSignals()
{
    QList<QGraphicsItem*> selection = spritemapScene->selectedItems();

    //Disconnect flips
    ui->buttonFlipX->disconnect();
    ui->buttonFlipY->disconnect();

    //Connect flips to selection
    Sprite *sprite = nullptr;
    while (!selection.isEmpty())
    {
        sprite = dynamic_cast<Sprite*>(selection.takeAt(0));
        if (0 == sprite) continue;
        connect(ui->buttonFlipX, &QPushButton::released, sprite, &Sprite::flipX);
        connect(ui->buttonFlipY, &QPushButton::released, sprite, &Sprite::flipY);
    }

    //Connect full spritemap flips
    connect(ui->buttonFlipX, &QPushButton::released, this, &MainWindow::flipSpritemapX);
    connect(ui->buttonFlipY, &QPushButton::released, this, &MainWindow::flipSpritemapY);
}

void MainWindow::selectSpriteListItems()
{
    QList<QGraphicsItem*> selection = spritemapScene->selectedItems();
    Sprite *sprite = nullptr;


    for (int i=0; i < ui->spriteIndexList->count(); i++)
    {
        ui->spriteIndexList->blockSignals(true);
        ui->spriteIndexList->item(i)->setSelected(false);
        ui->spriteIndexList->blockSignals(false);
    }

    while (!selection.isEmpty())
    {
        sprite = dynamic_cast<Sprite*>(selection.takeAt(0));
        if (0 == sprite) continue;

        ui->spriteIndexList->item(getSpriteListIndexOfSpriteObject(sprite))->setSelected(true);
    }
}

void MainWindow::flipSpritemapX()
{
    QList<QGraphicsItem*> selection = spritemapScene->selectedItems();
    if (selection.size() <= 1) return;

    int16_t minX = 0x7FFF;
    int16_t maxX = 0x8000;
    int16_t size = 0;
    int16_t x;
    Sprite *sprite = nullptr;
    while (!selection.isEmpty())
    {
        sprite = dynamic_cast<Sprite*>(selection.takeAt(0));
        if (0 == sprite) continue;
        x = sprite->pos().toPoint().x();
        size = sprite->isLarge() ? 16 : 8; //TODO: Support other sprite sizes than 8/16
        minX = (x < minX) ? x : minX;
        maxX = (x + size > maxX) ? x + size : maxX;
    }

    int16_t dx = 0; //Sprite left edge distance from spritemap left edge

    selection = spritemapScene->selectedItems();
    while (!selection.isEmpty())
    {
        sprite = dynamic_cast<Sprite*>(selection.takeAt(0));
        if (0 == sprite) continue;

        size = sprite->isLarge() ? 16 : 8; //TODO: Support other sprite sizes than 8/16
        dx = sprite->pos().toPoint().x() - minX;
        sprite->setPosition( QPoint(maxX-dx-size, sprite->getY()) );

        int listItemIndex = getSpriteListIndexOfSpriteObject(sprite);
        setSpriteListItemText(sprite, ui->spriteIndexList->item(listItemIndex), listItemIndex);
    }
}

void MainWindow::flipSpritemapY()
{
    QList<QGraphicsItem*> selection = spritemapScene->selectedItems();
    if (selection.size() <= 1) return;

    int16_t minY = 0x7FFF;
    int16_t maxY = 0x8000;
    int16_t size = 0;
    int16_t y;
    Sprite *sprite = nullptr;
    while (!selection.isEmpty())
    {
        sprite = dynamic_cast<Sprite*>(selection.takeAt(0));
        if (0 == sprite) continue;
        y = sprite->pos().toPoint().y();
        size = sprite->isLarge() ? 16 : 8; //TODO: Support other sprite sizes than 8/16
        minY = (y < minY) ? y : minY;
        maxY = (y + size > maxY) ? y + size : maxY;
    }

    int16_t dy = 0; //Sprite top edge distance from spritemap top edge

    selection = spritemapScene->selectedItems();
    while (!selection.isEmpty())
    {
        sprite = dynamic_cast<Sprite*>(selection.takeAt(0));
        if (0 == sprite) continue;

        size = sprite->isLarge() ? 16 : 8; //TODO: Support other sprite sizes than 8/16
        dy = sprite->pos().toPoint().y() - minY;
        sprite->setPosition( QPoint(sprite->getX(), maxY-dy-size) );

        int listItemIndex = getSpriteListIndexOfSpriteObject(sprite);
        setSpriteListItemText(sprite, ui->spriteIndexList->item(listItemIndex), listItemIndex);
    }
}

void MainWindow::on_radioDrawOrderDescend_toggled(bool checked)
{
    setSpritemapZValues();
}

/**
 * @brief MainWindow::on_comboSpriteSizes_activated Change what size small and large sprites are
 * @param index Index to table of accepted sizes
 */
void MainWindow::changeSpriteSize(int index)
{
    if (index < 0 || index > 5) return;

    //8x16, 8x32, 8x64, 16x32, 16x64, 32x64
    uint8_t smallSizes[] = {8, 8, 8, 16, 16, 32};
    uint8_t bigSizes[] = {16, 32, 64, 32, 64, 64};

    QGraphicsView *gfxViews[] = {ui->viewSpritemap, ui->gfxView0, ui->gfxView1, ui->gfxView2, ui->gfxView3, ui->gfxView4, ui->gfxView5, ui->gfxView6, ui->gfxView7};
    SpriteScene *scene;

    for (int i=0; i < 9; i++)
    {
        scene = qobject_cast<SpriteScene*>(gfxViews[i]->scene());
        if (nullptr == scene) return; //TODO: error handling

        scene->setSmallSize(smallSizes[index]);
        scene->setLargeSize(bigSizes[index]);
    }

    //Redraw sprites with new sizes TODO: separate into a function
    QList<QGraphicsItem*> sprites = ui->viewSpritemap->scene()->items();
    Sprite *sprite = nullptr;

    for (int i=0; i < sprites.size(); i++)
    {
        sprite = dynamic_cast<Sprite*>(sprites[i]);
        if (nullptr == sprite) continue;

        sprite->setPixmap( getSpriteGraphic(sprite) );
    }
}

/**
 * @brief MainWindow::on_comboSpriteSizes_activated Change what size small and large sprites are
 * @param index Index to table of accepted sizes
 */
void MainWindow::on_comboSpriteSizes_activated(int index)
{
    changeSpriteSize(index);
}

void MainWindow::on_comboSpriteSizes_currentIndexChanged(int index)
{
    changeSpriteSize(index);
}

/**
 * @brief MainWindow::on_spriteIndexList_itemSelectionChanged Set corresponding sprites as selected
 */
void MainWindow::on_spriteIndexList_itemSelectionChanged()
{
    Sprite* sprite = nullptr;
    Sprite* sprite2 = nullptr;

    spritemapScene->blockSignals(true);
    auto items = ui->viewSpritemap->items();
    for (auto item : items)
    {
        sprite = dynamic_cast<Sprite*>(item);
        if (nullptr == sprite) continue;

        sprite->setSelected(false); //Deselect all
    }

    for (auto item : ui->spriteIndexList->selectedItems())
    {
        sprite = qvariant_cast<Sprite*>(item->data(Qt::UserRole));
        if (nullptr == sprite) continue;

        sprite->setSelected(true); //Select
        sprite2 = sprite;
    }

    if (ui->spriteIndexList->selectedItems().count() > 0) //Display values of newest item in editing boxes TODO: Get this working when drag-selectin in spritemap view
    {
        auto item = ui->spriteIndexList->currentItem();
        sprite = nullptr == item ? nullptr : qvariant_cast<Sprite*>(item->data(Qt::UserRole));
        if (nullptr != sprite)
        {
            ui->spinPosX->blockSignals(true);
            ui->spinPosY->blockSignals(true);
            ui->checkLargeSprite->blockSignals(true);
            ui->spinTileNumber->blockSignals(true);
            ui->comboPriority->blockSignals(true);
            ui->comboPalette->blockSignals(true);

            ui->spinPosX->setValue(sprite->getX()); ui->spinPosX->setProperty("oldValue", sprite->getX());
            ui->spinPosY->setValue(sprite->getY()); ui->spinPosY->setProperty("oldValue", sprite->getY());
            ui->checkLargeSprite->setChecked(sprite->isLarge());
            ui->spinTileNumber->setValue(sprite->getTileIndex());
            ui->comboPriority->setCurrentIndex(sprite->getPriority());
            ui->comboPalette->setCurrentIndex(sprite->getPalette());

            ui->spinPosX->blockSignals(false);
            ui->spinPosY->blockSignals(false);
            ui->checkLargeSprite->blockSignals(false);
            ui->spinTileNumber->blockSignals(false);
            ui->comboPriority->blockSignals(false);
            ui->comboPalette->blockSignals(false);
        }
    }

    spritemapScene->blockSignals(false);
    if (nullptr != sprite2) ////Reselect an item to force spritescene to emit selectionChanged signal
    {
        disconnect(spritemapScene, &SpriteScene::selectionChanged, this, &MainWindow::selectSpriteListItems);
        sprite2->setSelected(false);
        sprite2->setSelected(true);
        connect(spritemapScene, &SpriteScene::selectionChanged, this, &MainWindow::selectSpriteListItems);
    }
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    //Block signals from all tabs
    for (int i=ui->tabWidget->count()-1; i >= 0; i--)
    {
        dynamic_cast<QGraphicsView*>(ui->tabWidget->widget(i)->children()[0])->scene()->blockSignals(true);
    }

    //Allow signals from selected tab only
    dynamic_cast<QGraphicsView*>(ui->tabWidget->widget(index)->children()[0])->scene()->blockSignals(false);
}

void MainWindow::currentSpritemapChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    //Store current spritemap to previous
    QList<Sprite*> spritemap;
    Sprite *sprite = nullptr;

    for (int i=0; i < ui->spriteIndexList->count(); i++)
    {
        sprite = ui->spriteIndexList->item(i)->data(Qt::UserRole).value<Sprite*>();
        if (nullptr == sprite) continue;
        spritemap.append(sprite->raw());
    }

    if (nullptr != previous) previous->setData(Qt::UserRole, QVariant::fromValue(spritemap));

    //Display new spritemap
    if (nullptr == current) return;
    spritemap = current->data(Qt::UserRole).value<QList<Sprite*>>();

    ui->viewSpritemap->scene()->clear();
    ui->spriteIndexList->clear();

    QListWidgetItem* listItem;
    for (int i=0; i < spritemap.size(); i++)
    {
        sprite = spritemap.at(i)->raw();
        listItem = new QListWidgetItem();
        ui->spriteIndexList->insertItem(ui->spriteIndexList->count(), listItem);
        setSpriteListItemText(sprite, listItem, i);
        listItem->setData(Qt::UserRole, QVariant::fromValue(sprite));
        addSprite(sprite);
    }
}

void MainWindow::actAddSpritemap()
{
    QList<Sprite*> map;

    map.append(Sprite(nullptr).raw());
    for (Sprite* sprite : map)
    {
        sprite->setX(sprite->getX()-0x60);
        sprite->setY(sprite->getY()-0x40);
        sprite->setPosition(QPoint(sprite->getX()-0x60, sprite->getY()-0x40));
    }

    if (ui->spritemapList->count() == 0) //No spritemap already existed, get data from current view
    {
        map.clear();
        QList<QGraphicsItem*> sprites = ui->viewSpritemap->items();
        Sprite* sprite = nullptr;

        for (uint i=0; i < sprites.count(); i++)
        {
            sprite = dynamic_cast<Sprite*>(sprites[i]);
            if (nullptr == sprite) continue;
            map.append(sprite->raw());
        }
    }
    QString name = QString("Frame %0").arg(ui->spritemapList->count());
    QPair<QString, QList<Sprite*>> spritemap = QPair<QString, QList<Sprite*>>(name, map);
    QListWidgetItem *listItem = new QListWidgetItem(spritemap.first, ui->spritemapList);
    listItem->setFlags(listItem->flags() | Qt::ItemIsEditable);
    listItem->setData(Qt::UserRole, QVariant::fromValue(spritemap.second));
}

void MainWindow::actDelSpritemap()
{
    int row = ui->spritemapList->currentRow();
    if (row == -1) return;

    QListWidgetItem *item = ui->spritemapList->takeItem(row);
    if (nullptr != item) delete item;
}

void MainWindow::actExportAnimation()
{
    AnimationObject exportObj;
    createAnimObjectFromGui(exportObj);
    QString result = exportObj.exportToAsm();

    auto string = result.toUtf8();

    std::vector<uint8_t> writeThis;
    writeThis.assign(string.begin(), string.end());

    QString outFile = QFileDialog::getSaveFileName(this, tr("Export"), "./", tr("File (*.asm)"));
    if (outFile.isEmpty()) return;
    RomHandler::writeFile(outFile.toStdString(), writeThis);
}

void MainWindow::createAnimObjectFromGui(AnimationObject &saveObj)
{
    //Copy graphics data over
    saveObj.setGfxData(animationObject.getGfxData(), 0);
    saveObj.setPaletteData(animationObject.getPaletteData());
    saveObj.setPaletteData(paletteData); //TODO: Get rid of this one (static var at top of file)

    //Save current spritemap
    QList<Sprite*> spritemap;
    Sprite *sprite = nullptr;
    QList<QGraphicsItem*> items = spritemapScene->items();
    for (int i=items.size()-1; i >= 0; i--)
    {
        sprite = dynamic_cast<Sprite*>(items[i]);
        if (nullptr == sprite) continue;
        spritemap.append(sprite->raw());
    }
    QListWidgetItem* item = ui->spritemapList->currentItem();
    if (nullptr != item) item->setData(Qt::UserRole, QVariant::fromValue(spritemap));

    //TODO: hitboxes

    //Attach spritemaps to save object
    for (int i=0; i < ui->spritemapList->count(); i++)
    {
        QListWidgetItem* item = ui->spritemapList->item(i);
        QPair<QString, QList<Sprite*>> map = QPair<QString, QList<Sprite*>>(item->text(), item->data(Qt::UserRole).value<QList<Sprite*>>());
        saveObj.addSpritemap(map);
    }

    //Instruction lists //TODO: Add support for multiple
    QList<QPair<QString, QString>> instrList;
    QTableWidgetItem* tItem0 = nullptr;
    QTableWidgetItem* tItem1 = nullptr;
    for (int i=0; i < ui->instructionListTable->rowCount(); i++)
    {
        tItem0 = ui->instructionListTable->item(i, 0);
        tItem1 = ui->instructionListTable->item(i, 1);
        instrList.append(QPair<QString, QString>(tItem0->text(), tItem1->text()));
    }
    QPair<QString, QList<QPair<QString, QString>>> namedList;
    namedList.first = "Animation";
    namedList.second = instrList;
    saveObj.addInstructionList(namedList);

    //Code commands
    QList<QList<QString>> commandList;
    QList<QString> command;
    QString str;
    for (int i=0; i < ui->instKnownCommands->rowCount(); i++)
    {
        command.clear();
        for (int j=0; j < ui->instKnownCommands->columnCount(); j++) //Name, arg format, pointer
        {
            str = "";
            tItem0 = ui->instKnownCommands->item(i, j);
            if (nullptr != tItem0) str = tItem0->text();
            command.append(str.isEmpty() ? "" : str);
        }

        commandList.append(command);
    }
    saveObj.setCommandList(commandList);
}

/**
 * @brief MainWindow::actSaveAnimObject Save project to json file
 */
void MainWindow::actSaveAnimObject()
{
    AnimationObject saveObj;

    createAnimObjectFromGui(saveObj);

    //Create JSON doc //TODO: Convert to Qt JSON for a more sensical file?
    nlohmann::json result = saveObj.exportToJson();

    //Write to disk
    QString outFile = QFileDialog::getSaveFileName(this, tr("Save project"), "./", tr("File (*.json)"));
    if (outFile.isEmpty()) return;
    std::ofstream of(outFile.toStdString());
    of << std::setw(1) << result << std::endl;
}

void MainWindow::actLoadAnimObject()
{
    QString inFile = QFileDialog::getOpenFileName(this, tr("Load project"), "./", tr("*.json"));
    if (inFile.isEmpty()) return;

    nlohmann::json json;
    std::ifstream inf(inFile.toStdString());
    inf >> json;

    animationObject = AnimationObject(json);

    createGraphics(animationObject);
    loadSpritemaps(animationObject);
    loadInstructionLists(animationObject);
    loadCodeCommands(animationObject);
}

void MainWindow::actAddInstruction()
{
    //Add a draw command 4 frames for currently selected spritemap
    int row = ui->instructionListTable->currentRow()+1;
    if (row == 0) row = ui->instructionListTable->rowCount();

    ui->instructionListTable->insertRow(row);

    QTableWidgetItem* item = new QTableWidgetItem("Draw"); // Draw/execute
    ui->instructionListTable->setVerticalHeaderItem(row, item);
    //ui->instructionListTable->set

    item = new QTableWidgetItem("0004"); // Timer/command
    ui->instructionListTable->setItem(row, 0, item);

    item = new QTableWidgetItem(ui->instComboSpritemap->currentText()); // Spritemap/arg
    ui->instructionListTable->setItem(row, 1, item);

    //Set new instruction as selected
    ui->instructionListTable->selectRow(row);
    actInstListActivated();
}

void MainWindow::actDelInstruction()
{
    int row = ui->instructionListTable->currentRow();
    if (row == -1) return;

    ui->instructionListTable->removeRow(row);
}

void MainWindow::actSetInstTimer()
{
    int row = ui->instructionListTable->currentRow();
    if (row == -1) return;

    QTableWidgetItem* item = ui->instructionListTable->verticalHeaderItem(row);
    if (item->text().compare("Draw") != 0) return;

    item = ui->instructionListTable->item(row, 0);
    if (nullptr == item) return; //Something has gone terribly wrong

    item->setText(QString::number(ui->instTimer->value(), 16).rightJustified(4, '0').toUpper());
}

void MainWindow::actSetInstSpritemap(int i)
{
    if (i == -1) return;
    int row = ui->instructionListTable->currentRow();
    if (row == -1) return;

    QTableWidgetItem* item = ui->instructionListTable->verticalHeaderItem(row);
    if (item->text().compare("Draw") != 0) return;

    item = ui->instructionListTable->item(row, 1);
    if (nullptr == item) return; //Something has gone terribly wrong

    item->setText(ui->instComboSpritemap->itemText(i));

    //Display spritemap
    ui->spritemapList->setCurrentRow(i);
}

void MainWindow::actInstListActivated()
{
    QList<QTableWidgetItem*> items = ui->instructionListTable->selectedItems();

    for (QTableWidgetItem* item: items)
    {
        int row = item->row();
        int col = item->column();

        if (col != 1) continue; //Return if not spritemap/arg cell
        item = ui->instructionListTable->verticalHeaderItem(row);
        if (nullptr == item) return;
        if (item->text().compare("Draw") == 0)
        {
            //Draw instruction detected, display spritemap and timer
            item = ui->instructionListTable->item(row, col);
            QString sprMapName = item->text();
            ui->instComboSpritemap->setCurrentText(sprMapName);
            int index = ui->instComboSpritemap->currentIndex();
            if (index == -1) return;
            ui->spritemapList->setCurrentRow(index);

            item = ui->instructionListTable->item(row, 0);
            ui->instTimer->setValue( item->text().toInt(nullptr, 16) );

            ui->instListCommandType->setCurrentIndex(0);

            ui->instTimer->setEnabled(true);
            ui->instComboSpritemap->setEnabled(true);
            ui->instComboCommand->setEnabled(false);
            ui->instLineArgument->setEnabled(false);

            break;
        }
        if (item->text().compare("Execute") == 0)
        {
            ui->instListCommandType->setCurrentIndex(1);

            item = ui->instructionListTable->item(row, col);
            ui->instLineArgument->setText(item->text());

            item = ui->instructionListTable->item(row, 0);
            ui->instComboCommand->setCurrentText(item->text());

            ui->instTimer->setEnabled(false);
            ui->instComboSpritemap->setEnabled(false);
            ui->instComboCommand->setEnabled(true);
            ui->instLineArgument->setEnabled(true);
        }
    }
}

/**
 * @brief MainWindow::actSetInstructionTypeGuiElements Enable/disable relevant GUI items. 0=draw, 1=execute
 * @param i Type 0=draw 1=execute
 */
void MainWindow::actSetInstructionTypeGuiElements(int i)
{
    ui->instTimer->setEnabled(i==0);
    ui->instComboSpritemap->setEnabled(i==0);
    ui->instComboCommand->setEnabled(i==1);
    ui->instLineArgument->setEnabled(i==1);
}

/**
 * @brief MainWindow::actSetInstructionType Set instruction type
 * @param i 0=draw 1=execute
 */
void MainWindow::actSetInstructionType(int i)
{
    int row = ui->instructionListTable->currentRow();
    if (row == -1) return;

    QTableWidgetItem* item = ui->instructionListTable->verticalHeaderItem(row);
    item->setText(i==0 ? "Draw" : "Execute");

    if (i==0) //Set to draw
    {
        actSetInstTimer();
        actSetInstSpritemap(ui->instComboSpritemap->currentIndex());
        return;
    }
    if (i==1) //Set to execute
    {
        actSetInstCommand();
        actSetInstArgument();
        return;
    }
}

void MainWindow::actSetInstCommand()
{
    int row = ui->instructionListTable->currentRow();
    if (row == -1) return;

    QTableWidgetItem* item = ui->instructionListTable->verticalHeaderItem(row);
    if (item->text().compare("Execute") != 0) return;

    item = ui->instructionListTable->item(row, 0); //Command
    item->setText(ui->instComboCommand->currentText());
}

void MainWindow::actSetInstArgument()
{
    int row = ui->instructionListTable->currentRow();
    if (row == -1) return;

    QTableWidgetItem* item = ui->instructionListTable->verticalHeaderItem(row);
    if (item->text().compare("Execute") != 0) return;

    item = ui->instructionListTable->item(row, 1); //Argument
    item->setText(ui->instLineArgument->text());
}

void MainWindow::actAddCodeCommand()
{
    ui->instKnownCommands->insertRow(ui->instKnownCommands->rowCount());
    int row = ui->instKnownCommands->rowCount()-1;

    ui->instKnownCommands->setItem(row, 0, new QTableWidgetItem("Name"));
    ui->instKnownCommands->setItem(row, 1, new QTableWidgetItem("Argument format"));
    ui->instKnownCommands->setItem(row, 2, new QTableWidgetItem("Pointer"));
}

void MainWindow::actDeleteCodeCommand()
{
    int row = ui->instKnownCommands->currentRow();
    if (row == -1) return;

    ui->instKnownCommands->removeRow(row);
}

void MainWindow::actDisplayDefaultCommandLoader()
{
    QStringList items;
    items << "Enemies ($A0)" << "Projectiles ($86)" << "Generic sprite objects ($B4)";

    bool ok = false;
    QString item = QInputDialog::getItem(this, "Select default command list", "", items, 0, false, &ok);
    if (!ok || item.isEmpty()) return;

    QList<QList<QString>> list;
    QList<QString> command;

    if (item.compare("Enemies ($A0)") == 0)
    {
        command.append("Goto"); command.append("DW %instr"); command.append("80ED"); list.append(command); command.clear();
        command.append("Dec C & goto if nonzero"); command.append("DW %instr"); command.append("8108"); list.append(command); command.clear();
        command.append("Set C"); command.append("DW %hw"); command.append("8123"); list.append(command); command.clear();
        command.append("Sleep"); command.append(""); command.append("812F"); list.append(command); command.clear();
        command.append("Wait"); command.append("DW %hw"); command.append("813A"); list.append(command); command.clear();
        AnimationObject ao;
        ao.setCommandList(list);
        loadCodeCommands(ao);
        return;
    }
    if (item.compare("Projectiles ($86)") == 0)
    {
        command.append("Sleep"); command.append(""); command.append("8159"); list.append(command); command.clear();
        command.append("Goto"); command.append("DW %instr"); command.append("81AB"); list.append(command); command.clear();
        command.append("Dec C & goto if nonzero"); command.append("DW %instr"); command.append("81C6"); list.append(command); command.clear();
        command.append("Set C"); command.append("DW %hw"); command.append("81D5"); list.append(command); command.clear();
        AnimationObject ao;
        ao.setCommandList(list);
        loadCodeCommands(ao);
        return;
    }
    if (item.compare("Generic sprite objects ($B4)") == 0)
    {
        command.append("Goto"); command.append("DW %instr"); command.append("BD12"); list.append(command); command.clear();
        AnimationObject ao;
        ao.setCommandList(list);
        loadCodeCommands(ao);
        return;
    }
}

void MainWindow::actAcceptInstructionList(AnimationObject in)
{
    if (in.instructionListCount() <= 0) return;
    loadInstructionLists(in);
    if (in.spritemapCount() > 0) loadSpritemaps(in);;
    if (in.commandCount() > 0) loadCodeCommands(in);
}

void MainWindow::actToggleAnimation()
{
    bool start = (nullptr == animator);
    if (start)
    {
        actPlayAnimation();
    }
    else
    {
        actStopAnimation();
    }
}

void MainWindow::actPlayAnimation()
{
    int instList = 0; //TODO: Add support for multiple instruction lists
    AnimationObject playObj;
    createAnimObjectFromGui(playObj);

    animator = new Animator(playObj.getInstructionList(instList).second);
    animator->setInstruction(ui->spinCurrentInstruction->value());
    animator->setCounter(ui->spinCounter->value());
    connectAnimatorSignals();
    animator->start();

    ui->instructionListTable->setEnabled(false);
    ui->pushAddIndstruction->setEnabled(false);
    ui->pushDelInstruction->setEnabled(false);
    ui->instComboCommand->blockSignals(true);
    ui->instComboSpritemap->blockSignals(true);
    ui->instTimer->blockSignals(true);
    ui->instLineArgument->blockSignals(true);

    ui->instPlay->setText("Stop");
}

void MainWindow::actStopAnimation()
{
    delete animator;
    animator = nullptr;

    ui->instructionListTable->setEnabled(true);
    ui->pushAddIndstruction->setEnabled(true);
    ui->pushDelInstruction->setEnabled(true);
    ui->instComboCommand->blockSignals(false);
    ui->instComboSpritemap->blockSignals(false);
    ui->instTimer->blockSignals(false);
    ui->instLineArgument->blockSignals(false);

    ui->instPlay->setText("Play");
}
