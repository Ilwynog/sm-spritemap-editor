#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QColorDialog>
#include "gui/animationobject.h"
#include "instrlistparserdialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class Sprite; class QListWidgetItem; class TilesetTile; class AnimationObject; class Animator;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void loadTilesheet(int paletteIndex = 0);
    QImage loadGraphicsImage (std::vector<uint8_t> &data, uint32_t gfxPointer, uint32_t palPointer, bool gfxCompressed, bool palCompressed, uint8_t paletteIndex);
    QImage loadGraphicsImage (std::vector<uint8_t> &data, uint32_t gfxPointer, std::vector<uint16_t> &palette, bool gfxCompressed, uint8_t paletteIndex);
    std::vector<TilesetTile> loadGraphicsSheet(std::vector<uint8_t> &data, uint32_t pointer, bool decompress);
    std::vector<uint16_t> loadPalette(std::vector<uint8_t> &data, uint32_t pointer, bool decompress);

    void createGraphics(const AnimationObject &animObject);
    void loadSpritemaps(const AnimationObject &animObject);
    void loadInstructionLists(const AnimationObject &animObject);
    void loadCodeCommands(const AnimationObject &animObject);

private:
    /**
     * @brief addSprite Adds new default sprite to the spritemap
     */
    Sprite* addSprite();

    /**
     * @brief addSprite Adds sprite using given data
     * @param data Data
     */
    Sprite* addSprite(std::vector<uint8_t> const &data, const int ptr);

    /**
     * @brief addSprite Adds given sprite to spritemap
     * @param sprite Sprite to add
     * @return Return same sprite
     */
    Sprite* addSprite(Sprite* sprite);

    /**
     * @brief parseSprite Parses sprite from data but does not add to spritemap
     * @param data Data
     * @param ptr Pointer to data
     * @return Sprite
     */
    Sprite *parseSprite(const std::vector<uint8_t> &data, const int ptr);

    /**
     * @brief setSpritemapZValues Set the Z value for each sprite
     */
    void setSpritemapZValues();

    /**
     * @brief redrawSpritemap Redraws entire spritemap
     */
    void redrawSpritemap();
    QImage loadNewGfx(std::vector<uint8_t> gfxData, std::vector<uint16_t> palData, int paletteIndex, QPoint topLeft);

    void setPalette(std::vector<uint16_t> data, int paletteIndex);
    void loadSinglePalette(int paletteIndex);

    QString getSpritemapAsm();

    QList<Sprite *> parseSpritemap(std::vector<uint8_t> const &rom, uint32_t mapPointer);

    /**
     * @brief parseInstructionList Parses instruction list from rom
     * @param rom Rom
     * @param commands Known commands used in instruction list
     * @param mapBank Bank spritemaps are in
     * @param start Start of instruction list, pc
     * @param end End of instruction list, pc
     * @return AnimationObject containing the instruction list
     */
    AnimationObject parseInstructionList(std::vector<uint8_t> const &rom, const QList<QList<QString> > &commands, uint8_t mapBank, uint32_t start, uint32_t end=0xFFFFFFFF);

private slots:
    void actLoadRom();
    void actSaveRom();
    void actSaveNewRom();
    void actSaveSpritemap();
    void actSaveSpritemapAsm();
    void actShowAbout();
    void actLoadGfxPage0File();
    void actLoadGfxPage1File();
    void actLoadFullPalette();
    void actLoadPal0();
    void actLoadPal1();
    void actLoadPal2();
    void actLoadPal3();
    void actLoadPal4();
    void actLoadPal5();
    void actLoadPal6();
    void actLoadPal7();

    void actShowInstListParser();

    void actLoadSpritemap();
    void actLoadPalette();
    void actLoadGfx();
    void enablePage1(bool enable);
    void changeBackdropColor();

    void actShowSpritemapAsm();

    void on_checkLargeSprite_toggled(bool checked);
    int getSpriteListIndexOfSpriteObject(Sprite *sprite);
    void setSpriteListIndexViaSpriteObject(Sprite* sprite);
    void on_spinPosX_valueChanged(int pos);
    void on_spinPosY_valueChanged(int pos);
    void on_spinTileNumber_valueChanged(int val);
    void on_comboPalette_currentIndexChanged(int index);
    void on_comboPriority_currentIndexChanged(int index);
    void on_buttonDelSprite_released();
    void addSprite(int tileIndex);

    void on_comboObject_currentIndexChanged(int index);

    void on_comboAnim_currentIndexChanged(int index);

    void on_comboFrame_currentIndexChanged(int index);
    void connectSpriteSignals();
    void selectSpriteListItems();
    void flipSpritemapX();
    void flipSpritemapY();

    void on_radioDrawOrderDescend_toggled(bool checked);

    void on_comboSpriteSizes_activated(int index);

    void on_comboSpriteSizes_currentIndexChanged(int index);

    void on_spriteIndexList_itemSelectionChanged();

    void on_tabWidget_currentChanged(int index);
    void currentSpritemapChanged(QListWidgetItem* current,QListWidgetItem* previous);
    void actAddSpritemap();
    void actDelSpritemap();
    void actExportAnimation();
    void actSaveAnimObject();
    void actLoadAnimObject();

    void actAddInstruction();
    void actDelInstruction();
    void actSetInstTimer();
    void actSetInstSpritemap(int i);
    void actInstListActivated();
    void actSetInstructionTypeGuiElements(int i);
    void actSetInstructionType(int i);
    void actSetInstCommand();
    void actSetInstArgument();
    void actAddCodeCommand();
    void actDeleteCodeCommand();
    void actDisplayDefaultCommandLoader();
    void actAcceptInstructionList(AnimationObject in);

    void actToggleAnimation();
    void actPlayAnimation();
    void actStopAnimation();

private:
    Ui::MainWindow *ui;
    instrListParserDialog *instListParserDialog;
    QPixmap getSpriteGraphic(Sprite *sprite);
    QImage sheetImage = QImage(0x100, 0x200, QImage::Format_ARGB32);

    QString lastLoadedRom = "";
    //std::unique_ptr<Animator> animator = nullptr;
    Animator* animator = nullptr;
    //Animator animator;

    void createMenuActions();
    void loadObjectList();
    /**
     * @brief MainWindow::setSpriteListItemText
     * @param sprite Sprite to get data from
     * @param listItem List item to set text of
     * @param i Sprite index
     */
    void setSpriteListItemText(const Sprite* sprite, QListWidgetItem* listItem, uint16_t i);
    void changeSpriteSize(int index);
    void createAnimObjectFromGui(AnimationObject &saveObj);
    void connectAnimatorSignals();
};
#endif // MAINWINDOW_H
