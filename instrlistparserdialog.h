#ifndef INSTRLISTPARSERDIALOG_H
#define INSTRLISTPARSERDIALOG_H

#include <QDialog>
#include "gui/animationobject.h"
#include "lorom/lorom.h"
#include <QAbstractItemModel>

namespace Ui {
class instrListParserDialog;
}

class instrListParserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit instrListParserDialog(QWidget *parent = nullptr);
    ~instrListParserDialog();

    std::vector<uint8_t> rom;
    void setCommands(QList<QList<QString> > commands);
    void clearInstructionList();

    SpriteScene *pal0Scene = nullptr;
    SpriteScene *pal1Scene = nullptr;
    SpriteScene *pal2Scene = nullptr;
    SpriteScene *pal3Scene = nullptr;
    SpriteScene *pal4Scene = nullptr;
    SpriteScene *pal5Scene = nullptr;
    SpriteScene *pal6Scene = nullptr;
    SpriteScene *pal7Scene = nullptr;

public slots:
    void parseInstructionList();

signals:
    void sigParseInstList();
    void sigAcceptInstList(AnimationObject);

private:
    Ui::instrListParserDialog *ui;
    AnimationObject animObj;
    QList<QList<QString> > getCommands();
    uint16_t readWord(const std::vector<uint8_t> &data, uint32_t offset);
    uint32_t readLong(const std::vector<uint8_t> &data, uint32_t offset);

    void loadInstructionList(const AnimationObject &animObject);
    void displaySpritemap();
    void outputInstructionList();

private slots:
    void loadDefaultCommands(QString str);
};

#endif // INSTRLISTPARSERDIALOG_H
