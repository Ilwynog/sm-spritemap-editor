#include "romhandler.h"
#include <iostream>
#include <fstream>
#include <QProgressDialog>

/**
 * @brief RomHandler::loadRom Loads a file to a vector
 * @param filePath Path to file
 * @param toVector Vector to write to
 * @return Could load succesfully
 */
bool RomHandler::loadFile(std::string filePath, std::vector<uint8_t> &toVector)
{
    //Open the file
    std::fstream rom;
    rom.open(filePath, std::ios_base::in | std::ios_base::binary);
    if (!rom.is_open())
        throw std::runtime_error("File could not be opened: " + filePath);

    //Find filesize and reserve vector
    uint64_t romSize;
    rom.seekg(0, std::ios_base::end);
    romSize = rom.tellg();
    toVector.resize(romSize);

    //Read and close the file
    rom.seekg(0, std::ios_base::beg);
    rom.read((char*)toVector.data(), romSize);
    rom.close();
    if (rom.rdstate() != std::ios_base::goodbit) {throw std::runtime_error("File read error:" + filePath);}

    //Return read success
    return rom.good();
}

/**
 * @brief writeFile Writes given vector to a file
 * @param filePath Path to file
 * @param fromVector Vector to write to file
 * @return Was writing successful
 */
bool RomHandler::writeFile(std::string filePath, const std::vector<uint8_t> &fromVector)
{
    //Create and open the file
    std::fstream file;
    file.open(filePath, std::ios_base::out | std::ios_base::binary);
    if (!file.is_open()) {throw std::runtime_error("File could not be created");}

    //Write and close the file
    //for (size_t i=0; i < fromVector.size(); i++)
    //    file << fromVector[i];
    file.write(reinterpret_cast<const char*>(&fromVector[0]), fromVector.size());
    file.close();
    if (file.rdstate() != std::ios_base::goodbit) {throw std::runtime_error("File write error: " + filePath);}

    return true;
}

bool RomHandler::writeFile(QString filePath, const std::vector<uint8_t> &fromVector)
{
    try {
        return writeFile((filePath.toStdString()), fromVector);
    } catch (...) { throw;
    }
}


/**
 * @brief naiveParseAsm Naively reads bytes until 0x60 or 0x6B
 * @param rom Rom data
 * @param asmBytes Vector to write to
 * @param start Reading start position
 * @param max How many bytes to read at most
 * @return Did read data fit into given maximum
 */
bool RomHandler::naiveParseAsm(const std::vector<uint8_t> &rom, std::vector<uint8_t> &bytes, uint32_t start, uint16_t max = 0x20)
{
    bytes.resize(max);

    for (uint16_t i = 0; i < max; i++)
    {
        bytes[i] = rom[start+i];
        if (bytes[i] == 0x60 || bytes[i] == 0x6B)
        {
            bytes.resize(i+1);
            return true;
        }
    }

    return false;
}




/***************************************************************************************************
 ***************************************************************************************************
 *********** ROM Compiler                                                                ***********
 ***************************************************************************************************
 ***************************************************************************************************/

bool wrapperComp(const DataWrapper &a, const DataWrapper &b)
{
    if (a.getAllowedBanks().empty() || b.getAllowedBanks().empty()) return false;
    if (a.getAllowedBanks()[0] == 0xA1 && b.getAllowedBanks()[0] == 0xA1)
    {
        return (a.getData().size() < b.getData().size());
    }
    return (a.getAllowedBanks().size() < b.getAllowedBanks().size());
}

void RomHandler::writeToRom(std::vector<uint8_t> &rom, uint32_t pointer, const std::vector<uint8_t> &data)
{
    for (size_t i=0, total = data.size(); i < total; i++)
        rom[pointer+i] = data[i];
}


/***************************************************************************************************
 ***************************************************************************************************
 *********** Data wrapper                                                                ***********
 ***************************************************************************************************
 ***************************************************************************************************/
std::vector<uint8_t> DataWrapper::getAllowedBanks() const
{
    return allowedBanks;
}

void DataWrapper::setAllowedBanks(const std::vector<uint8_t> &value)
{
    allowedBanks = value;
}

std::vector<uint32_t> DataWrapper::getPointerLocations() const
{
    return pointerLocations;
}

void DataWrapper::setPointerLocations(const std::vector<uint32_t> &value)
{
    pointerLocations = value;
}

size_t DataWrapper::getDataHash() const
{
    return dataHash;
}

void DataWrapper::setDataHash(const size_t &value)
{
    dataHash = value;
}

std::vector<uint8_t> DataWrapper::getData() const
{
    return data;
}

void DataWrapper::setData(const std::vector<uint8_t> &value)
{
    data = value;
}

size_t DataWrapper::hash(const std::vector<uint8_t> &value)
{
  std::size_t seed = value.size();
    for(auto& i : value)
      seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    return seed;
}

#define sameHash(i,j) ( i->getDataHash() == j->getDataHash() )
#define sameBank(i,j) ( i->getAllowedBanks()[0] == j->getAllowedBanks()[0] )
#define anyBank(i,j) ( (i->getPointerSize()[0]   & j->getPointerSize()[0]) == 3 )
void DataWrapper::combineDuplicates(std::list<DataWrapper> &input, QProgressDialog *progressDialog)
{    
    progressDialog->setLabelText(QString("Combining duplicate data"));
    progressDialog->setValue(0);
    progressDialog->setMaximum(input.size());
    int progressViewValue = 0;

    std::vector<uint8_t> temp; std::vector<uint32_t> temp2;
    std::vector<uint8_t> temp3; std::vector<uint32_t> temp4;
    temp.reserve(64); temp2.reserve(512);

    for (auto i = input.begin(); i != input.end(); i++)
    {
        for (auto j = std::next(i); j != input.end(); j++)
        {
            if (sameHash(i,j) && (     //Same data
                    anyBank(i,j)  //Anywhere in rom, or
                 || sameBank(i,j) //In same bank
                                 )
                )
              {
                  temp = i->getAllowedBanks();
                  temp2 = i->getPointerLocations();
                  temp3 = i->getPointerSize();

                  for (auto &i: temp)
                      j->addAllowedBank(i);
                  for (auto &i: temp2)
                      j->addPointerLocation(i);
                  for (auto &i: temp3)
                      j->addPointerSize(i);

                  i = input.erase(i);
              }
        }

        progressViewValue++;
        progressDialog->setLabelText(QString("Combining duplicate data %1/%2").arg(progressViewValue)
                                                                              .arg(input.size()));
        progressDialog->setValue(progressViewValue);
        if (progressDialog->wasCanceled()) throw std::runtime_error("ROM compilation canceled");

        progressDialog->setMaximum(input.size());
    }
}
#undef sameHash
#undef sameBank
#undef anyBank

std::vector<uint8_t> DataWrapper::getPointerSize() const
{
    return pointerSize;
}

void DataWrapper::setPointerSize(const std::vector<uint8_t> &value)
{
    pointerSize = value;
}
