#ifndef ROMHANDLER_H
#define ROMHANDLER_H

#include <vector>
#include <string>
#include <list>
#include <memory>
#include <map>

class MDB; class DDB; class Tileset; class QString; class Music; class SaveStation; class Project; class QProgressDialog;


/**
 * @brief The RomHandler class
 * Contains functions for working with roms and decompiled rom source files,
 * and also functionality for compiling new roms.
 */
class RomHandler
{
public:
    /**
     * @brief RomHandler::loadRom Loads a file to a vector
     * @param filePath Path to file
     * @param toVector Vector to write to
     * @return Could load succesfully
     */
    bool static loadFile(std::string filePath, std::vector<uint8_t> &toVector);

    /**
     * @brief writeFile Writes given vector to a file
     * @param filePath Path to file
     * @param fromVector Vector to write to file
     * @return Was writing successful
     */
    bool static writeFile(std::string filePath, const std::vector<uint8_t> &fromVector);

    bool static writeFile(QString filePath, const std::vector<uint8_t> &fromVector);

    /**
     * @brief naiveParseAsm Naively reads bytes until 0x60 or 0x6B
     * @param rom Rom data
     * @param asmBytes Vector to write to
     * @param start Reading start position
     * @param max How many bytes to read at most
     * @return Was max size exceeded
     */
    bool static naiveParseAsm(const std::vector<uint8_t> &rom, std::vector<uint8_t> &bytes, uint32_t start, uint16_t max);



    /******************************
     * ROM Compiling stuff below  *
     ******************************/

    void static compileNewRom(Project hack, std::vector<uint8_t> &baseRom, QProgressDialog *progressDialog);
    void static compileNewRom(std::vector<uint8_t> &baseRom, const std::vector<std::shared_ptr<MDB> > &roomFiles, const std::vector<std::shared_ptr<DDB> > &allDoors, std::vector<Tileset> &tilesetFiles, std::vector<Music> &musicFiles, Tileset &creSet,
                              const std::vector<std::shared_ptr<SaveStation> > &saveStations, QProgressDialog *progressDialog);

private:
    /**
     * @brief writeToRom Writes data to rom
     * @param rom Rom to write to
     * @param pointer Where in rom to write
     * @param data Data to write
     */
    void static writeToRom(std::vector<uint8_t> &rom, uint32_t pointer, const std::vector<uint8_t> &data);
};


/**
 * @brief The DataWrapper class A wrapper for data yet to be written
 * Holds info about pointer size, allowed banks, hash of data and data itself
 */
class DataWrapper
{
    std::vector<uint8_t> pointerSize = std::vector<uint8_t>();
    std::vector<uint8_t> allowedBanks = std::vector<uint8_t>();
    std::vector<uint32_t> pointerLocations = std::vector<uint32_t>();
    size_t dataHash = 0;
    std::vector<uint8_t> data = std::vector<uint8_t>();

public:
    std::vector<uint8_t> getPointerSize() const;
    void setPointerSize(const std::vector<uint8_t> &value);
    std::vector<uint8_t> getAllowedBanks() const;
    void setAllowedBanks(const std::vector<uint8_t> &value);
    std::vector<uint32_t> getPointerLocations() const;
    void setPointerLocations(const std::vector<uint32_t> &value);
    size_t getDataHash() const;
    void setDataHash(const size_t &value);
    std::vector<uint8_t> getData() const;
    void setData(const std::vector<uint8_t> &value);

    /**
     * @brief addPointerLocation Adds a location where to write pointer to data to
     * @param value Pointer location
     */
    void addPointerLocation(const uint32_t &value)
    {
        pointerLocations.emplace_back(value);
    }

    /**
     * @brief addPointerSize Adds a size for a pointer; only useful if all wrappers define a size for all pointers
     * @param value Pointer size
     */
    void addPointerSize(const uint8_t &value)
    {
        pointerSize.emplace_back(value);
    }

    /**
     * @brief addAllowedBank Adds an allowed bank for data
     * @param value Bank
     */
    void addAllowedBank(const uint8_t &value)
    {
        for (auto &i: allowedBanks)
            if (i == value) return; //Don't add bank if it already exists
        allowedBanks.emplace_back(value);
    }

    /**
     * @brief hash Calculates a hash value for a vector
     * @param value Input vector
     * @return Hash value
     */
    static size_t hash(const std::vector<uint8_t> &value);

    /**
     * @brief combineDuplicates Combines duplicate datas under one pointer
     * @param input Input datas
     */
    static void combineDuplicates(std::list<DataWrapper> &input, QProgressDialog *progressDialog);

    /**
     * @brief clear Resets the wrapper
     */
    void clear()
    {
        pointerSize.clear();
        allowedBanks.clear();
        pointerLocations.clear();
        dataHash = 0;
        data.clear();
    }
};

#endif // ROMHANDLER_H
