#ifndef TILESETBLOCK_H
#define TILESETBLOCK_H

#include <QByteArray>

class TilesetBlock
{
    uint16_t data[4] = {0,0,0,0};
    /* each corner is 2 bytes, arranged top left, top right, bottom left, bottom right
       each corner has the format
        YXLP PPTT TTTT TTTT
        Y is flipped vertically, X for flipped horizontally. L is layer priority (1 means it draws over sprites, ie the red dot in smile). P is the palette of the corner. T is the tile number in the graphics sheet
        tile can be from 000 (top left of SCE) to 3FF (bottom right of CRE)
        palette ranges from 0000 to 1C00, in steps of 400.*/

public:
    TilesetBlock();
    TilesetBlock(uint16_t data[4]);
    uint16_t *getData();
    static TilesetBlock parseBlock(const QByteArray input);
};

#endif // BLOCK_H
