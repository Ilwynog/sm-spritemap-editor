#ifndef TILESETTILE_H
#define TILESETTILE_H

#include <QByteArray>


class TilesetTile
{
    uint8_t data[32]; //8x8 pixels, 4bpp

public:
    TilesetTile();
    TilesetTile(uint8_t data[32]);
    uint8_t *getData();
    static TilesetTile parseTile(const QByteArray input);
    int8_t getPixelColor(int8_t, bool easyTile = true);
};

#endif // TILE_H
