#include "tilesetTile.h"

TilesetTile::TilesetTile()
{
    std::fill(data,data+32,0);
}

TilesetTile::TilesetTile(uint8_t data[32])
{
    std::copy(data, data+32,this->data);
}


uint8_t* TilesetTile::getData()
{
    return this->data;
}


/**
 * @brief TilesetTile::parseTile Parses one tilesheet tile from 32 bytes of data
 * @param input Graphic data
 * @return Tilesheet tile 8x8 4bpp
 */
TilesetTile TilesetTile::parseTile(const QByteArray input)
{
    if (input.size() != 32) return TilesetTile(); //Return null tile if wrong size data

    uint8_t data[32];
    std::copy(input.begin(), input.end(), std::begin(data));

    return TilesetTile(data);
}

/**
 * @brief TilesetTile::getPixelColor Get color index of specified pixel.
 * @param pixel Which pixel
 * @param easyTile Is easy format assumed (not bitplane format)
 * @return Color index of specified pixel, or -1 if error
 */
int8_t TilesetTile::getPixelColor(int8_t pixel, bool easyTile)
{
    //TODO: Don't assume easy format
    if (pixel < 0 || pixel > 63) return -1;
    if (pixel % 2 == 0) return ((this->data[pixel/2] >> 4) & 0xF);
    return (this->data[pixel/2] & 0xF);
}
