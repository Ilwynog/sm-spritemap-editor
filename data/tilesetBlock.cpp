#include "tilesetBlock.h"

TilesetBlock::TilesetBlock()
{
}

TilesetBlock::TilesetBlock(uint16_t data[4])
{
    std::copy(data, data+4,this->data);
}

uint16_t *TilesetBlock::getData()
{
    return this->data;
}


/**
 * @brief TilesetBlock::parseBlock Parses one tiletable block from 8 bytes of input, assumed snes format
 * @param data 8 bytes of input data
 * @return Tiletable block
 */
TilesetBlock TilesetBlock::parseBlock(const QByteArray input)
{
    if (input.size() != 8) return TilesetBlock(); //Return null block if wrong size data

    uint16_t block[4] = {0,0,0,0};
    uint16_t wut;

    for (int i=0; i<4; i++)
    {
        wut = input[i*2+1];
        wut = wut << 8;
        block[i] = (input[i*2+1] << 8) | (input[i*2] & 0x00FF);
    }

    return TilesetBlock(block);
}
