#include "tileset.h"
#include "lorom/lorom.h"
#include <iostream>
#include <rom/romhandler.h>
#include <QDir>
#include <QImage>
#include <QPainter>

Tileset::Tileset()
{
}

Tileset::Tileset(std::vector<TilesetBlock> tilemap, std::vector<TilesetTile> rawTilesheet, std::vector<uint16_t> palette)
{
    this->tilemap = tilemap;
    this->tilesheet = rawTilesheet;
    this->easyTilesheet = parseFakeTilesheet(rawTilesheet);
    this->palette = palette;
}

/**
 * @brief Tileset::loadTileset Loads a tileset from a given location in a file. Expects tiletable (16x16), tilesheet (8x8), and palette pointers. Long for each.
 * @param rom File
 * @param ptr Pointer to tileset pointers
 * @return Tileset data
 */
Tileset Tileset::parseTileset(const std::vector<uint8_t> &rom, uint32_t ptr)
{
    QByteArray data;

    for (uint i=0; i < 9; i++)
        data[i] = rom[ptr+i];

    tilemapPointer = LoRom::snes2pc(data.left(3));
    if (tilemapPointer > rom.size()) throw std::runtime_error(QString("Tileset: Invalid tilemap pointer %1 at %2").
                                                          arg(QString::number(tilemapPointer,16)).
                                                          arg(QString::number(ptr,16)).toStdString()
                                                          );
    tilesheetPointer = LoRom::snes2pc(data.mid(3,3));
    if (tilesheetPointer > rom.size()) throw std::runtime_error(QString("Tileset: Invalid tilesheet pointer %1 at %2").
                                                          arg(QString::number(tilesheetPointer,16)).
                                                          arg(QString::number(ptr,16)).toStdString()
                                                          );
    palettePointer = LoRom::snes2pc(data.right(3));
    if (palettePointer > rom.size()) throw std::runtime_error(QString("Tileset: Invalid palette pointer %1 at %2").
                                                          arg(QString::number(palettePointer,16)).
                                                          arg(QString::number(ptr,16)).toStdString()
                                                          );

    palette = parsePalette(rom,palettePointer);
    tilesheet = parseTilesheet(rom,tilesheetPointer); easyTilesheet = parseFakeTilesheet(tilesheet);
    tilemap = parseTilemap(rom,tilemapPointer);

    return *this;
}


/**
 * @brief Tileset::loadTileset Loads a CRE set from rom. Tilesheet and tiletable, but no palette. Gets pointers from hardcoded locations in $82
 * @param rom File
 * @return Tileset data
 */
Tileset Tileset::parseCreTileset(const std::vector<uint8_t> &rom)
{
    tilesheetPointer = rom[LoRom::snes2pc(0x82e415)] << 16 | rom[LoRom::snes2pc(0x82e41a)] << 8 | rom[LoRom::snes2pc(0x82e419)];
    tilemapPointer = rom[LoRom::snes2pc(0x82e83d)] << 16 | rom[LoRom::snes2pc(0x82e842)] << 8 | rom[LoRom::snes2pc(0x82e841)];

    tilesheetPointer = LoRom::snes2pc(tilesheetPointer);
    tilemapPointer = LoRom::snes2pc(tilemapPointer);

    palettePointer = 0; paletteSize = 0;

    tilesheet = parseTilesheet(rom,tilesheetPointer); easyTilesheet = parseFakeTilesheet(tilesheet);
    tilemap = parseTilemap(rom,tilemapPointer);

    return *this;
}

/**
 * @brief Tileset::parsePalette Loads a tileset palette
 * @param rom ROM to load from
 * @param ptr Pointer to data
 * @param decompress Does data need to be decompressed
 * @return Uncompressed palette data (BGR)
 */
std::vector<uint16_t> Tileset::parsePalette(const std::vector<uint8_t> &rom, uint32_t ptr, bool decompress)
{
    std::vector<uint16_t> palette;

    const uint32_t palSize = 2*16*8; //2 bytes per color, 16 colors per line, 8 lines

    uchar udst[palSize];

    size_t srcSize = 0;
    paletteSize = palSize;

    if (decompress) paletteSize = Compresch_Metroid::Decompress(&rom[ptr], palSize, udst, srcSize);
    else for (size_t i=0; i < paletteSize; i++) udst[i] = rom[ptr+i];

    for (uint i=0; i < paletteSize; i+=2) //Bytes into words
    {
        palette.emplace_back(( (udst[i+1]<<8) & 0xFF00) | udst[i]);
    }

    this->palette = palette;

    return palette;
}


/**
 * @brief Tileset::parseTilesheet Loads a tilesheet (8x8 tiles)
 * @param rom ROM to load from
 * @param ptr Pointer to data
 * @param decompress Does data need to be decompressed
 * @return Uncompressed tilesheet data
 */
std::vector<TilesetTile> Tileset::parseTilesheet(const std::vector<uint8_t> &rom, uint32_t ptr, bool decompress)
{
    std::vector<TilesetTile> tilesheet;

    uint32_t gfxSize = 16*64*8*8*(1/2.0); //16x64 tiles maximum, each tile is 8x8 pixels, format is 4 bits per pixel, or two pixels per byte

    if (ptr >= rom.size()) //TODO: data goes beyond end-of-file, throw error?
        return tilesheet;

    if (ptr + gfxSize >= rom.size())
        gfxSize -= gfxSize - (rom.size() - ptr);
    uchar udst[gfxSize];

    size_t srcSize = 0;
    size_t graphicSize = gfxSize;
    if (decompress) graphicSize = Compresch_Metroid::Decompress(&rom[ptr], gfxSize, udst, srcSize);
    else for (size_t i=0; i < gfxSize; i++) udst[i] = rom[ptr+i];
    tilesheetSize = srcSize;

    TilesetTile tile;
    QByteArray tileData;
    tilesheet.reserve(graphicSize / 32);

    for (uint i=0; i < graphicSize; i+=32)
    {
        tileData.clear();
        for (int j=0; j < 32; j++)
        {
            tileData.append(udst[j+i]);
        }
        tile = TilesetTile::parseTile(tileData);
        tilesheet.emplace_back(tile);
    }

    this->tilesheet = tilesheet;

    return tilesheet;
}

/**
 * @brief Tileset::getFakeTilesheet Returns tilesheet in convenient index format
 * @return Convenient tilesheet
 */
std::vector<TilesetTile> Tileset::parseFakeTilesheet(std::vector<TilesetTile> rawTilesheet)
{
    std::vector<TilesetTile> fakeSheet;

    if (rawTilesheet.size() % 32 != 0) return fakeSheet;

    TilesetTile tile, easyTile;
    QByteArray easyTileData = QByteArray(32, 0); //Will be twice as large as original, because no longer encoding two pixels in with 8 bits
    char value0, value1, value2, value3;
    uint8_t *data;

    for (uint i=0; i < rawTilesheet.size(); i++)
    {
        /*Bitplane explanation
         * 00 00 00 00 00 00 1F 00 78 07 DD 02 E8 13 26 D9 FF 00 FF 00 FF 00 FF 00 FF 00 FF 00 FF 00 FF 00 Raw data
         *
         * 00 00 00 1F 78 DD E8 26 [0,  2,  4,  6,  8,  A,  C,  E] Interleaved data, 16-byte chunks
         * 00 00 00 00 07 02 13 D9 [1,  3,  5,  7,  9,  B,  D,  F]
         * FF FF FF FF FF FF FF FF [10, 12, 14, 16, 18, 1A, 1C, 1E]
         * 00 00 00 00 00 00 00 00 [11, 13, 15, 17, 19, 1B, 1D, 1F]
         *
         * 00000000 00000000 00000000 00011111 01111000 11011101 11101000 00100110 Above in binary
         * 00000000 00000000 00000000 00000000 00000111 00000010 00010011 11011001
         * 11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111
         * 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
         *
         * 44444444 44444444 44444444 44455555 45555666 55455565 55565466 66566556 Read bits vertically
         *
         * 4 4 4 4 4 4 4 4 Result
         * 4 4 4 4 4 4 4 4
         * 4 4 4 4 4 4 4 4
         * 4 4 4 5 5 5 5 5
         * 4 5 5 5 5 6 6 6
         * 5 5 4 5 5 5 6 5
         * 5 5 5 6 5 4 6 6
         * 6 6 5 6 6 6 5 6*/
        tile = rawTilesheet[i];
        data = tile.getData();

        for (int j=0; j < 8; j++)
        {
            value0 = data[0x02*j]; value1 = data[0x02*j+1];
            value2 = data[0x02*j+0x10]; value3 = data[0x02*j+0x11];

            easyTileData[4*j] =
                       (value0 >> 6 & 1)
                    | ((value1 >> 6 & 1) << 1)
                    | ((value2 >> 6 & 1) << 2)
                    | ((value3 >> 6 & 1) << 3)
                    | ((value0 >> 7 & 1) << 4)
                    | ((value1 >> 7 & 1) << 5)
                    | ((value2 >> 7 & 1) << 6)
                    | ((value3 >> 7 & 1) << 7);
            easyTileData[4*j+1] =
                       (value0 >> 4 & 1)
                    | ((value1 >> 4 & 1) << 1)
                    | ((value2 >> 4 & 1) << 2)
                    | ((value3 >> 4 & 1) << 3)
                    | ((value0 >> 5 & 1) << 4)
                    | ((value1 >> 5 & 1) << 5)
                    | ((value2 >> 5 & 1) << 6)
                    | ((value3 >> 5 & 1) << 7);
            easyTileData[4*j+2] =
                       (value0 >> 2 & 1)
                    | ((value1 >> 2 & 1) << 1)
                    | ((value2 >> 2 & 1) << 2)
                    | ((value3 >> 2 & 1) << 3)
                    | ((value0 >> 3 & 1) << 4)
                    | ((value1 >> 3 & 1) << 5)
                    | ((value2 >> 3 & 1) << 6)
                    | ((value3 >> 3 & 1) << 7);
            easyTileData[4*j+3] =
                       (value0 >> 0 & 1)
                    | ((value1 >> 0 & 1) << 1)
                    | ((value2 >> 0 & 1) << 2)
                    | ((value3 >> 0 & 1) << 3)
                    | ((value0 >> 1 & 1) << 4)
                    | ((value1 >> 1 & 1) << 5)
                    | ((value2 >> 1 & 1) << 6)
                    | ((value3 >> 1 & 1) << 7);

        }
        easyTile = TilesetTile::parseTile(easyTileData);
        fakeSheet.emplace_back(easyTile);
    }

    return fakeSheet;
}

void Tileset::clearRomData(std::vector<uint8_t> &rom)
{
    for (size_t i=0; i < paletteSize; i++)
        rom[palettePointer+i] = 0xFF;
    for (size_t i=0; i < tilesheetSize; i++)
        rom[tilesheetPointer+i] = 0xFF;
    for (size_t i=0; i < tilemapSize; i++)
        rom[tilemapPointer+i] = 0xFF;
}

void Tileset::writeToRom(std::list<DataWrapper> &dataWrappers, const uint32_t pointerPos, const uint32_t index)
{
    unsigned char compressedData[0x8000];
    unsigned char uncompressedData[0x8000];
    size_t dataHash; size_t size;

    DataWrapper wrap; std::vector<uint8_t> vec;// = std::vector<uint8_t>(compressedData, compressedData+size);

    //Write palette
    for (uint i=0; i < palette.size(); i++)
    {
        uncompressedData[i*2] = palette[i];
        uncompressedData[i*2+1] = palette[i] >> 8;
    }
    dataHash = DataWrapper::hash(std::vector<uint8_t>(uncompressedData, uncompressedData+palette.size()*2));
    size = Compresch_Metroid::Compress(&uncompressedData[0], palette.size()*2, &compressedData[0]);
    vec = std::vector<uint8_t>(compressedData, compressedData+size);
    vec.emplace_back(0x00);
    wrap.setDataHash(dataHash); wrap.setData(vec); wrap.setPointerSize({3});
    wrap.setPointerLocations({pointerPos + index*9 + 6});
    if (pointerPos != 0) dataWrappers.emplace_back(wrap);  //Palette only written for non-CRE sets
    wrap.clear(); std::fill(uncompressedData, uncompressedData+0x8000, 0);

    //Write tilesheet
    for (uint i=0; i < tilesheet.size(); i++)
    {
        for (int j=0; j < 32; j++)
            uncompressedData[i*32+j] = tilesheet[i].getData()[j];
    }
    dataHash = DataWrapper::hash(std::vector<uint8_t>(uncompressedData, uncompressedData+tilesheet.size()*32));
    size = Compresch_Metroid::Compress(&uncompressedData[0], tilesheet.size()*32, &compressedData[0]);
    vec = std::vector<uint8_t>(compressedData, compressedData+size);
    vec.emplace_back(0x00);
    wrap.setDataHash(dataHash); wrap.setData(vec); wrap.setPointerSize({3});
    wrap.setPointerLocations({pointerPos + index*9 + 3});
    if (pointerPos == 0) //CRE //TODO: Improve CRE writing - MultiCRE support?
    {
        wrap.setPointerLocations({0x16415, 0x16419, 0x16797, 0x1679B}); //1, 2, 1, 2
        wrap.setPointerSize({1,2,1,2});
    }
    dataWrappers.emplace_back(wrap);
    wrap.clear(); std::fill(uncompressedData, uncompressedData+0x8000, 0);


    //Write tilemap
    for (uint i=0; i < tilemap.size(); i++)
    {
        for (int j=0; j < 4; j++)
        {
            uncompressedData[i*8+j*2] = tilemap[i].getData()[j];
            uncompressedData[i*8+j*2+1] = tilemap[i].getData()[j] >> 8;
        }
    }
    dataHash = DataWrapper::hash(std::vector<uint8_t>(uncompressedData, uncompressedData+tilemap.size()*8));
    size = Compresch_Metroid::Compress(&uncompressedData[0], tilemap.size()*8, &compressedData[0]);
    vec = std::vector<uint8_t>(compressedData, compressedData+size);
    vec.emplace_back(0x00);
    wrap.setDataHash(dataHash); wrap.setData(vec); wrap.setPointerSize({3});
    wrap.setPointerLocations({pointerPos + index*9 + 0});
    if (pointerPos == 0) //CRE
    {
        wrap.setPointerLocations({0x1683D, 0x16841, 0x16AED, 0x16AF1}); //1, 2, 1, 2
        wrap.setPointerSize({1,2,1,2});
    }
    dataWrappers.emplace_back(wrap);
    wrap.clear();
}


/**
 * @brief writeToDisk Writes tileset to disk, uncompressed
 * @param directory Folder to write to
 * @return Was writing successful
 */
bool Tileset::writeToDisk(const QString &directory)
{
    QDir dir;
    dir.mkpath(directory);
    writePaletteToDisk(directory); //TODO: Choose format?
    writeSheetToDisk(directory);
    writeTilemapToDisk(directory);
    return true;
}

Tileset Tileset::readFromDisk(const QString &directory)
{
    std::vector<uint8_t> data; data.reserve(0x10000);

    try {
        RomHandler::loadFile((directory + "/Palette.bin").toStdString(), data);
        palette = parsePalette(data, 0, false);
    } catch(...) {std::cout << "No tileset palette, assuming CRE\n";}

    try {
        RomHandler::loadFile((directory + "/Tilesheet.gfx").toStdString(), data);
        tilesheet = parseTilesheet(data, 0, false);
        easyTilesheet = parseFakeTilesheet(tilesheet);
    } catch(...) {throw;}

    try {
        RomHandler::loadFile((directory + "/Tiletable.ttb").toStdString(), data);
        tilemap = parseTilemap(data, 0, false);
    } catch(...) {throw;}

    return *this;
}

bool Tileset::writePaletteToDisk(const QString &directory)
{
    std::vector<uint8_t> data;
    data.resize(palette.size()*2);
    for (uint i=0; i < palette.size(); i++)
    {
        data[i*2] = palette[i];
        data[i*2+1] = palette[i] >> 8;
    }
    return RomHandler::writeFile((directory + "/Palette.bin"), data);
}

bool Tileset::writeSheetToDisk(const QString &directory)
{
    std::vector<uint8_t> data;
    data.resize(tilesheet.size()*32);
    for (uint i=0; i < tilesheet.size(); i++)
    {
        for (int j=0; j < 32; j++)
            data[i*32+j] = tilesheet[i].getData()[j];
    }
    return RomHandler::writeFile((directory + "/Tilesheet.gfx"), data);
}

bool Tileset::writeTilemapToDisk(const QString &directory)
{
    std::vector<uint8_t> data;
    data.resize(tilemap.size()*8);
    for (uint i=0; i < tilemap.size(); i++)
    {
        for (int j=0; j < 4; j++)
        {
            data[i*8+j*2] = tilemap[i].getData()[j];
            data[i*8+j*2+1] = tilemap[i].getData()[j] >> 8;
        }
    }
    return RomHandler::writeFile((directory + "/Tiletable.ttb"), data);
}


/**
 * @brief Tileset::parseTilemap Loads a tilemap (16x16 tiles)
 * @param rom ROM to load from
 * @param ptr Pointer to data
 * @param decompress Does data need to be decompressed
 * @return Uncompressed tilemap data
 */
std::vector<TilesetBlock> Tileset::parseTilemap(const std::vector<uint8_t> &rom, uint32_t ptr, bool decompress)
{
    std::vector<TilesetBlock> tilemap;
    const uint32_t ttbSize = 768*8*2; //768 tiles (not including cre), 8 bytes per tile

    uchar udst[ttbSize];

    size_t srcSize = 0; size_t trueSize = rom.size();
    if (decompress) trueSize = Compresch_Metroid::Decompress(&rom[ptr], ttbSize, udst, srcSize);
    else for (size_t i=0; i < rom.size(); i++) udst[i] = rom[i];
    tilemapSize = srcSize;

    /* each corner is 2 bytes, arranged top left, top right, bottom left, bottom right
       each corner has the format
        YXLP PPTT TTTT TTTT
        Y is flipped vertically, X for flipped horizontally. L is layer priority (1 means it draws over sprites, ie the red dot in smile). P is the palette of the corner. T is the tile number in the graphics sheet
        tile can be from 000 (top left of SCE) to 3FF (bottom right of CRE)
        palette ranges from 0000 to 1C00, in steps of 400.*/

    TilesetBlock block;
    QByteArray blockData;

    for (size_t i=0; i < trueSize; i+=8) //8 bytes per block
    {
        blockData.clear();
        for (int j=0; j < 8; j++)
        {
            blockData.append(udst[i+j]);
        }
        block = TilesetBlock::parseBlock(blockData);
        tilemap.emplace_back(block);
    }

    return tilemap;

}

/**
 * @brief Tileset::getTilesheet Return tilesheet
 * @param easy Return convenient format
 * @return  Tilesheet, convenient or bitplane
 */
std::vector<TilesetTile> Tileset::getTilesheet(bool easy) const
{
    if (!easy) return this->tilesheet;
    return this->easyTilesheet;
}


/**
 * @brief makeTilesheetImage Turns given SCE and CRE into a vector of tilesheet images
 * @param sce SCE
 * @param cre CRE
 * @param paletteIndex Which palette line to use (from SCE)
 * @return Pointer to vector of 8x8 images
 */
std::vector<QImage>* Tileset::makeTilesheetGraphics(const Tileset &sce, const Tileset &cre, uint8_t paletteIndex)
{
    std::vector<QImage>* result = new std::vector<QImage>();
    result->reserve(1024);
    QImage tile;
    std::vector<QColor> tilePalette; QColor color; uint16_t temp; uint16_t r,g,b;
    QColor tempColor;

    std::vector<uint16_t> pal = sce.getPalette();

    //Make convenient palette
    for (size_t i=0; i < pal.size(); i++)
    {
        //B G R
        temp = pal[i];
        r = (temp&0x001f) << 3;
        g = (temp&0x03e0) >> 2;
        b = (temp&0x7c00) >> 7;

        color = QColor::fromRgb(r,g,b, 255);
        tilePalette.emplace_back(color);
    }

    //Combine SCE and CRE tilesheets
    std::vector<TilesetTile> sheetSce = sce.getTilesheet();
    std::vector<TilesetTile> sheetCre = cre.getTilesheet();
    size_t start = sheetSce.size();
    sheetSce.resize(1024);
    if (start < 640) start = 640;
    for (size_t i=start-640; i < sheetCre.size(); i++)
        sheetSce[i+start] = sheetCre[i];

    //Make the images
    for (size_t i=0, total = sheetSce.size(); i < total; i++)
    {
        tile = QImage(8,8,QImage::Format_ARGB32); tile.fill(QColor().black());
        for (size_t y=0, total=8; y<total; y++)
            for (size_t x=0, total=8; x<total; x++)
            {
                temp = sheetSce[i].getPixelColor(8*y+x);
                tempColor = tilePalette[temp + paletteIndex*16];
                if (!temp) tempColor.setRgba(qRgba(0,0,0,0));
                if (true) tile.setPixelColor(x, y, tempColor); //Only set color if not transparent
            }
        result->emplace_back(tile);
    }

    return result;
}

/**
 * @brief makeTilesheetImage Turns given graphical tilesheet into one big image
 * @param source Tilesheet vector
 * @return Image
 */
QImage Tileset::makeTilesheetImage(std::vector<QImage> &source)
{
    int height = source.size() / 16 + 1;
    QImage bigImage = QImage(16*8,height*8,QImage::Format_ARGB32);
    bigImage.fill(0);
    QPainter qp(&bigImage); qp.setCompositionMode(QPainter::CompositionMode_Source);
    for (size_t i=0, total = source.size(); i < total; i++)
    {
        qp.drawImage((i%16)*8,(i/16)*8,source[i]);
    }

    return bigImage;
}


/**
 * @brief makeTiletableGraphics Turns tiletable into vector of images
 * @param sce SCE
 * @param cre CRE
 * @param gfx Tilesheets, must contain 8 entires
 * @return Graphical tiletable
 */
std::vector<QImage> *Tileset::makeTiletableGraphics(Tileset &sce, Tileset &cre, std::vector<std::vector<QImage> *> &gfx)
{

    /* each corner is 2 bytes, arranged top left, top right, bottom left, bottom right
       each corner has the format
        YXLP PPTT TTTT TTTT
        Y is flipped vertically, X for flipped horizontally. L is layer priority (1 means it draws over sprites, ie the red dot in smile). P is the palette of the corner. T is the tile number in the graphics sheet
        tile can be from 000 (top left of SCE) to 3FF (bottom right of CRE)
        palette ranges from 0000 to 1C00, in steps of 400.*/

    std::vector<QImage>* result = new std::vector<QImage>();

    QImage corner;//, block;
    QImage block = QImage(QSize(16,16),QImage::Format_ARGB32);
    uint16_t *data; //4 words
    uint16_t tile = 0; uint8_t palette = 0; bool xFlip, yFlip;//, priority;

    //Combine SCE and CRE tiletables
    std::vector<TilesetBlock> tableSce = sce.getTilemap();
    std::vector<TilesetBlock> tableCre = cre.getTilemap();
    std::vector<TilesetBlock> tableFull;
    tableFull.resize(tableCre.size() + tableSce.size());
    for (size_t i=0; i < tableCre.size(); i++)
        tableFull[i] = tableCre[i];
    for (size_t i=tableCre.size(); i < tableFull.size(); i++)
        tableFull[i] = tableSce[i-tableCre.size()];

    QPainter qp(&block);
    qp.setCompositionMode(QPainter::CompositionMode_Source);

    for (size_t i=0; i < tableFull.size(); i++)
    {
        //block.fill(0);
        //qp.fillRect(0,0,16,16,QColor(0,0,0));
        data = tableFull[i].getData();
        for (size_t j=0; j < 4; j++)
        {
            tile = data[j] & 0x03FF;
            palette = (data[j] & 0x1C00) >> 10;
            xFlip = data[j] & 0x4000; yFlip = data[j] & 0x8000; //priority = data[j] & 0x2000;

            corner = gfx[palette]->at(tile);
            corner = corner.mirrored(xFlip, yFlip);

            qp.drawImage(j%2*8,j/2*8,corner);
        }
        result->emplace_back(block);
    }

    return result;
}

/**
 * @brief makeTilesheetImage Turns given graphical tiletable into one big image
 * @param source Tiletable vector
 * @return Image
 */
QImage Tileset::makeTiletableImage(std::vector<QImage> &source)
{
    QImage bigImage = QImage(32*16,32*16,QImage::Format_ARGB32);
    bigImage.fill(0);
    QPainter qp(&bigImage);
    qp.setCompositionMode(QPainter::CompositionMode_Source);
    for (size_t i=0; i < source.size(); i++)
    {
        qp.drawImage(i%32*16,i/32*16,source[i]);
    }
    return bigImage;
}
