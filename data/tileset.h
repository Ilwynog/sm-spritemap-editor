#ifndef TILESET_H
#define TILESET_H

#include "QVector"
#include "QFile"
#include <data/tilesetTile.h>
#include <data/tilesetBlock.h>
#include <Compresch/compresch_metroid.h>

class DataWrapper; class QImage;

class Tileset
{
    std::vector<uint16_t> palette;       //BGR, 5 bits per color, highest bit unused
    std::vector<TilesetTile> tilesheet; //8x8 tiles, bitplane format
    std::vector<TilesetBlock> tilemap; //16x16 tiles

    std::vector<TilesetTile> easyTilesheet;   //8x8 tiles, easy format

    uint32_t palettePointer = 0; size_t paletteSize = 0;    //Original pointers and compressed sizes
    uint32_t tilesheetPointer = 0; size_t tilesheetSize = 0;
    uint32_t tilemapPointer = 0; size_t tilemapSize = 0;

    /**
     * @brief writeToDisk Writes palette to disk
     * @param directory Folder to write to
     * @return Was writing successful
     */
    bool writePaletteToDisk(const QString &directory);
    /**
     * @brief writeToDisk Writes raw tilesheet to disk
     * @param directory Folder to write to
     * @return Was writing successful
     */
    bool writeSheetToDisk(const QString &directory);
    /**
     * @brief writeToDisk Writes tilemap to disk
     * @param directory Folder to write to
     * @return Was writing successful
     */
    bool writeTilemapToDisk(const QString &directory);
public:
    Tileset();
    Tileset(std::vector<TilesetBlock> tilemap, std::vector<TilesetTile> tilesheet, std::vector<uint16_t> palette);

    Tileset parseTileset(const std::vector<uint8_t> &, uint32_t);


    std::vector<uint16_t> parsePalette(const std::vector<uint8_t> &, uint32_t, bool decompress=true);
    std::vector<TilesetTile> parseTilesheet(const std::vector<uint8_t> &, uint32_t, bool decompress=true);
    std::vector<TilesetBlock> parseTilemap(const std::vector<uint8_t> &, uint32_t, bool decompress=true);

    /**
     * @brief parseCreTileset Loads a CRE set from rom. Tilesheet and tiletable, but no palette. Gets pointers from hardcoded locations in $82
     * @return CRE tileset
     */
    Tileset parseCreTileset(const std::vector<uint8_t> &);
    std::vector<TilesetTile> parseFakeTilesheet(std::vector<TilesetTile> tilesheet);

    /**
     * @brief clearRomData Clears data from the rom
     * @param rom Rom to clear
     */
    void clearRomData(std::vector<uint8_t> &rom);

    /**
     * @brief writeToRom Writes tileset data as it would appear in the rom. If pointerPos=0, writes as vanilla CRE
     * @param dataWrappers List of wrappers for binary data
     * @param pointerPos Location of tileset pointers
     * @param index Index of current tileset, to find absolute pointer location
     */
    void writeToRom(std::list<DataWrapper> &dataWrappers, const uint32_t pointerPos, const uint32_t index);

    /**
     * @brief writeToDisk Writes tileset to disk, uncompressed
     * @param directory Folder to write to
     * @return Was writing successful
     */
    bool writeToDisk(const QString &directory);

    /**
     * @brief readFromDisk Reads tileset from disk. Folder must contain Palette.bin, Tilesheet.gfx and Tiletable.ttb
     * @param directory Folder to read from
     * @return Self
     */
    Tileset readFromDisk(const QString &directory);

    std::vector<uint16_t> getPalette() const {return palette;}
    std::vector<TilesetTile> getTilesheet(bool easy = true) const;
    std::vector<TilesetBlock> getTilemap() const {return tilemap;}

    /**
     * @brief makeTilesheetImage Turns given SCE and CRE into a vector of tilesheet images
     * @param sce SCE
     * @param cre CRE
     * @param paletteIndex Which palette line to use (from SCE)
     * @return Vector of 8x8 images
     */
    static std::vector<QImage> *makeTilesheetGraphics(const Tileset &sce, const Tileset &cre, uint8_t paletteIndex);

    /**
     * @brief makeTilesheetImage Turns given graphical tilesheet into one big image
     * @param source Tilesheet vector
     * @return Image
     */
    static QImage makeTilesheetImage(std::vector<QImage> &source);

    /**
     * @brief makeTiletableGraphics Turns tiletable into vector of images
     * @param sce SCE
     * @param cre CRE
     * @param gfx Tilesheets, must contain 8 entires
     * @return Graphical tiletable
     */
    static std::vector<QImage> *makeTiletableGraphics(Tileset &sce, Tileset &cre, std::vector<std::vector<QImage>*> &gfx);

    /**
     * @brief makeTilesheetImage Turns given graphical tiletable into one big image
     * @param source Tiletable vector
     * @return Image
     */
    static QImage makeTiletableImage(std::vector<QImage> &source);

    std::vector<TilesetTile> makeRealTilesheet();
};

#endif // TILESET_H
