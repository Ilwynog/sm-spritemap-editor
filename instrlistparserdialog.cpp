#include "instrlistparserdialog.h"
#include "ui_instrlistparserdialog.h"
#include <QMessageBox>
#include <QInputDialog>
#include <iostream>

instrListParserDialog::instrListParserDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::instrListParserDialog)
{
    ui->setupUi(this);

    connect(ui->pushParse, &QPushButton::released, this, [=]() { emit sigParseInstList(); });
    connect(ui->pushDelCommand, &QPushButton::released, this, [=]() { int row = ui->tableCommands->currentRow(); if (row == -1) return; ui->tableCommands->removeRow(row); });
    connect(ui->pushAddCommand, &QPushButton::released, this, [=]() { ui->tableCommands->insertRow(ui->tableCommands->rowCount()); });
    connect(ui->pushLoadDefaultCommands, &QPushButton::released, this, [=]() { loadDefaultCommands(ui->comboDefaultCommands->currentText()); });
    connect(ui->tableInstructionList, &QTableWidget::itemSelectionChanged, this, [=]() {displaySpritemap();});
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, [=] { outputInstructionList();});
}

instrListParserDialog::~instrListParserDialog()
{
    delete ui;
}

void instrListParserDialog::setCommands(QList<QList<QString>> commands)
{
    ui->tableCommands->setRowCount(0);
    for (int i=0; i < commands.count(); i++)
    {
        ui->tableCommands->insertRow(i);
        for (int j=0; j < commands[i].count(); j++)
        {
            ui->tableCommands->setItem(i, j, new QTableWidgetItem(commands[i][j]));
        }
    }
}

void instrListParserDialog::clearInstructionList()
{
    ui->tableInstructionList->clearContents();
    ui->tableInstructionList->setRowCount(0);
}

void error(QString str, QWidget* parent)
{
    QMessageBox::warning(parent, "Error", str);
}

uint16_t instrListParserDialog::readWord(const std::vector<uint8_t> &data, uint32_t offset)
{
    if (offset+2 >= data.size()) return 0;
    return (data[offset+1] << 8) | (data[offset]);
}

uint32_t instrListParserDialog::readLong(const std::vector<uint8_t> &data, uint32_t offset)
{
    if (offset+3 >= data.size()) return 0;
    return (data[offset+1] << 16) | (data[offset+1] << 8) | (data[offset]);
}

void instrListParserDialog::loadInstructionList(const AnimationObject &animObject)
{
    clearInstructionList();

    QPair<QString, QList<QPair<QString, QString>>> list;
    bool ok;
    QPair<QString, QString> instruction;
    QString header, first;
    int timer;

    uint lists = animObject.instructionListCount();
    for (uint i=0; i < lists; i++)
    {
        list = animObject.getInstructionList(i);

        uint instrs = list.second.size();
        for (uint j=0; j < instrs; j++)
        {
            instruction = list.second[j];
            first = instruction.first;
            header = "Execute";
            timer = QString(instruction.first).toInt(&ok, 10);
            if (ok) //Hex number = draw instruction
            {
                header = "Draw";
                first = QString::number(timer, 16).rightJustified(4, '0').toUpper();
            }
            ui->tableInstructionList->insertRow(j);
            ui->tableInstructionList->setVerticalHeaderItem(j, new QTableWidgetItem(header));
            ui->tableInstructionList->setItem(j, 0, new QTableWidgetItem(first));
            ui->tableInstructionList->setItem(j, 1, new QTableWidgetItem(instruction.second));
        }
    }
}

void instrListParserDialog::displaySpritemap()
{
    auto item = ui->tableInstructionList->verticalHeaderItem(ui->tableInstructionList->currentRow());
    if (nullptr == item) return;
    if (item->text().compare("Draw") != 0) return;

    SpriteScene* scene = static_cast<SpriteScene*>(ui->viewInstrListPreview->scene());
    if (nullptr == scene)
    {
        scene = new SpriteScene(SpriteScene::TYPE::SPRITEMAP);
        scene->toggleGrid(false);
        ui->viewInstrListPreview->setScene(scene);
    }
    scene->clear();

    item = ui->tableInstructionList->item(ui->tableInstructionList->currentRow(), 1);
    QString name = item->text();
    //QPair<QString, QList<Sprite*>>
    QPair<QString, QList<Sprite*>> spritemap = animObj.getSpritemap(name);
    if (spritemap.second.isEmpty()) return;

    QList<Sprite*> sprites = spritemap.second;
    SpriteScene* sheets[] = {pal0Scene, pal1Scene, pal2Scene, pal3Scene, pal4Scene, pal5Scene, pal6Scene, pal7Scene};
    for (int i=0; i < sprites.count(); i++)
    {
        auto data = sprites[i]->getByteData();
        int pal = sprites[i]->getPalette();
        if ((pal < 0) || (pal > 7)) continue;
        QPixmap blerp = sheets[pal]->getSpriteGraphic(sprites[i]);
        Sprite* sprite = new Sprite(scene, data[0], data[1], data[2], data[3], data[4]);
        sprite->setPixmap(blerp);
        scene->addItem(sprite);
    }
}

void instrListParserDialog::outputInstructionList()
{
    emit sigAcceptInstList(animObj);
    return;

    AnimationObject out;

    //Spritemaps
    //for (int i=0; i < ui->spritemapList->count(); i++)
    //{
    //    QListWidgetItem* item = ui->spritemapList->item(i);
     //   QPair<QString, QList<Sprite*>> map = QPair<QString, QList<Sprite*>>(item->text(), item->data(Qt::UserRole).value<QList<Sprite*>>());
      //  out.addSpritemap(map);
    //}

    //Instruction list
    QList<QPair<QString, QString>> instrList;
    QTableWidgetItem* tItem0 = nullptr;
    QTableWidgetItem* tItem1 = nullptr;
    for (int i=0; i < ui->tableInstructionList->rowCount(); i++)
    {
        tItem0 = ui->tableInstructionList->item(i, 0);
        tItem1 = ui->tableInstructionList->item(i, 1);
        instrList.append(QPair<QString, QString>(tItem0->text(), tItem1->text()));
    }
    QPair<QString, QList<QPair<QString, QString>>> namedList;
    namedList.first = "Animation";
    namedList.second = instrList;
    out.addInstructionList(namedList);

    emit sigAcceptInstList(out);
}

void instrListParserDialog::loadDefaultCommands(QString str)
{
    QList<QList<QString>> list;
    QList<QString> command;

    if (str.compare("Enemies ($A0)") == 0)
    {
        command.append("Goto"); command.append("DW %instr"); command.append("80ED"); list.append(command); command.clear();
        command.append("Dec C & goto if nonzero"); command.append("DW %instr"); command.append("8108"); list.append(command); command.clear();
        command.append("Set C"); command.append("DW %hw"); command.append("8123"); list.append(command); command.clear();
        command.append("Sleep"); command.append(""); command.append("812F"); list.append(command); command.clear();
        command.append("Wait"); command.append("DW %hw"); command.append("813A"); list.append(command); command.clear();

        setCommands(list);
        return;
    }
    if (str.compare("Projectiles ($86)") == 0)
    {
        command.append("Sleep"); command.append(""); command.append("8159"); list.append(command); command.clear();
        command.append("Goto"); command.append("DW %instr"); command.append("81AB"); list.append(command); command.clear();
        command.append("Dec C & goto if nonzero"); command.append("DW %instr"); command.append("81C6"); list.append(command); command.clear();
        command.append("Set C"); command.append("DW %hw"); command.append("81D5"); list.append(command); command.clear();

        setCommands(list);
        return;
    }
    if (str.compare("Generic sprite objects ($B4)") == 0)
    {
        command.append("Goto"); command.append("DW %instr"); command.append("BD12"); list.append(command); command.clear();

        setCommands(list);
        return;
    }
}

Sprite *parseSprite(const std::vector<uint8_t> &data, const int ptr)
{
    if (static_cast<int>(data.size()) < ptr+5) throw std::runtime_error("Not enough data for sprite");

    Sprite *sprite = new Sprite(nullptr,
                                data[ptr+0],
                                data[ptr+1],
                                data[ptr+2],
                                data[ptr+3],
                                data[ptr+4]
                                );

    return sprite;
}

QList<Sprite*> parseSpritemap(std::vector<uint8_t> const &rom, uint32_t mapPointer)
{
    uint16_t spriteCount = (rom[mapPointer+1] << 8) | rom[mapPointer]; //TODO: Check sanity
    mapPointer += 2;

    QList<Sprite*> map;
    Sprite* sprite = nullptr;
    for (uint16_t i=0; i < spriteCount; i++)
    {
        sprite = parseSprite(rom, mapPointer);
        if (nullptr == sprite) continue;

        //map.append(sprite->raw());
        map.prepend(sprite->raw());
        mapPointer += 5;
    }

    return map;
}

void instrListParserDialog::parseInstructionList()
{
    bool ok = false;
    bool stopAtSleep = ui->checkStopSleep->isChecked();

    if (rom.empty())
    {
        error("No ROM loaded", this);
        return;
    }
    uint32_t start = ui->lineStartPointer->text().toInt(&ok, 16);
    if (!ok)
    {
        error("Invalid start pointer", this);
        return;
    }
    uint32_t end = ui->lineEndPointer->text().toInt(&ok, 16);
    if (!ok)
    {
        error("Invalid end pointer", this);
        return;
    }
    if (end <= start)
    {
        error("End can't be before Start", this);
        return;
    }

    uint8_t mapBank = (start&0xFF0000) >> 16;
    QString str = ui->lineMapBank->text();
    if (!str.isEmpty())
    {
        mapBank = ui->lineMapBank->text().toInt(&ok, 16);
        if (!ok)
        {
            error("Invalid map bank", this);
            return;
        }
    }

    AnimationObject result;
    QList<QList<QString>> commands = getCommands();
    QPair<QString, QList<QPair<QString, QString>>> list;
    list.first = QString("Spritemap_%1").arg(QString::number(LoRom::pc2snes(start), 16).toUpper());
    QPair<QString, QString> instruction;
    //QString str;
    uint16_t cmd, arg;
    uint32_t currentPtr = start;
    uint32_t sprMapPtr;

    uint8_t instrBank = start >> 16;

    QPair<QString, QList<Sprite*>> spritemap;
    QList<QPair<QString, QList<Sprite*>>> spritemapList;
    QList<uint16_t> parsedSpritemaps;

    QList<QPair<int, int>> lineOffsets; QPair<int, int> lineOffset; //Line number, pointer to rom
    QList<QList<QString>> argTypesList;
    for (int i=0; i < commands.count(); i++)
    {
        QStringList format = commands[i][1].split(' ');
        QList<QString> sizes;
        for (int j=0; j < format.count(); j++)
        {
            QString s = format[j];
            if (!s.startsWith('%')) continue;
            sizes.push_back(s);
        }
        argTypesList.push_back(sizes);
    }

    int currentLine = -1;
    int linesLeft = 100;
    while (currentPtr < end)
    {
        if (!instruction.first.isEmpty()) list.second.push_back(instruction);
        currentLine++;
        linesLeft--;

        if (linesLeft < 0)
        {
            QMessageBox::StandardButton button;
            button = QMessageBox::question(this, "Long instruction list", QString("%1 instructions parsed, continue?").arg(currentLine));
            if (button == QMessageBox::No)
            {
                currentPtr = end;
                continue;
            }
            linesLeft = 49;
        }

        cmd = readWord(rom, LoRom::snes2pc(currentPtr));
        lineOffset.first = currentLine;
        lineOffset.second = currentPtr;
        lineOffsets.push_back(lineOffset);
        currentPtr += 2;
        if (cmd < 0x8000) //Draw instruction
        {
            instruction.first = QString::number(cmd, 10);//.rightJustified(4, '0').toUpper();
            cmd = readWord(rom, LoRom::snes2pc(currentPtr));
            currentPtr += 2;
            str = QString("Frame_%1").arg(QString::number(cmd, 16).rightJustified(4, '0').toUpper());
            instruction.second = str;
            if (parsedSpritemaps.contains(cmd)) continue;
            //New spritemap, add to list
            spritemap = QPair<QString, QList<Sprite*>>();
            spritemap.first = str;
            sprMapPtr = LoRom::snes2pc( ((mapBank<<16)&0xFF0000) | cmd);
            spritemap.second = parseSpritemap(rom, sprMapPtr);
            spritemapList.push_back(spritemap);
            parsedSpritemaps.push_back(cmd);
            continue;
        }
        else //Code instruction, currentPtr points to arguments
        {
            QStringList argStrings;
            bool ok = false;
            str = QString::number(cmd, 16).rightJustified(4, '0').toUpper();
            for (int i=0; i < commands.count(); i++)
            {
                if (commands[i].last().compare(str) == 0)
                {
                    //Known command
                    ok = true;
                    instruction.first = commands[i][0];
                    QList<QString> argTypes = argTypesList[i];
                    for (int j=0; j < argTypes.count(); j++)
                    {
                        QString type = argTypes[j];
                        if (type.compare("%hb") == 0)
                        {
                            //Byte
                            str = QString::number(rom[LoRom::snes2pc(currentPtr)]);
                            argStrings.push_back(str);
                            currentPtr += 1;
                            continue;
                        }
                        if (type.compare("%hw") == 0)
                        {
                            //Word
                            str = QString::number(readWord(rom, LoRom::snes2pc(currentPtr)));
                            argStrings.push_back(str);
                            currentPtr += 2;
                            continue;
                        }
                        if (type.compare("%hl") == 0)
                        {
                            //Long TODO: Implement
                            str = QString::number(readLong(rom, LoRom::snes2pc(currentPtr)));
                            argStrings.push_back(str);
                            currentPtr += 3;
                            continue;
                        }
                        if (type.compare("%hd") == 0)
                        {
                            //Double TODO: Implement
                            argStrings.push_back("%hd not supported");
                            currentPtr += 4;
                            continue;
                        }
                        if (type.compare("%instr") == 0)
                        {
                            //Label to instruction list
                            arg = readWord(rom, LoRom::snes2pc(currentPtr));
                            int targetOffset = ( (instrBank << 16)&0xFF0000) | arg ;
                            str = "Invalid target line";
                            for (QPair<int,int> &offset: lineOffsets)
                            {
                                if (offset.second == targetOffset)
                                {
                                    str = QString::number(offset.first);
                                }
                            }
                            argStrings.push_back(str);
                            currentPtr += 2;
                            continue;
                        }
                    }
                    if (stopAtSleep && (instruction.first.compare("Sleep") == 0))
                    {
                        end = currentPtr;
                    }
                    break;
                }
            }
            instruction.second = argStrings.join(',');
            if (ok) continue;
            //Unknown command
            else
            {
                std::cerr << "Unknown unhandled command: " << str.toStdString() << std::endl;
                QString label = QString("Unknown command: $%1.\nGive name").arg(QString::number(cmd, 16).toUpper());
                QString name = QInputDialog::getText(this, "Unknown command", label);
                if (name.isNull()) continue; //User pressed Cancel, exit
                label = "Define argument formant using %hb %hw %hl\nExample: DW %hw : DB %hb";
                QString formatStr = QInputDialog::getText(this, "Unkown command", label);

                QStringList format = formatStr.split(' ');
                QList<QString> sizes;
                for (int j=0; j < format.count(); j++)
                {
                    QString s = format[j];
                    if (!s.startsWith('%')) continue;
                    sizes.push_back(s);
                }
                argTypesList.push_back(sizes);

                QList<QString> newCommand;
                newCommand.append(name);
                newCommand.append(formatStr);
                newCommand.append( QString::number(cmd, 16).toUpper() );
                commands.append(newCommand);

                currentPtr -= 2; //Try parsing this command again
                continue;
            }
        }
    }
    //Push last instruction
    list.second.push_back(instruction);

    for (int i=0; i < spritemapList.count(); i++)
        result.addSpritemap(spritemapList[i]);
    result.addInstructionList(list);

    result.setCommandList(commands);

    animObj = result;

    loadInstructionList(animObj);
}

QList<QList<QString>> instrListParserDialog::getCommands()
{
    //Code commands
    QList<QList<QString>> commandList;
    QList<QString> command;
    QString str;
    QTableWidgetItem* tItem0 = nullptr;

    for (int i=0; i < ui->tableCommands->rowCount(); i++)
    {
        command.clear();
        for (int j=0; j < ui->tableCommands->columnCount(); j++) //Name, arg format, pointer
        {
            str = "";
            tItem0 = ui->tableCommands->item(i, j);
            if (nullptr != tItem0) str = tItem0->text();
            command.append(str.isEmpty() ? "" : str);
        }

        commandList.append(command);
    }

    return commandList;
}
