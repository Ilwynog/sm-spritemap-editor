#ifndef ANIMATIONOBJECT_H
#define ANIMATIONOBJECT_H

#include "gui/spritescene.h"
#include "rom/json.hpp"
#include <QTimer>


class AnimationObject
{
public:
    AnimationObject();

    /**
     * @brief AnimationObject Construct animation object from json data
     * @param in Json input
     */
    AnimationObject(const nlohmann::json in);

    /**
     * @brief addSpritemap Add spritemap to animation object
     * @param spritemap Spritemap
     * @return Success
     */
    bool addSpritemap(QPair<QString, QList<Sprite *> > &spritemap);

    /**
     * @brief getSpritemap Return spritemap at index i
     * @param i Index
     * @return Spritemap
     */
    QPair<QString, QList<Sprite*>> getSpritemap(int i) const;

    /**
     * @brief getSpritemap Return spritemap by name
     * @param name Name
     * @return Spritemap
     */
    QPair<QString, QList<Sprite*>> getSpritemap(const QString &name) const;

    /**
     * @brief getInstructionLists Return instruction list at index i
     * @param i Index
     * @return Instruction list
     */
    QPair<QString, QList<QPair<QString, QString>>> getInstructionList(int i) const;

    QList<QList<QString>> getCommandList() const;

    /**
     * @brief spritemapCount Get count of spritemaps
     * @return Spritemap count
     */
    uint spritemapCount() const;

    /**
     * @brief instructionListCount Get count of instruction lists
     * @return Instruction list count
     */
    uint instructionListCount() const;

    uint commandCount() const;

    void addInstructionList(QPair<QString, QList<QPair<QString, QString>>> instList);

    void setGfxData(std::vector<uint8_t> data, int offset);
    void setPaletteData(std::array<uint16_t, 128> data);
    void setPaletteData(std::vector<uint16_t> data);
    void setPaletteLine(std::vector<uint16_t> data, int i);

    void setCommandList(QList<QList<QString>> commandList);

    const std::vector<uint8_t> getGfxData() const;
    const std::array<uint16_t, 128> getPaletteData() const;

    nlohmann::json exportToJson();
    QString exportToAsm() const;

private:
    /**
     * @brief spritemapList List of <name, spritemap> pairs
     */
    QList<QPair<QString, QList<Sprite*>>> spritemapList;
    std::vector<uint8_t> gfxData;
    std::array<uint16_t, 128> paletteData;

    QList<QPair<QString, QList<QPair<QString, QString>>>> instructionLists;
    QList<QList<QString>> commandList;

    /**
     * @brief argumentToString Transforms given argument line into string to be exported
     * @param cmd Command whose argument format will be used
     * @param arg Argument line
     * @param labeledLines List of instruction list line numbers that need labels
     * @return String
     */
    QString argumentToExportString(QString cmd, QString arg, QList<int> &labeledLines) const;

    /**
     * @brief argumentToString Transforms given command name into string to be exported
     * @param arg Command
     * @return String
     */
    QString commandToPointer(QString arg) const;
};

//Animator processes the instruction list
class Animator : public QObject
{
    Q_OBJECT

public:
    Animator();
    Animator(QList<QPair<QString, QString>> instructionList);

    void start();
    void stop();

    bool setInstruction(int i);
    void setCounter(int i);
    void setLinkInstruction(int i);

signals:
    void currentDrawInstruction(int);
    void counterChanged(int);
    void timerChanged(int);
    void terminate();
    void sleeping();

private:
    QList<QPair<QString, QString>> instructionList;
    int instruction;
    uint16_t counter; //Called "Timer" in bank logs
    int linkInstruction;

    QTimer timer;
    const int fps = 60;

    void processInstructionList();
};

#endif // ANIMATIONOBJECT_H
