#include "animationobject.h"
#include <iostream>

AnimationObject::AnimationObject()
{
    gfxData.assign(0x4000, 0);
    for (uint i=0; i < paletteData.size(); i++)
    {
        paletteData[i] = i | (i << 5) | (i << 10); //Shitty default palette
    }
}

AnimationObject::AnimationObject(const nlohmann::json in)
{
    nlohmann::json j, obj;

    //gfx data
    if (in.contains("Graphics"))
    {
        j = in["Graphics"];
        if (j.contains("Base"))
        {
            j = j["Base"];
            if (j.contains("Data"))
            {
                gfxData = j["Data"].get<std::vector<uint8_t>>();
            }
        }
    }

    //palette
    if (in.contains("Palette"))
    {
        j = in["Palette"];
        paletteData = j.get<std::array<uint16_t, 128>>();
    }

    //spritemaps
    if (in.contains("Spritemaps"))
    {
        QString name;
        std::vector<uint8_t> spriteData;
        Sprite* sprite = nullptr;
        QList<Sprite*> spriteList;
        j = in["Spritemaps"];
        if (!j.is_array()) return;
        for (uint i=0; i < j.size(); i++)
        {
            spriteList.clear();
            obj = j[i];
            name = QString::fromStdString(obj["Name"].get<std::string>());
            spriteData = obj["Sprites"].get<std::vector<uint8_t>>();
            spriteData.resize( (spriteData.size() / 5) * 5, 0 ); //Ensure 5n bytes
            for (uint c=0; c < spriteData.size(); c+=5)
            {
                sprite = new Sprite(nullptr, spriteData[c+0], spriteData[c+1], spriteData[c+2], spriteData[c+3], spriteData[c+4]);
                spriteList.push_back(sprite);
            }
            spritemapList.push_back(QPair<QString, QList<Sprite*>>(name, spriteList));
        }
    }

    //instruction lists
    nlohmann::json animation;
    if (in.contains("InstructionLists"))
    {
        j = in["InstructionLists"];
        if (!j.is_array()) return;

        QString name;
        QPair<QString, QString> instruction;
        QList<QPair<QString, QString>> instrList;
        QString type, str;
        for (uint i=0; i < j.size(); i++)
        {
            instrList.clear();
            name = QString::fromStdString(j[i]["Name"].get<std::string>());
            animation = j[i]["Instructions"];
            for (uint k=0; k < animation.size(); k++)
            {
                obj = animation[k];
                type = QString::fromStdString(obj["Type"].get<std::string>());
                if (type.compare("Draw") == 0) //Draw instruction
                {
                    str = QString::number(obj["Frame"]["Timer"].get<int>()).toUpper();
                    instruction.first = str;
                    str = QString::fromStdString(obj["Frame"]["Spritemap"].get<std::string>());
                    instruction.second = str;
                }
                if (type.compare("Execute") == 0) //Code instruction
                {
                    str = QString::fromStdString(obj["Definition"]["Command"].get<std::string>());
                    instruction.first = str;
                    str = QString::fromStdString(obj["Definition"]["Argument"].get<std::string>());
                    instruction.second = str;
                }
                instrList.push_back(instruction);
            }
            addInstructionList(QPair<QString, QList<QPair<QString, QString>>>(name, instrList));
        }
    }

    //Code commands
    if (in.contains("CodeCommands"))
    {
        j = in["CodeCommands"];
        if (!j.is_array()) return;

        QList<QList<QString>> commandList;
        QList<QString> command;

        for (uint i=0; i < j.size(); i++)
        {
            obj = j[i];
            command.clear();
            command.append( QString::fromStdString(obj["Name"].get<std::string>()) );
            command.append( QString::fromStdString(obj["Arg Format"].get<std::string>()) );
            command.append( QString::fromStdString(obj["Pointer"].get<std::string>()) );

            commandList.append(command);
        }

        setCommandList(commandList);
    }
}

/**
 * @brief addSpritemap Add spritemap to animation object
 * @param spritemap Spritemap
 * @return Success
 */
bool AnimationObject::addSpritemap(QPair<QString, QList<Sprite *>> &spritemap)
{
    spritemapList.append(spritemap);
    return true;
}

/**
     * @brief getSpritemap Return spritemap at index i, or default-constructed if doesn't exist
     * @param i Index
     * @return Spritemap
     */
QPair<QString, QList<Sprite *> > AnimationObject::getSpritemap(int i) const
{
    return spritemapList.value(i);
}

/**
     * @brief getSpritemap Return spritemap by name, or default-constructed if doesn't exist
     * @param name Name
     * @return Spritemap
     */
QPair<QString, QList<Sprite*>> AnimationObject::getSpritemap(const QString &name) const
{
    QPair<QString, QList<Sprite*>> result;
    for (int i=0; i<spritemapList.count(); i++)
    {
        if (name.compare(spritemapList.at(i).first) != 0) continue;
        result = spritemapList.at(i);
        break;
    }
    return result;
}

QPair<QString, QList<QPair<QString, QString>>> AnimationObject::getInstructionList(int i) const
{
    return instructionLists.value(i);
}

QList<QList<QString> > AnimationObject::getCommandList() const
{
    return commandList;
}

uint AnimationObject::spritemapCount() const
{
    return spritemapList.count();
}

uint AnimationObject::instructionListCount() const
{
    return instructionLists.count();
}

uint AnimationObject::commandCount() const
{
    return commandList.count();
}

void AnimationObject::addInstructionList(QPair<QString, QList<QPair<QString, QString> > > instList)
{
    instructionLists.push_back(instList);
}

void AnimationObject::setGfxData(std::vector<uint8_t> data, int offset)
{
    std::copy(data.begin(), data.end(), gfxData.begin()+offset); //TODO: Bounds check
}

void AnimationObject::setPaletteData(std::array<uint16_t, 128> data)
{
    paletteData = data;
}

void AnimationObject::setPaletteData(std::vector<uint16_t> data)
{
    std::copy(data.begin(), data.end(), paletteData.begin());
}

void AnimationObject::setPaletteLine(std::vector<uint16_t> data, int i)
{
    std::copy(data.begin(), data.end(), paletteData.begin()+i*16);
}

void AnimationObject::setCommandList(QList<QList<QString> > commandList)
{
    this->commandList = commandList;
}

const std::vector<uint8_t> AnimationObject::getGfxData() const
{
    return gfxData;
}

const std::array<uint16_t, 128> AnimationObject::getPaletteData() const
{
    return paletteData;
}

nlohmann::json AnimationObject::exportToJson()
{
    nlohmann::json result;
    nlohmann::json obj;

    //Spritemaps
    for (int i=0; i < spritemapList.count(); i++)
    {
        QString name = spritemapList[i].first;
        QList<Sprite*> map = spritemapList[i].second;
        std::vector<uint8_t> bytes;
        nlohmann::json sprite;
        sprite["Name"] = name.toStdString();
        //for (int j=map.count()-1; j >= 0; j--)
        for (int j=0; j < map.count(); j++)
        {
            bytes = map[j]->getByteData();
            for (uint k=0; k < bytes.size(); k++)
            {
                sprite["Sprites"].emplace_back(bytes[k]); //TODO: use parse() by explicitly setting 'sprite' to array type? (sprite = json::array())
            }
        }

        obj["Spritemaps"].push_back(sprite);
    }

    //Instruction lists
    for (int i=0; i < instructionLists.count(); i++)
    {
        QPair<QString, QList<QPair<QString, QString>>> list = instructionLists[i];
        QString name = list.first;
        QList<QPair<QString, QString>> instrs = list.second;
        nlohmann::json jList, instr;
        jList["Name"] = name.toStdString();

        for (int j=0; j < instrs.count(); j++)
        {
            instr.clear();
            name = instrs[j].first;
            bool ok = false;
            int timer = name.toInt(&ok, 16);
            if (ok) //hex number, ie. draw instruction
            {
                instr["Type"] = "Draw";
                instr["Frame"]["Timer"] = timer;
                instr["Frame"]["Spritemap"] = instrs[j].second.toStdString();
            }
            else //not a hex number, ie. code command
            {
                instr["Type"] = "Execute";
                instr["Definition"]["Command"] = name.toStdString();
                instr["Definition"]["Argument"] = instrs[j].second.toStdString();
            }

            jList["Instructions"].push_back(instr);
        }
        obj["InstructionLists"].push_back(jList);
    }

    //Code commands
    for (int i=0; i < commandList.count(); i++)
    {
        QList<QString> command = commandList[i];
        if (command.count() != 3) continue; //No 3 parameters => invalid command

        nlohmann::json jList;
        jList["Name"] = command[0].toStdString();
        jList["Arg Format"] = command[1].toStdString();
        jList["Pointer"] = command[2].toStdString();

        obj["CodeCommands"].push_back(jList);
    }

    obj["Type"] = "Spritemap";
    obj["Graphics"]["Base"]["Data"] = gfxData;
    obj["Graphics"]["Base"]["Offset"] = 0;
    obj["Graphics"]["AltTest"]["Data"] = {1,2,3,4,5};
    obj["Graphics"]["AltTest"]["Offset"] = 17;
    obj["Palette"] = paletteData;

    result = obj;
    return result;
}

QString AnimationObject::exportToAsm() const
{
    QStringList result;
    QString baseLabel = "SpreditExport";

    result.append(baseLabel % ":\n");

    result.append(".spritemaps");

    //Spritemaps
    for (uint i=0; i < spritemapCount(); i++)
    {
        QPair<QString, QList<Sprite*>> spritemap = spritemapList[i];
        QString name = spritemap.first;
        name = name.simplified().replace(" ", "");
        QList<Sprite*> map = spritemap.second;
        result.append(QString("..%1").arg(name));
        result.append(QString("  DW $%1").arg(QString::number(map.size(), 16).rightJustified(4, '0').toUpper()));
        //for (uint j=0; j < map.count(); j++)
        for (int j=map.count()-1; j >= 0; j--)
        {
#define byte(a) QString::number(a,16).rightJustified(2,'0').toUpper()
            //std::vector<uint8_t> bytes = map[j]->getByteData();
            QString temp = QString("  %1").arg(map[j]->toAsmString());
            /*QString temp = QString("  DB $%1,$%2,$%3,$%4,$%5").arg(byte(bytes[0]))
                               .arg(byte(bytes[1]))
                               .arg(byte(bytes[2]))
                               .arg(byte(bytes[3]))
                               .arg(byte(bytes[4]));*/
#undef byte
            result.append(temp);
        }
        result.last().append("\n");
    }

    //Instruction lists
    result.append("\n.instructionLists");
    for (uint i=0; i < instructionListCount(); i++)
    {
        QPair<QString, QList<QPair<QString, QString>>> instList = instructionLists[i];
        QList<int> labeledLines;
        QString name = instList.first;
        name = name.simplified().replace(" ", "");
        result.append( QString("..%1").arg(name) );
        int instrListStartLine = result.count();

        QList<QPair<QString, QString>> instructions = instList.second;
        QPair<QString, QString> instr;
        QString cmd, arg;
        for (uint j=0; j < instructions.count(); j++)
        {
            instr = instructions[j];
            cmd = instr.first;
            arg = instr.second;
            //Export instructions
            bool ok = false;
            int timer = cmd.toInt(&ok, 16);
            if (ok) //Plain old draw instruction
            {
                cmd = QString::number(timer, 16).rightJustified(4, '0').toUpper();
                arg = arg.simplified().replace(" ", "");
                result.append( QString("  DW $%1, %2_spritemaps_%3").arg(cmd, baseLabel, arg));
                continue;
            }
            else //Code instruction
            {
                if (!arg.isEmpty()) {arg = argumentToExportString(cmd, arg, labeledLines);}
                cmd = commandToPointer(cmd);
                QString f = arg.isEmpty() ? "  DW $%1" : "  DW $%1 : %2";
                result.append(QString(f).arg(cmd, arg));
            }
        }
        int instrListEndLine = result.count();
        std::sort(labeledLines.begin(), labeledLines.end());
        while (!labeledLines.isEmpty()) //Write labels if needed
        {
            int n = labeledLines.takeLast();
            if (instrListStartLine+n > instrListEndLine)
            {
                std::cerr << "Instruction list target line out of bounds.\n";
                continue;
            }
            result.insert(instrListStartLine+n, QString("...line%1").arg(n));
        }
    }

    //Convert result to one string
    QString finalResult;
    for (int i=0; i < result.count(); i++)
    {
        finalResult += result[i] % "\n";
    }

    return finalResult;
}

/**
     * @brief argumentToString Transforms given argument line into string to be exported
     * @param cmd Command whose argument format will be used
     * @param arg Argument line
     * @param labeledLines List of instruction list line numbers that need labels
     * @return String
     */
QString AnimationObject::argumentToExportString(QString cmd, QString arg, QList<int> &labeledLines) const
{
    QList<QString> command;
    QString format, result;
    bool ok;

    for (uint i=0; i < commandList.count(); i++)
    {
        command = commandList[i];
        if (command.first().compare(cmd)) continue;
        //Match found
        format = command[1];
        break;
    }

    format = format.trimmed();
    if (format.isEmpty()) return result;

    QStringList formatParts = format.split(' ');
    QStringList argParts = arg.split(',', Qt::SkipEmptyParts);
    QString part;
    int argIndex = 0;
    for (uint i=0; i < formatParts.count(); i++)
    {
        part = formatParts[i];
        if (part.isEmpty()) {result += " "; continue;}
        //if (!part.startsWith('%')) continue;
        if (!part.startsWith('%'))
        {
            result += part + " ";
            continue;
        }

        if (part.compare("%instr") == 0) //Label to instruction list
        {
            if (argParts.size() <= argIndex) return "Not enough arguments for instruction";
            int lineNumber = argParts[argIndex].toInt(&ok);
            if (!ok) return "Instruction list line number arg not valid";
            if (!labeledLines.contains(lineNumber)) labeledLines.push_back(lineNumber);
            result += QString("...line%1 ").arg(lineNumber);
        }
        if (part.compare("%s") == 0) //Straight up string
        {
            if (argParts.size() <= argIndex) return "Not enough arguments for instruction";
            result += argParts[argIndex] % " ";
        }
        if (part.compare("%hb") == 0) //Hex number, byte
        {
            if (argParts.size() <= argIndex) return "Not enough arguments for instruction";
            int hex = argParts[argIndex].toInt(&ok, 16) & 0xFF;
            if (!ok) return "Hex value is not valid";
            result += '$' % QString::number(hex, 16).rightJustified(2, '0').toUpper() % " ";
        }
        if (part.compare("%hw") == 0) //Hex number, word
        {
            if (argParts.size() <= argIndex) return "Not enough arguments for instruction";
            int hex = argParts[argIndex].toInt(&ok, 16) & 0xFFFF;
            if (!ok) return "Hex value is not valid";
            result += '$' % QString::number(hex, 16).rightJustified(4, '0').toUpper() % " ";
        }
        if (part.compare("%d") == 0) //Decimal number
        {
            if (argParts.size() <= argIndex) return "Not enough arguments for instruction";
            int val = argParts[argIndex].toInt(&ok);
            if (!ok) return "Dec value is not valid";
            result += QString::number(val, 10).toUpper() % " ";
        }

        argIndex++;
    }

    return result;
}

QString AnimationObject::commandToPointer(QString arg) const
{
    QList<QString> command;

    for (uint i=0; i < commandList.count(); i++)
    {
        command = commandList[i];
        if (command.first().compare(arg)) continue;
        //Match found
        return command.last();
    }

    return "Command not found";
}

/////////////////////////////////////////////////////////////
//           Animator implementation                       //
/////////////////////////////////////////////////////////////
Animator::Animator()
{
    //
}
Animator::Animator(QList<QPair<QString, QString> > instructionList)
{
    this->instructionList = instructionList;
    instruction = 0;
    counter = 0;
    linkInstruction = 0;

    QObject::connect(&timer, &QTimer::timeout, [=]() { processInstructionList(); });
}

void Animator::start()
{
    timer.start( 1.0 / fps );
}

void Animator::stop()
{
    timer.stop();
}

bool Animator::setInstruction(int i)
{
    instruction = i; //TODO: Error checking
    return true;
}

void Animator::setCounter(int i)
{
    counter = i&0xFFFF;
}

void Animator::processInstructionList()
{
    QPair<QString, QString> currentInstruction;
    QString cmd, arg;

    int processedInstructions = 0; //Used to break out of infinite loops
    while (true)
    {
        if ((instruction < 0) || (instruction >= instructionList.count()) || (processedInstructions > 50))
        {
            //Instruction out of bounds or processed too many instructions => likely infinite loop
            emit terminate();
            return;
        }

        currentInstruction = instructionList[instruction];
        cmd = currentInstruction.first; //Timer or code name
        arg = currentInstruction.second; //Spritemap name or code arg

        bool ok;
        int delay = cmd.toInt(&ok, 16);
        if (ok)
        {
            //Draw instruction
            timer.setInterval(delay*1000 / fps);
            //emit SIGNAL(currentInstruction(instruction));
            emit currentDrawInstruction(instruction);
            emit timerChanged(delay);
            instruction++;
            return;
        }
        else
        {
            //Code instruction
            if (cmd.compare("Goto") == 0)
            {
                processedInstructions++;
                int target = arg.toInt(&ok);
                if (ok) {instruction = target; continue;}
            }
            if (cmd.compare("Set C") == 0)
            {
                processedInstructions++;
                int val = arg.toInt(&ok);
                if (ok)
                {
                    counter = val;
                    instruction++;
                    emit counterChanged(counter);
                    continue;}
            }
            if (cmd.compare("Dec C & goto if nonzero") == 0)
            {
                processedInstructions++;
                counter--;
                emit counterChanged(counter);
                int target = arg.toInt(&ok);
                if (counter == 0) {instruction++; continue;}
                if (ok) {instruction = target; continue;}
            }
            if (cmd.compare("Sleep") == 0)
            {
                processedInstructions++;
                emit currentDrawInstruction(instruction);
                emit sleeping();
                return;
            }
            if (cmd.compare("Wait") == 0)
            {
                processedInstructions++;
                int val = arg.toInt(&ok);
                if (ok)
                {
                    timer.setInterval(val*1000 / fps);
                    emit currentDrawInstruction(instruction);
                    emit timerChanged(val);
                    instruction++;
                    return;
                }
            }
            instruction++;
            processedInstructions++;
        }
    }

    return;
}
