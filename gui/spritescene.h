#ifndef SPRITESCENE_H
#define SPRITESCENE_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <memory>

class Sprite;

class SpriteScene : public QGraphicsScene
{
    Q_OBJECT

public:
    enum TYPE{GFX, SPRITEMAP};

    SpriteScene(TYPE type) {this->type = type; QGraphicsScene(); connect(this, &SpriteScene::selectionChanged, this, &SpriteScene::emitSpriteData);
                           selectionItem = new QGraphicsRectItem; selectionItem->setZValue(1); addItem(selectionItem);}
    QPixmap getSelectedSprite();
    QPixmap getSpriteGraphic(Sprite *sprite);
    void setGfxSheet(QImage &sheet) {this->gfxSheet = sheet;}
    QImage getGfxSheet() {return gfxSheet;}

    void clear();
    void addItem(QGraphicsItem *item);

private:
   QGraphicsRectItem* selectionItem = nullptr;
    bool useLarge = false;
    uint8_t smallSprite = 8;
    uint8_t largeSprite = 16;
    TYPE type;
    QImage gfxSheet; //For gfx viewer only. Used to get transparent tiles easily
    bool drawGrid = true;

    /**
     * @brief sprSize Return current sprite size depending on useLarge
     * @return Sprite size (8, 16, 32, 64)
     */
    uint8_t sprSize(bool large);

    /**
     * @brief emitSpriteData Emits the values of selected sprite for updating the gui
     */
    void emitSpriteData();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
    void drawBackground(QPainter *painter, const QRectF &rect) override;

public slots:
    void setUseLarge(bool large) {useLarge = large;}
    void setSmallSize(uint8_t size) {smallSprite = size;}
    void setLargeSize(uint8_t size) {largeSprite = size;} //TODO: Use enum to hardcode specific possibilities?
    void selectSpriteTile(Sprite* sprite);
    void translateSprites(QPoint delta);
    void requestSprite();
    void toggleGrid(bool enable);

signals:
    void spriteXPos(int);
    void spriteYPos(int);
    void spriteLarge(bool);
    void spriteTileIndex(int); //TODO: More signals as needed
    void spritePalette(int);
    void spritePriority(int);

    void spriteObject(Sprite*);

    //GFX scene signals
    void spriteDoubleClicked(int); //Emits tile index
};


/**
 * @brief The Sprite class Defines a single sprite. A sprite has a position, tile number to use, flips, layer priority, and palette index
 */
class Sprite : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

    uint16_t tileIndex = 0; //Maximum 0x01FF
    uint16_t x = 0; // -FF - FF
    uint16_t y = 0; //  00 - FF
    bool xFlip = false;
    bool yFlip = false;
    bool large = false;
    uint8_t priority = 0; //0,1,2 or 3. TODO: Define enum later
    uint8_t palette = 0; //1,2,3,4,5,6,7 TODO: Define enum later

public:
    Sprite(SpriteScene* scene);
    Sprite(SpriteScene* scene, uint8_t byte0, uint8_t byte1, uint8_t byte2, uint8_t byte3, uint8_t byte4);
    void connectSignals();

    uint16_t getTileIndex() const {return tileIndex;}
    std::vector<uint8_t> getByteData();
    /**
     * @brief raw Return raw sprite that is not in any scene
     * @return Raw sprite
     */
    Sprite *raw();

    /**
     * @brief emitData Emits values of each property, for updating GUI
     */
    void emitData() {emit spriteObject(this);
                     emit xPos(x); emit yPos(y); emit tileVal(tileIndex); emit sprLarge(large);
                     emit sprPalette(palette); emit sprPriority(priority);} //TODO: Emit more signals as they are added
    bool isLarge() const {return large;}
    bool isFlipX() const {return xFlip;}
    bool isFlipY() const {return yFlip;}
    uint16_t getX() const {return x;}
    uint16_t getY() const {return y;}
    uint8_t getPalette() const {return palette;}
    uint8_t getPriority() const {return priority;}

    QString toAsmString();

protected:
    //void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

public slots:
    void flipX();
    void flipY();

    /**
     * @brief setPos Sets position values
     * @param pos Position, in view coordinates. Used to calculate raw X/Y position data
     */
    void setPosition(QPoint pos);

    void setTileIndex(int val) {tileIndex = val & 0x01FF;}
    void setIsLarge(bool val) {large = val;}
    void setPalette(int val) {palette = val & 0x07;}
    void setPriority(int val) {priority = val & 0x03;}

signals:
    void delta(QPoint);
    void xPos(int);
    void yPos(int);
    void tileVal(int);
    void sprLarge(bool); //TODO: More signals for other properties
    void sprPalette(int);
    void sprPriority(int);

    void spriteObject(Sprite*);
};

#endif // SPRITESCENE_H
