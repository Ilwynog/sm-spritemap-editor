#include "spritescene.h"
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsRectItem>
#include <memory>
#include <QWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QGraphicsView>

#include <QApplication>

QPixmap SpriteScene::getSelectedSprite()
{
    if (!selectionItem) return QPixmap();

    QPixmap result(selectionItem->rect().size().toSize());
    QPainter painter(&result);
    //TODO: Implement
    return QPixmap();
}

QPixmap SpriteScene::getSpriteGraphic(Sprite *sprite)
{
    QRectF rect(0,0, sprSize(sprite->isLarge()), sprSize(sprite->isLarge()));
    uint8_t tileX = sprite->getTileIndex() & 0x000F;
    uint8_t tileY = (sprite->getTileIndex() & 0x01F0) >> 4;

    rect.translate(QPointF(tileX*8,
                             tileY*8
                             )); //TODO: Handle large tiles going 'off-screen'

    return QPixmap::fromImage( (gfxSheet.copy(rect.toRect()))
                               .mirrored(sprite->isFlipX(), sprite->isFlipY()));
}

void SpriteScene::clear()
{
    QGraphicsScene::clear();

    selectionItem = new QGraphicsRectItem();
    selectionItem->setZValue(1);
    addItem(selectionItem);
}

void SpriteScene::addItem(QGraphicsItem *item)
{
    QGraphicsScene::addItem(item);

    Sprite *sprite = dynamic_cast<Sprite*>(item);
    if (nullptr != sprite)
        sprite->connectSignals();
}

uint8_t SpriteScene::sprSize(bool large) {return large ? largeSprite : smallSprite;}

void SpriteScene::emitSpriteData()
{
    QList<QGraphicsItem*> selection = selectedItems();
    if (selection.size() != 1) return; //Update only if exactly one item is selected

    Sprite* sprite = dynamic_cast<Sprite*>(selection.takeLast());
    if (!sprite) return; //Selected item is not Sprite, return

    sprite->emitData();
}

void SpriteScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if (type == SpriteScene::SPRITEMAP) {QGraphicsScene::mousePressEvent(mouseEvent); return;} //TODO: Implement mouse press for spritemap

    auto rect = sceneRect();

    int x = mouseEvent->scenePos().toPoint().x();
    int y = mouseEvent->scenePos().toPoint().y();

    x /= 8; x *=  8;
    y /= 8; y *=  8;

    //selectionItem = std::make_shared<QGraphicsRectItem>(x,y,
    //                                                    sprSize(useLarge),sprSize(useLarge));
    selectionItem->setRect(x,y,sprSize(useLarge),sprSize(useLarge));
    selectionItem->setFlags(QGraphicsItem::ItemIsSelectable);
    selectionItem->setSelected(true);
    //addItem(selectionItem.get());

    setSceneRect(rect);

    return;
}

void SpriteScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsScene::mouseDoubleClickEvent(mouseEvent); //Remove?

    if (type == SpriteScene::GFX)
    {
        //Add new sprite to spritemap
        int spriteIndex = 0;

        QPoint pos = selectionItem->rect().topLeft().toPoint();
        spriteIndex = pos.y()*16/8 + pos.x()/8;

        emit spriteDoubleClicked(spriteIndex);
    }
}

void SpriteScene::drawBackground(QPainter *painter, const QRectF &rect)
{
    QGraphicsScene::drawBackground(painter, rect);

    if (type == SpriteScene::SPRITEMAP)
    {
        if (!drawGrid) return;
        //Draw 8x8 grid
        //TODO: Support offsets, and other sizes?
        painter->setPen(QPen(Qt::lightGray, 0.0));
        int c1;
        for (int i=-16; i < 16; i++)
        {
            c1 = i*8;
            painter->drawLine(-128, c1, 128, c1); //horizontal lines
            painter->drawLine(c1, -128, c1, 128); //vertical lines
        }

        //Draw cross going through (0,0)
        painter->setPen(QPen(Qt::black, 1.5));
        painter->drawLine(-128,0,
                          128, 0);
        painter->drawLine(0, -128,
                          0, 128);
    }
}

void SpriteScene::selectSpriteTile(Sprite* sprite)
{
    if (type != GFX) return;

    auto rect = sceneRect();

    setSceneRect(rect);
}

void SpriteScene::translateSprites(QPoint delta)
{
    Sprite* sprite = nullptr;
    auto items = selectedItems();
    for (QGraphicsItem* item : items)
    {
        sprite = dynamic_cast<Sprite*>(item);
        if (nullptr == sprite) continue;
        sprite->setPosition(sprite->pos().toPoint() + delta);
    }
}

void SpriteScene::requestSprite()
{
    if (type == SpriteScene::GFX)
    {
        //Add new sprite to spritemap
        int spriteIndex = 0;

        QPoint pos = selectionItem->rect().topLeft().toPoint();
        spriteIndex = pos.y()*16/8 + pos.x()/8;

        emit spriteDoubleClicked(spriteIndex);
    }
}

void SpriteScene::toggleGrid(bool enable)
{
    drawGrid = enable;
    update();
}


/**
 * SPRITE IMPLEMENTATION
 **/

Sprite::Sprite(SpriteScene *scene, uint8_t byte0, uint8_t byte1, uint8_t byte2, uint8_t byte3, uint8_t byte4)
{
    x = ((byte1 & 0x01) << 8) | byte0;
    large = byte1 & 0x80;
    y = byte2;
    tileIndex = ((byte4 & 0x01) << 8) | byte3;
    xFlip = byte4 & 0x40;
    yFlip = byte4 & 0x80;
    priority = (byte4 & 0x30) >> 4;
    palette = (byte4 & 0x0E) >> 1;

    //Set own position
    qreal posX = x; if ( x > 0xFF ) posX = -((x & 0xFF)^0xFF);
    qreal posY = y; if ( y > 0x7F ) posY = y-0x100;

    setPos(posX,posY);

    connectSignals();
}

void Sprite::connectSignals()
{
    SpriteScene *scene = qobject_cast<SpriteScene*>(this->scene());
    //Add to scene if possible
    if (nullptr != scene)
    {
        //Set up connections to scene
        connect(this, &Sprite::xPos, scene, &SpriteScene::spriteXPos); //TODO: Other connections
        connect(this, &Sprite::yPos, scene, &SpriteScene::spriteYPos);
        connect(this, &Sprite::sprLarge, scene, &SpriteScene::spriteLarge);
        connect(this, &Sprite::tileVal, scene, &SpriteScene::spriteTileIndex);
        connect(this, &Sprite::spriteObject, scene, &SpriteScene::spriteObject);
        connect(this, &Sprite::sprPalette, scene, &SpriteScene::spritePalette);
        connect(this, &Sprite::sprPriority, scene, &SpriteScene::spritePriority);
        connect(this, &Sprite::delta, scene, &SpriteScene::translateSprites);
    }
}

Sprite::Sprite(SpriteScene *scene)
{
    Sprite(scene, 0,0,0,0,0);
}

std::vector<uint8_t> Sprite::getByteData()
{
    std::vector<uint8_t> result;
    result.resize(5);

    result[0] = x;
    result[1] = ((x >> 8) & 0x01)
              | (large << 7);
    result[2] = y & 0xFF;
    result[3] = tileIndex;
    result[4] =  ((tileIndex >> 8) & 0x01)
               | ((palette   << 1) & 0xE)
               | ((priority  << 4) & 0x30)
               | ((xFlip     << 6) & 0x40)
               | ((yFlip     << 7) & 0x80);

    return result;
}

Sprite *Sprite::raw()
{
    std::vector<uint8_t> data = getByteData();
    return new Sprite(nullptr, data[0], data[1], data[2], data[3], data[4]);
}

QString Sprite::toAsmString()
{
    QString result;
    /*QString temp = QString("  DB $%1,$%2,$%3,$%4,$%5").arg(byte(bytes[0]))
                               .arg(byte(bytes[1]))
                               .arg(byte(bytes[2]))
                               .arg(byte(bytes[3]))
                               .arg(byte(bytes[4]));*/
    std::vector<uint8_t> bytes = getByteData();
    uint16_t first = (bytes[1] << 8) | bytes[0];
    uint8_t second = bytes[2];
    uint16_t third = (bytes[4] << 8) | bytes[3];

    QString a, b, c;
    a = QString::number(first, 16).rightJustified(4, '0').toUpper();
    b = QString::number(second, 16).rightJustified(2, '0').toUpper();
    c = QString::number(third, 16).rightJustified(4, '0').toUpper();
    result = QString("DW $%1 : DB $%2 : DW $%3").arg(a, b, c);
    return result;
}

void Sprite::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    QPoint targetPos = event->scenePos().toPoint();
    targetPos.setX( targetPos.x() - pixmap().width()/2);
    targetPos.setY( targetPos.y() - pixmap().height()/2);

    //Snap if NOT holding SHIFT
    int x = targetPos.x();
    int y = targetPos.y();
    bool snap = !QApplication::keyboardModifiers().testFlag(Qt::ShiftModifier);
    if (snap)
    {
        x /= 4; x *= 4;
        y /= 4; y *= 4;
    }
    targetPos.setX(x);
    targetPos.setY(y);

    QPoint oldPos = scenePos().toPoint();
    //setPosition( targetPos ); //Will be set via delta in scene slot

    emit delta(targetPos - oldPos);
    emit xPos(this->x); emit yPos(this->y);
}

/**
 * @brief Sprite::keyReleaseEvent Keyboard shortcuts for sprite - arrow keys flip. Space toggles size?
 * @param event Event
 */
void Sprite::keyReleaseEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Right:
    case Qt::Key_Left:
        flipX();
        break;

    case Qt::Key_Up:
    case Qt::Key_Down:
        flipY();
        break;

    case Qt::Key_Space:
        setIsLarge( !isLarge() );
        emit sprLarge(isLarge());
        break;
    }
}

/**
 * @brief Sprite::flipX Flip sprite horizontally
 */
void Sprite::flipX()
{
    xFlip = !xFlip;
    setPixmap( pixmap().transformed(QTransform().scale(-1,1)) );
}

/**
 * @brief Sprite::flipX Flip sprite vertically
 */
void Sprite::flipY()
{
    yFlip = !yFlip;
    setPixmap( pixmap().transformed(QTransform().scale(1,-1)) );
}


/**
 * @brief setPos Sets position values
 * @param pos Position, in view coordinates. Used to calculate raw X/Y position data
 */
void Sprite::setPosition(QPoint pos)
{
    x = pos.x() & 0x01FF;
    y = pos.y() & 0x00FF;


    //Set own position
    qreal posX = x & 0x007F; if ( x > 0x7F ) posX = x-0x200;
    qreal posY = y & 0x00FF; if ( y > 0x7F ) posY = y-0x100;

    setPos(posX,posY);
}
