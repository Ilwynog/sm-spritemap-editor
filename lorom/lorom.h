#ifndef LOROM_H
#define LOROM_H

#include "QByteArray"
#include <vector>

class LoRom
{
public:
    LoRom();

    /**
     * @brief snes2pc Turns a byte array (3 bytes, little endian) into a pc address
     * @param src The byte array. Position 0 is low, 1 is high, 2 is bank
     * @return PC address
     */
    static uint32_t snes2pc(QByteArray src);

    /**
     * @brief snes2pc Turns a byte array (2 bytes, little endian) into a pc address. Bank is separated
     * @param src The byte array. Position 0 is low, 1 is high
     * @param bank The implied bank.
     * @return PC address
     */
    static uint32_t snes2pc(QByteArray src, uint8_t bank);

    /**
     * @brief snes2pc Converts a value, assumed lorom, into a pc address. Highest byte is ignored
     * @param src Source value xx|LL|HH|BB
     * @return PC address
     */
    static uint32_t snes2pc(uint32_t src);

    /**
     * @brief pc2snes Turns a given address into a lorom address
     * @param src Source address
     * @return Converted address
     */
    static uint32_t pc2snes(uint32_t src);

    /**
     * @brief findFreeSpace Returns pointer to free space.
     * @param howMuch How large free space needs to be
     * @param allowBankCross Can free space cross bank boundaries
     * @return Pointer to free space, or 0 if could not find
     */
    static uint32_t findFreeSpace(std::vector<uint8_t> &rom, uint32_t howMuch, const bool allowBankCross = true);

    /**
     * @brief findFreeSpace Returns pointer to free space. Does not allow crossing bank boundaries
     * @param howMuch How large free space needs to be
     * @return Pointer to free space, or 0 if could not find
     */
//    static uint32_t findFreeSpace(std::vector<uint8_t> &rom, uint32_t howMuch);

    /**
     * @brief findFreeSpace Returns pointer to free space in a specific bank
     * @param howMuch How large free space needs to be
     * @param bank Which bank
     * @param allowBankCross Can free space cross bank boundaries. Effectively makes bank first allowed bank
     * @return Pointer to free space, or 0 if could not find
     */
    static uint32_t findFreeSpace(std::vector<uint8_t> &rom, uint32_t howMuch, uint8_t bank, const bool allowBankCross);

    /**
     * @brief findFreeSpace Returns pointer to free space in a specific bank
     * @param howMuch How large free space needs to be
     * @param bank Which bank
     * @return Pointer to free space, or 0 if could not find
     */
    static uint32_t findFreeSpace(std::vector<uint8_t> &rom, uint32_t howMuch, uint8_t bank);
};

#endif // LOROM_H
