#include "lorom.h"

LoRom::LoRom()
{

}

// This version has the bank included (long address)
// low = src[0] | hi = src[1] | bank = src[2]
uint32_t LoRom::snes2pc(QByteArray src)
{
    uint8_t bank = src[2];
    uint16_t address = (src[1] << 8) | (src[0] & 0x00FF);

    uint32_t result = ((bank-0x80) * 0x8000) + (address - 0x8000);
    return result;
}

// This version has the bank separated (short address)
// Can be convenient if the bank is implied
uint32_t LoRom::snes2pc(QByteArray src, uint8_t bank)
{
    src.append(bank);
    return snes2pc(src);
}

uint32_t LoRom::snes2pc(uint32_t src)
{
    // Drop the highest byte
    uint32_t rawAddr = src & 0x00FFFFFF;
    uint8_t low = rawAddr & 0x0000FF;
    uint8_t hi = (rawAddr & 0x00FF00) >> 8;
    uint8_t bank = (rawAddr & 0xFF0000) >> 16;
    QByteArray combo;
    combo.append(low).append(hi).append(bank);
    return snes2pc(combo);
}

/**
 * @brief pc2snes Turns a given address into a lorom address
 * @param src Source address
 * @return Converted address
 */
uint32_t LoRom::pc2snes(uint32_t src)
{
    uint8_t bank = src / 0x8000;
    uint16_t address = src - (bank*0x8000) + 0x8000;
    bank += 0x80;

    return (bank << 16 | address);
}

/**
 * @brief findFreeSpace Returns pointer to free space.
 * @param howMuch How large free space needs to be
 * @param allowCrossBank Can free space cross bank boundaries
 * @return Pointer to free space, or 0 if could not find
 */
uint32_t LoRom::findFreeSpace(std::vector<uint8_t> &rom, uint32_t howMuch, uint8_t bank, bool const allowBankCross)
{
    uint64_t start = (bank - 0x80) * 0x8000;
    uint64_t end = (bank - 0x80 + 1) * 0x8000;
    if (allowBankCross) end = rom.size();

    uint32_t freeBytes = 0;
    uint8_t const byte = 0xFF; //Every free byte must be this

    //TODO: Bank boundary checks
    for (uint64_t i = start; i < end; i++)
    {
            if (rom[i] != byte)
            {
                freeBytes = 0;
                continue;
            }
            freeBytes++;

            if (freeBytes >= howMuch)
            {
                return i - howMuch + 1;
            }
    }

    return 0;
}


/**
 * @brief findFreeSpace Returns pointer to free space in a specific bank
 * @param howMuch How large free space needs to be
 * @param bank Which bank
 * @param allowBankCross Can free space cross bank boundaries. Effectively makes bank first allowed bank
 * @return Pointer to free space, or 0 if could not find
 */
/*uint32_t LoRom::findFreeSpace(std::vector<uint8_t> &rom, uint32_t howMuch, bool allowBankCross, uint8_t bank)
{
    uint32_t start = (bank - 0x80) * 0x8000;
    uint32_t end = (bank - 0x80 + 1) * 0x8000;
    if (allowBankCross) end = rom.size();

    uint32_t freeBytes = 0;
    uint8_t const byte = 0xFF; //Every free byte must be this

    for (uint64_t i = start; i < end; i++)
    {
            if (rom[i] != byte)
            {
                freeBytes = 0;
                continue;
            }
            freeBytes++;

            if (freeBytes >= howMuch)
            {
                return i - howMuch + 1;
            }
    }

    return 0;
}*/


/**
 * @brief findFreeSpace Returns pointer to free space in a specific bank
 * @param howMuch How large free space needs to be
 * @param bank Which bank
 * @return Pointer to free space, or 0 if could not find
 */
uint32_t LoRom::findFreeSpace(std::vector<uint8_t> &rom, uint32_t howMuch, uint8_t bank)
{
    return findFreeSpace(rom, howMuch, bank, false);
}
